FROM python:3.8-slim-buster
WORKDIR /app
COPY . .
# mount host drive to this folder
RUN mkdir -p /mnt/data
# install ssh client to use pexpect to login to remote machine
RUN apt update
RUN apt install -y openssh-client
# mounting windows cifs to container
#RUN apt install -y cifs-utils
#RUN apt install -y nfs-common
RUN python3 setup.py install
CMD ["/bin/bash"]
