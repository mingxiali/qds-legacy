# BAU automation project
### Command line tools
### QDS BAU modules
### Installation
Docker approach: Install docker
Use docker command:
```shell
# build docker image
docker build -t qds-legacy .
# create container with name bau
# make sure you add the folder to mount in docker settings -> Resources -> file sharing
docker run --name bau --network host -v C:/Users/MingL/Docker:/mnt/data -it qds-legacy
# linux run:
# docker run --name bau --network host -v /mnt/docker:/mnt/data -it qds-legacy
```
Start container
```
# start container and go to the bash
docker start bau
docker exec -it bau bash
```
Rebuild
```bash
docker stop bau
docker rm bau
docker rmi -f qds-legacy:latest
docker build -t qds-legacy .
docker run --name bau --network host -v C:/Users/MingL/Docker:/mnt/data -it qds-legacy
```


WSL approach:
Install WSL1 on the laptop and mount //general/{username} to /mnt/general
### Configuration
1. Create jira [access token](https://id.atlassian.com/manage-profile/security/api-tokens)
2. Install Python3.8 and qds-legacy package
3. Run command to config the project with 
 ```bau config edit```
4. Type in all requested info
5. Test command in cmd ```gira list -vt5```
### showcase commands
```bash
bau config edit
bau config show
gira list -vt5
gira create -c "Legacy System" -w -s "Showcase for BAU tools" -d "Share the tools" -p QBAU
qm -u qdsclient -m devel2
bau run -u qdsclient -r QBAU-946 -f data_file -t /home/qdsclient/term -m qds-devel2
bau run -u qdsclient -r QBAU-946 -m qds-devel2 -s "mv ~/term/data_file ~/term/data_file_test"
bau run -u qdsclient -r QBAU-946 -f /home/qdsclient/term/data_file_test -t data_file_test -m qds-devel2 -d
bau db -a extract -s "SELECT * FROM security limit 5" -r QBAU-946 -n global_db -p uat
#Please provide credentials for global_db_uat
#db_host_name: qds-pgsql-dev
#db_host_path: /opt/postgres/10-pgdg/bin/64/psql
#db_name: global_db
#db_user: load
#port: 5432
#Password: 
#profile: uat
#engine: psql
# exit
docker bau start
docker exec -it bau bash
```
### Samples
### Docs
### Extra
TODO:
1. support new gateway
2. adjust API