# Always prefer setuptools over distutils
from setuptools import setup, find_packages
import pathlib

here = pathlib.Path(__file__).parent.resolve()

# Get the long description from the README file
long_description = (here / "README.md").read_text(encoding="utf-8")

setup(
    name="qds-legacy",  # Required
    version="0.3",  # Required
    description="Legacy BAU automation project",  # Optional
    long_description=long_description,  # Optional
    long_description_content_type="text/markdown",  # Optional
    author="mingxiali",  # Optional
    author_email="leemingxia@outlook.com",  # Optional
    url="www.alienz.top",
    classifiers=[  # Optional
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Developers",
        "Topic :: Software Development :: Build Tools",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "Programming Language :: Python :: 3.7",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3 :: Only",
    ],
    keywords="automation, putty, development",  # Optional
    package_dir={"": "src"},  # Optional
    packages=find_packages(where="src"),  # Required
    python_requires=">=3.6, <4",
    install_requires=[
        'pandas>=1.4.2',
        # 'numpy>=1.20.3',
        'paramiko>=2.10.3',
        'paramiko-expect>=0.3.0',
        'termcolor>=1.1.0',
        'colorama>=0.4.4',
        'jira>=3.1.1',
        'pexpect>=4.8.0',  # only works on linux/unix
        # 'wexpect>=4.0.0',
        'loguru>=0.6.0',
        'win32-setctime>=1.1.0;platform_system=="Windows"',
        'fiscalyear>=0.4.0',
        # 'trading-calendars>=2.1.1', # no longer maintained
        'regex>=2022.3.15',
        'requests>=2.27.1',
        'XlsxWriter>=1.2.8',
        'xlrd>=2.0.1',
        'openpyxl>=3.0.10'
    ],
    # List additional groups of dependencies here (e.g. development
    # dependencies). Users will be able to install these using the "extras"
    # syntax, for example:
    #
    #   $ pip install sampleproject[dev]
    #
    # Similar to `install_requires` above, these must be valid existing
    # projects.
    extras_require={  # Optional
        "dev": ["check-manifest"],
        "test": ["coverage"],
    },
    package_data={  # Optional
        "commands": ["settings.ini"],
        "dbmanager": ["db.json"],
    },
    data_files=[("qds_data", ["data/data_file"])],  # Optional
    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # `pip` to create the appropriate form of executable for the target
    # platform.
    #
    # For example, the following would provide a command called `commands` which
    # executes the function `main` from this package when invoked:
    entry_points={
        "console_scripts": [
            "bau=commands.bau:main",
            "gira=commands.gira:main",
            "qm=commands.qm:main",
        ],
    },
)
print("Please set config the project with command: bau config edit")
