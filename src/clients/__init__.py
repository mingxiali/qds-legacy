import pathlib

PACK_PATH = pathlib.Path(__file__).parent.resolve()
DEFAULT_KWS = ['wara', 'parac', 'swealth', 'dbaseload']
RELEASE_ROOT = '/home/qdsbuild/wkspace/i386/SunOS-5.11/sunpro/release/32'

__all__ = ['PACK_PATH', 'DEFAULT_KWS', 'RELEASE_ROOT', ]
