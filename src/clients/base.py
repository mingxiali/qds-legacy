from abc import ABC, abstractmethod
import pandas as pd
from loguru import logger
from clients.util import MonthlyReportDates, MonthlyReportDirs, diff_check as dc
from session import Session
from utils.funcs import get_issue_key


class Request(ABC):
    # flow: fetch meta -> process
    @staticmethod
    def check_process(keywords: list, session: Session):
        """
        check if process with keywords still running
        :return: True if no process found
        """
        elements = [f"[{key[0]}]" + key[1:] for key in keywords]
        kw_str = '|'.join(elements)
        cmd = f"ps -aef | egrep '{kw_str}' | grep -v screen"
        output = session.check_output(cmd)
        if len(output):
            logger.error(f'Check if other process is running with command "{cmd}"')
            for line in output:
                logger.error(line)
            raise ValueError('Other process running on remote machine')

    @classmethod
    def confirm_action(cls, prompt: str = 'confirm your action') -> bool:
        """
        confirm action before proceeding to the next step
        :param prompt: prompt string
        :return: True of False
        """
        action = input(f"Please Enter Y to confirm action: {prompt}: ")
        if action.strip().lower() in ['', 'y', 'yes']:
            return True
        return False

    @staticmethod
    def diff_check(src: str, des: str, session: Session):
        """
        Diff check between files
        :param src: source file
        :param des: des file
        :param session: session to run commands
        :return: None
        """
        dc(src, des, session)

    @abstractmethod
    def run(self):
        """
        main function to trigger the process
        :return:
        """
        pass


class MonthlyTask(Request, ABC):
    def __init__(self, client: str, rpt_month: str = '', ticket: str = '', kw: str = ''):
        super(MonthlyTask, self).__init__()
        self.client = client
        if not rpt_month:
            rpt_month = (pd.Timestamp.now() - pd.Timedelta('28 days')).strftime('%Y%m')
        self.dates = MonthlyReportDates(rpt_month)
        self.dirs = MonthlyReportDirs(self.client, self.dates.rpt_month)
        if not ticket and kw:
            ticket = get_issue_key(kw=kw)
        self.ticket = ticket


class Workflow(Request, ABC):
    # flow: reformat -> load -> process (call request) -> rec -> publish
    pass


