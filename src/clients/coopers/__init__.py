import pathlib

PACK_PATH = pathlib.Path(__file__).parent.resolve()
REMOTE_BASE = '/data/attribution/parac/data'
BIN_BASE = '/projects/attribution/parac/bin'
CVS_BASE = 'src/attribution/parac/bin.sh'
TICKET_KW = 'coopers monthly attribution'
PORTFOLIOS = ['PCBRAE', 'PCHEST', 'PCPENS']
INIT_COMMAND = f'export LOGNAME=parac;cd {REMOTE_BASE};'

__all__ = [PACK_PATH, REMOTE_BASE, BIN_BASE, TICKET_KW, PORTFOLIOS, INIT_COMMAND, ]
