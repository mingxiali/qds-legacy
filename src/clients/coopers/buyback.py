#!/usr/bin/env python3
import shutil
from pathlib import Path
import json
import pandas as pd
import os
from commands import DATA_PATH
from clients.base import MonthlyTask
from clients.coopers import TICKET_KW, INIT_COMMAND, REMOTE_BASE
from loguru import logger
from session import Session


class BuyBack(MonthlyTask):
    """
    Create buyback pf file
    Use this as an example for WOW buyback
    https://cdn-api.markitdigital.com/apiman-gateway/ASX/asx-research/1.0/file/2924-02436724-2A1331512?access_token=83ff96335c2d45a094df02a206a39ff4
    BuybacksNCS Trades file has a Sale for the buyback and the price is usually the deemed capital proceeds. If this is true then we add the following three adjustments to the PF file.

    Franking credits to be added to PF file = number of shares participating in the buyback * fully franked dividend * 0.3 / 0.7

    Price adjustment to be added to PF file = number of shares participating in the buyback * (capital component - deemed capital proceeds)
    Dividend to be added to PF file = number of shares participating in the buyback * dividend per share

    For RIO buyback on 20150407:Franking credits to be added to PF file = 1265 * 39 * 0.3 / 0.7 = 21143.57
    Price adjustment to be added to PF file = 1265 * (9.44 – 16.78) = -9285.10
    Dividend to be added to PF file = 1265 * 39 = 49335

    [10:14:31] qds-proc2:/data/attribution/parac/data
    qdsclient$ cat  20190430pf.csv
    0,PERFFLOW,0,20190430,5
    9,PCPENS,U_CTXX,20190415,68831.64,Franking Credits
    9,PCPENS,U_CTXX,20190415,-26917.82,Price Adjustment
    9,PCPENS,U_CTXX,20190415,160607.16,Dividend
    """

    task_name = TICKET_KW
    meta = dict()

    def __init__(self, rpt_month: str = '', ticket: str = ''):
        client = Path(__file__).parent.name
        super().__init__(client, rpt_month, ticket, self.task_name)
        f_path = Path(DATA_PATH) / f'{self.dates.rpt_month}_buyback.json'
        if f_path.exists():
            with open(f_path) as file:
                self.meta = json.load(file)
            shutil.copy2(f_path, os.path.join(self.dirs.input, f'{self.dates.rpt_month}_buyback.json'))
        else:
            logger.error(f'No buyback file found in {DATA_PATH}')
        self._collect_data()
        self.session = Session(reason=self.ticket, display=False)
        self.session.send(INIT_COMMAND)
        logger.info('Buyback processor initialized')

    def _collect_data(self):
        """
        collect data from the trade file and calculate the numbers to generate the pf file
        :return:
        """
        rpt_month_bY = pd.to_datetime(self.dates.rpt_month, format='%Y%m').strftime('%b %Y')
        trade_file = f'NCS Trades {rpt_month_bY}.csv'
        df = pd.read_csv(os.path.join(self.dirs.input, trade_file), header=1, thousands=',',
                         dtype={'Units/Contracts': float, 'Unit Price (N)': float})
        df = df[pd.notnull(df['Asset ID'])]
        df.loc[:, "Transaction Date"] = df["Transaction Date"].apply(pd.to_datetime)
        for ticker in self.meta.keys():
            mask_ticker = df['Asset ID'] == ticker
            trade_date = pd.to_datetime(self.meta[ticker]['date'], format='%Y%m%d')
            mask_date = df['Transaction Date'] == trade_date
            mask_type = df['Transaction Code'] == 'OS-SAL'
            df_entries = df[mask_date & mask_ticker & mask_type]
            portfolios = df_entries['Portfolio Code'].to_list()
            assert len(set(portfolios)) == len(portfolios)
            assert len(portfolios) > 0
            deemed_capital_proceeds = df_entries['Unit Price (N)'].values[0]
            assert set(df_entries['Unit Price (N)']) == {deemed_capital_proceeds}
            df_meta = df_entries[['Portfolio Code', 'Units/Contracts', 'Unit Price (N)']]
            # fetch units in the holding before the buyback
            # idx_trade = self.dates.trading_days.index(trade_date)
            # if idx_trade < 1:
            #     # TODO: fetch previous month file
            #     logger.error(f'No previous trade day in current report month for {trade_date}')
            #     raise ValueError('Unexpected trade date')
            # prev_trade_day = self.dates.trading_days[idx_trade - 1]
            # prev_trade_day_str = prev_trade_day.strftime('%Y%m%d')
            # # get holdings
            # fn = f'{prev_trade_day_str} - attribution pv.csv'
            # df = pd.read_csv(os.path.join(self.dirs.input, fn), header=1)
            # df.columns = [x.strip() for x in df.columns]
            # df = df[['Pfolio', 'AssetID', 'SecurityName', 'UnitHolding', 'MarketPriceLastOther', 'ExchNtvtoBase',
            #          'MktAccValueLastOther(B)']]
            # df = df.rename(columns={'Pfolio': 'portfolio', 'UnitHolding': 'cust_holding',
            #                         'MarketPriceLastOther': 'cust_price',
            #                         'ExchNtvtoBase': 'ex_rate', 'MktAccValueLastOther(B)': 'amount'})
            # df.loc[:, 'cust_holding'] = df['cust_holding'].astype(float)
            # df = df[pd.notnull(df['AssetID'])]
            # df.loc[:, 'AssetID'] = df['AssetID'].apply(lambda x: x.strip())
            # df = df[df['AssetID'] == ticker]
            # for idx, row in df_meta.iterrows():
            #     # should hold it in pv file
            #     df_rows = df[df['portfolio'] == row['Portfolio Code']]
            #     assert len(df_rows) == 1
            #     df_meta.loc[idx, 'Units/Contracts'] = df_rows.iloc[0]['cust_holding']
            df_meta.insert(len(df_meta.columns), 'dividend', self.meta[ticker]['dividend'])
            df_meta.insert(len(df_meta.columns), 'franked_rate', self.meta[ticker]['frank_rate'])
            df_meta.insert(len(df_meta.columns), 'franking_credits',
                           (df_meta['Units/Contracts'] *
                            df_meta['dividend'] * df_meta['franked_rate'] * 0.3 / 0.7).round(2))
            df_meta.insert(len(df_meta.columns), 'price_adj',
                           (df_meta['Units/Contracts'] *
                            (self.meta[ticker]['capital_component'] - deemed_capital_proceeds)).round(2))
            df_meta.insert(len(df_meta.columns), 'dividend_adj',
                           (df_meta['Units/Contracts'] * self.meta[ticker]['dividend']).round(2))
            self.meta[ticker]['numbers'] = df_meta

    def generate_pf_file(self):
        """
        generate pf file based on the numbers in meta
        ex:
            0,PERFFLOW,0,20190430,5
            9,PCPENS,U_CTXX,20190415,68831.64,Franking Credits
            9,PCPENS,U_CTXX,20190415,-26917.82,Price Adjustment
            9,PCPENS,U_CTXX,20190415,160607.16,Dividend
        :return:
        """
        # download the files and get the data
        # append to the list and upload the file
        # convert the meta info
        # convert info
        data = []
        for ticker in self.meta.keys():
            df = self.meta[ticker]['numbers']
            date_str = self.meta[ticker]['date']
            for idx, row in df.iterrows():
                # Franking credits
                data.append(f"""9,{row['Portfolio Code']},U_{ticker},{date_str},{row['franking_credits']},Franking Credits\n""")
                data.append(
                    f"""9,{row['Portfolio Code']},U_{ticker},{date_str},{row['price_adj']},Price Adjustment\n""")
                data.append(
                    f"""9,{row['Portfolio Code']},U_{ticker},{date_str},{row['dividend_adj']},Dividend\n""")
        logger.info('Applying the data to the perform flow file')
        # merge remote file
        fn = f'{self.dates.bme_YYYYMMDD}pf.csv'
        data_lines = []
        if self.session.check_file(f'{REMOTE_BASE}/{fn}'):
            lines = self.session.read_file(f'{REMOTE_BASE}/{fn}')
            for line in data:
                if line in lines:
                    continue
                data_lines.append(line)
            # skip header
            data_lines += lines[1:]
        else:
            data_lines = data
        # create header
        header = [f"""0,PERFFLOW,0,{self.dates.bme_YYYYMMDD},{len(data_lines)}\n"""]
        with open(os.path.join(self.dirs.output, fn), 'w') as writer:
            writer.writelines(header + data_lines)
        # upload to remote
        shutil.copy2(os.path.join(self.dirs.output, fn), os.path.join(DATA_PATH, fn))
        self.session.upload(fn, f'{REMOTE_BASE}/{fn}')
        logger.info('Perf file uploaded')

    def run(self):
        self.generate_pf_file()


if __name__ == '__main__':
    BuyBack().run()
