import os
import shutil
from glob import glob
from pathlib import Path
import pandas as pd
import regex
from clients.base import MonthlyTask
from clients import PACK_PATH
from clients.coopers import TICKET_KW, INIT_COMMAND, REMOTE_BASE, BIN_BASE, CVS_BASE
from commands import DATA_PATH
from clients.util import Session, DBManager
from loguru import logger
from clients.util import diff_check, insert_file_before_kw


class Loader(MonthlyTask):
    """
    Loader class to load data into database and reformat input files during the process
    """
    reprice_file = 'repriceOSTrades.py'
    translate_unlisted_file = 'translate_unlisted.pl'
    translate_file = 'translate.pl'
    pv_price_file = 'extract_pv_prices.sh'
    task_name = TICKET_KW
    placements = set()
    new_sec = dict()
    created_sec = list()
    buybacks = set()
    foreign_stock = set()
    futures = set()
    dividend = pd.DataFrame()
    future_trade = ['FU-SAL', 'FU-PUR']
    fx_file = 'forex.csv'

    def __init__(self, rpt_month: str = '', ticket: str = ''):
        client = Path(__file__).parent.name
        super().__init__(client, rpt_month, ticket, self.task_name)
        self.session = Session(reason=self.ticket, display=False)
        self.session.send(INIT_COMMAND)
        self.build_sh = Session(reason=self.ticket, display=False)
        self.db_mgr = DBManager()
        self._load_file()
        logger.info(f"Task {self.task_name} initialized")

    def _load_file(self):
        """
        load files in input folder
        :return:
        """
        date_str = self.dates.month_start.strftime('%b %Y')
        trade_file = os.path.join(self.dirs.input, f'NCS Trades {date_str}.csv')
        df = pd.read_csv(trade_file, header=1)
        df = df[pd.notnull(df['Asset ID'])]
        df.loc[:, "Transaction Date"] = df["Transaction Date"].apply(pd.to_datetime)
        # download FX.csv
        cmd = f'{BIN_BASE}/FX.py {REMOTE_BASE}/forex.csv > {REMOTE_BASE}/FX.csv'
        self.session.send(cmd)
        self.session.download(f'{REMOTE_BASE}/FX.csv')
        df_ex = pd.read_csv(os.path.join(DATA_PATH, 'FX.csv'), header=None,
                            names=['date', 'currency', 'rate'])
        df_ex.loc[:, 'date'] = df_ex['date'].astype(str).apply(lambda x: pd.to_datetime(x, format='%Y%m%d'))
        self.currencies = [x for x in df_ex['currency'].unique() if len(x) >= 3]
        self.fx_rate = df_ex
        # reformat the asset id column for foreign stocks
        # download translate file
        self.session.download(f'{BIN_BASE}/{self.translate_file}')
        unexpected_code = set()
        currency_list = self.currencies + ['ESB']
        # deal with cases like ABCDESB when we are expecting ABCD/ESB
        for idx, row in df.iterrows():
            sec_code = row['Asset ID']
            if len(sec_code) > 3 and (sec_code[-3:] in currency_list) and sec_code[-4] != '/':
                new_id = row['Asset ID'][:-3] + '/' + row['Asset ID'][-3:]
                logger.info(f"Updating {row['Asset ID']} to {new_id}")
                df.loc[idx, 'Asset ID'] = new_id
                unexpected_code.add(sec_code)
            if row['Transaction Code'] in self.future_trade:
                self.futures.add(row['Asset ID'])
        self.df_trade = df
        self.convert_code(unexpected_code)

    def restore_db(self) -> None:
        """
        restore the tables from previous dumps
        :return: None
        """
        tables = ["instrument_pc", "perf_flow_pc", "cashposition_pc", "portfolio_holdings_pc", "portfolio_trans_pc",
                  "stockdaily_pc", "analyst_pc", "analyst_trans_pc", "country_pc", "dividend_pc"]
        for table in tables:
            sql = f"""truncate table live..{table}"""
            self.db_mgr.exec_sql(sql)
            cmd = f"""bcp live..{table} in backup.{table} -Uupdater -Pupdate05 -SQDS2_PROD -c -t"|" -Y"""
            self.session.send(cmd)
        # sql = f"""DELETE FROM dividend_pc WHERE enter_date>={self.dates.ms_YYYYMMDD}"""
        # self.db_mgr.exec_sql(sql)
        logger.info("DB clean up completed")

    def extract_foreign_dividend(self):
        """
        Extract foreign dividend from file and convert to AUD
        :return:
        """
        logger.info('Extracting dividend info')
        df_div = self.df_trade
        df_div = df_div[df_div['Transaction Code'] == 'DIV']
        df_div = df_div[['Portfolio Code', 'Settlement Indicator', 'Asset ID',
                         'Transaction Date', 'Deletion Indicator']]
        # filter for foreign div
        df_div = df_div[pd.notnull(df_div['Asset ID'])]
        mask_foreign = df_div['Asset ID'].str.contains('/', regex=False)
        df_div = df_div[mask_foreign]
        df_div = df_div.drop_duplicates(subset=['Settlement Indicator', 'Asset ID',
                                                'Transaction Date',
                                                'Deletion Indicator'])
        # unique
        # find ISIN list
        df_pv = pd.DataFrame()
        for file in glob(os.path.join(self.dirs.input, '20[0-9]* - attribution pv.csv')):
            date_str = os.path.basename(file).split('-')[0].strip()
            df = pd.read_csv(file, header=1)
            df.columns = [x.strip() for x in df.columns]
            df = df[['AssetID', 'ISIN']]
            df.insert(0, 'date', date_str)
            df_pv = pd.concat([df_pv, df])
        df_pv.loc[:, 'ISIN'] = df_pv['ISIN'].fillna('').astype(str).str.strip()
        df_pv = df_pv[df_pv['ISIN'] != '']
        mask_foreign = df_pv['AssetID'].astype(str).str.contains('/', regex=False)
        df_isin = df_pv[mask_foreign]
        df_isin = df_isin.drop_duplicates(keep='last')
        # merge with isin
        df_div = df_div.rename(columns={'Transaction Date': 'date'})
        df_div.loc[:, 'date'] = df_div['date'].apply(lambda x: pd.to_datetime(x, format='%d-%b-%y'))
        df_isin = df_isin.rename(columns={'AssetID': 'Asset ID'})
        df_isin.loc[:, 'date'] = df_isin['date'].apply(lambda x: pd.to_datetime(str(x), format='%Y%m%d'))
        df_div.loc[:, 'Asset ID'] = df_div['Asset ID'].str.strip()
        df_isin.loc[:, 'Asset ID'] = df_isin['Asset ID'].str.strip()
        df_ret = pd.merge(df_div, df_isin, how='left', on=['Asset ID', 'date'])
        # fix missing isin
        for idx, row in df_ret.iterrows():
            if str(row['ISIN']) != 'nan':
                continue
            # fetch from previous PV file
            if row['date'] > self.dates.month_end:
                raise ValueError(f'Cannot find isin for Asset {row["Asset ID"]}')
            pv_fn = f"{row['date'].strftime('%Y%m%d')} - attribution pv.csv"
            try:
                df = pd.read_csv(os.path.join(self.dirs.input, pv_fn), header=1)
            except FileNotFoundError as e:
                # check date
                if row['date'] < self.dates.month_start:
                    continue
                else:
                    raise e
            df.columns = [x.strip() for x in df.columns]
            df_entries = df[df['AssetID'] == row['Asset ID']]
            assert len(df_entries) > 0
            df_ret.loc[idx, 'ISIN'] = df_entries.iloc[0]['ISIN']
        # merge exchange rate
        df_ex = self.fx_rate
        df_ret.insert(0, 'currency', df_ret['Asset ID'].apply(lambda x: x.split('/')[-1]))
        df_ret = pd.merge(df_ret, df_ex, how='left', on=['date', 'currency'])
        # fill in missing ex-rate
        for idx, row in df_ret.iterrows():
            if str(row['rate']) != 'nan':
                continue
            # fetch from previous PV file
            if row['date'] >= self.dates.month_start:
                # check the currency
                if row['Asset ID'].endswith('ESB'):
                    # use EUR as currency for ESB
                    entries = df_ex[(df_ex['date'] == row['date']) & (df_ex['currency'] == 'EUR')]
                    assert len(entries) == 1
                    rate = entries.iloc[0]['rate']
                    df_ret.loc[idx, 'rate'] = rate
                    continue
                raise ValueError(f'Cannot find fx for Asset {row["Asset ID"]}')
            rpt_month = row['date'].strftime('%Y%m')
            fx = "forex.csv"
            if rpt_month == self.dates.rpt_month:
                df = pd.read_csv(os.path.join(self.dirs.input, fx))
            else:
                if not os.path.exists(os.path.join(DATA_PATH, f'{rpt_month}_{fx}')):
                    self.session.download(f'{REMOTE_BASE}/orig/{rpt_month}/{fx}', f'{rpt_month}_{fx}')
                df = pd.read_csv(os.path.join(DATA_PATH, f'{rpt_month}_{fx}'))
            df = df.rename(columns={'Unnamed: 0': 'date'})
            df.columns = [x.strip() for x in df.columns]
            df.columns = [x.replace('AUD', '') for x in df.columns]
            df.loc[:, 'date'] = df['date'].apply(lambda x: pd.to_datetime(x, format='%m/%d/%Y'))
            df_entries = df[df['date'] == row['date']]
            assert len(df_entries) > 0
            currency = row['currency']
            df_ret.loc[idx, 'rate'] = df_entries.iloc[0][currency]
        # df_ret.to_csv(os.path.join(self.dirs.output, 'div.csv'), index=False)
        # now update new sec member
        asset_ids = df_ret['Asset ID'].unique()
        for asset_id in asset_ids:
            self.new_sec[asset_id] = self.dates.ms_YYYYMMDD
            self.foreign_stock.add(asset_id)
        self.dividend = df_ret

    def find_placements(self) -> None:
        """
        drive input for trade file, ticker with exactly 5 chars and ends with [XX|YY|ZZ]
        :return: None
        """
        df = self.df_trade
        values = ['XX', 'YY', 'ZZ']
        pattern = r'^[A-Z][A-Z][A-Z]({})$'.format('|'.join(map(regex.escape, values)))
        asset_mask = df['Asset ID'].str.match(pattern)
        df_plc = df[asset_mask]
        df_plc = df_plc.sort_values(by=['Transaction Date'])
        df_plc = df_plc.drop_duplicates(subset=['Asset ID'], keep='first')
        for _, row in df_plc.iterrows():
            self.placements.add(row["Asset ID"])
            self.new_sec[row["Asset ID"]] = self.dates.ms_YYYYMMDD
        logger.info(f'Placements {self.placements} found in trade file')

    def find_buybacks(self):
        """
        find [A-Za-z][A-Za-z][A-Za-z]X in transaction file;
        Need to update the exchange rate file in repriceOSTrades.py
        :return:
        """
        other_mask = self.df_trade['Asset ID'].str.match('^[A-Z][A-Z][A-Z]X$')
        df_plc_ot = self.df_trade[other_mask]
        buybacks = set()
        if not df_plc_ot.empty:
            # in this pattern, update repriceOSTrades.py to set the exchange rate first
            # update the file and commit; ex:             elif stock == 'U_ALDX' : # local   rate = 1.0
            for _, row in df_plc_ot.iterrows():
                # need these securities as input of parseAndLoad script
                buybacks.add(row['Asset ID'])
                self.placements.add(row['Asset ID'])
                self.buybacks.add(row["Asset ID"])
                self.new_sec[row['Asset ID']] = self.dates.ms_YYYYMMDD
        if buybacks:
            logger.warning(f'Buy backs {buybacks} found in trade file')

    def find_foreign_stocks(self) -> None:
        """
        find stocks with ticker of [A-Za-z]*/[currency]
        Need to update the exchange rate file in repriceOSTrades.py
        :return: None
        """
        ex_currencies = self.currencies + ['ESB']
        for asset_id in self.df_trade['Asset ID'].unique():
            # loop over asset id to find foreign stocks
            if len(asset_id) > 3 and (asset_id[-3:] in ex_currencies) and asset_id[-4] == '/':
                self.new_sec[asset_id] = self.dates.ms_YYYYMMDD
                self.foreign_stock.add(asset_id)

    def find_new_sec(self):
        self.extract_foreign_dividend()
        self.find_placements()
        self.find_buybacks()
        self.find_foreign_stocks()

    def create_new_instrument(self) -> None:
        """
        Create new instruments for foreign stocks, placements and buybacks if they do not exist in the database
        will run script on remote machine to avoid interactions
        store it to new_sec.csv to keep a record
        :return: None
        """
        self.session.upload(os.path.join(PACK_PATH, 'resources', 'new_instr.sh'), '~/term')
        for sec, start_date in self.new_sec.items():
            sec = sec.replace('/', '')
            if not sec.startswith("U_"):
                sec = f'U_{sec}'
            if sec.endswith('ESB'):
                sec = sec[:-3] + 'EUR'
            from clients.new_sec import UnlistedSecurity
            ret = UnlistedSecurity(sec, self.client, start_date, self.session).create()
            if not ret:
                # already exists, skip it
                continue
            self.created_sec.append(sec)
        # store it to new_sec.csv as we need it to load gics in the next step
        if os.path.exists(os.path.join(self.dirs.input, 'new_sec.csv')):
            df_new_sec = pd.read_csv(os.path.join(self.dirs.input, 'new_sec.csv'))
        else:
            df_new_sec = pd.DataFrame(columns=['ticker'])
        df_created = pd.DataFrame(data={'ticker': list(self.created_sec)})
        df_new_sec = pd.concat([df_new_sec, df_created])
        df_new_sec = df_new_sec.drop_duplicates(subset=['ticker'])
        df_new_sec.to_csv(os.path.join(self.dirs.input, 'new_sec.csv'), index=False)
        # update new sec as newly created for this month
        df_new_sec.insert(1, 'start_date', self.dates.ms_YYYYMMDD)
        self.new_sec = dict(zip(df_new_sec['ticker'], df_new_sec['start_date']))

    def update_mapping(self) -> None:
        """
        update mapping file for newly created sec
        if already exist, skip
        commit the changes to cvs
        :return: None
        """
        # read files
        fn = self.translate_unlisted_file
        lines = self.session.read_file(f'{BIN_BASE}/{fn}')
        newlines = []
        for code in self.new_sec.keys():
            code = code.replace('U_', '').replace('/', '')
            if code.endswith('ESB'):
                new_code = code[:-3] + 'EUR'
                line = f's/,{code},/,U_{new_code},/;\n'
            else:
                line = f's/,{code},/,U_{code},/;\n'
            if line not in lines:
                newlines.append(line)
        # echo lines to the end of the file and commit changes
        self.update_translate_script(newlines, self.translate_unlisted_file)

    def update_scripts(self) -> None:
        """
        foreign stocks, buybacks will need manual hacks to apply foreign currency
        :return: None
        """
        # check foreign rate
        # check buyback
        df_fx = pd.read_csv(os.path.join(self.dirs.input, 'forex.csv'))
        currencies = [x.replace('AUD', '') for x in df_fx.columns if 'AUD' in x]
        new_items = []
        for value in self.new_sec:
            if value[2:] in self.buybacks:
                new_items.append(value)
                continue
            if value[-3:] in currencies:
                # make an entry in repriceOSTrades.py
                new_items.append(value)
            else:
                if value.replace('U_', '') not in self.placements:
                    logger.warning(f'FX not found for ticker {value}')
                # raise
        # cp to local and update the src
        # if ESB, update translate.pl and extract_pv_prices.sh too
        # update this file
        reprice_lines = []
        translate_lines = []
        pv_lines = []
        for item in new_items:
            cmd = f"""grep "stock == '{item}'" {BIN_BASE}/{self.reprice_file}"""
            output = self.session.check_output(cmd, last_line_only=True)
            if output:
                continue
            if item.endswith('ESB'):
                # need to update translate.pl and extract_pv_prices.sh
                new_code = item[:-3] + 'EUR'
                hack_lines = [f"""            elif stock == '{new_code}': # EUR\n""",
                              """                rate = FX[date]['EUR']\n"""]
                # add to translate.pl
                orig_code = item[2:-3]
                translate_lines.append(f"""s/{orig_code}\/ESB,/{orig_code}\/EUR,/;\n""")
                # add to extract_pv_prices.sh
                pv_lines.append(f"""    # {orig_code}ESB rename\n""")
                pv_lines.append(f"""    perl -pi -e 's/{orig_code}ESB/{orig_code}\/EUR/' "$f\n""")
                logger.error('Please process this manually')
            elif item[2:] in self.buybacks:
                # buyback stock
                # patterns like XX|YY|ZZ are already covered
                hack_lines = [f"""            elif stock == '{item}': # {item} buyback\n""",
                              """                rate = 1.0\n"""]
            else:
                # foreign stock
                currency = item[-3:]
                hack_lines = [f"""            elif stock == '{item}': # {currency}\n""",
                              f"""                rate = FX[date]['{currency}']\n"""]
            reprice_lines += hack_lines
        self.insert_hack_script(reprice_lines, self.reprice_file, 'if (currpos < 0): # local unlisted stock')
        if translate_lines:
            new_lines = self.filter_hack_lines(translate_lines, self.translate_file)
            self.update_translate_script(new_lines, self.translate_file)
        if pv_lines:
            new_lines = self.filter_hack_lines(pv_lines, self.pv_price_file)
            self.insert_hack_script(new_lines, self.pv_price_file, '# Get date from filename')

    def filter_hack_lines(self, pv_lines: list, fn: str):
        """
        read from remote file and compare with new lines to filter out
        :param pv_lines: lines to be inserted into the file
        :param fn: file name in remote BIN_BASE
        :return:
        """
        new_lines = []
        lines = self.session.read_file(f'{BIN_BASE}/{fn}')
        for line in pv_lines:
            if line not in lines:
                new_lines.append(line)
        return new_lines

    def insert_hack_script(self, hack_lines: list, fn: str, kw: str):
        """
        add hack lines to reprice script
        :param hack_lines: lines to add for new securities
        :param kw: keyword to search in the file
        :param fn: file name to update
        :return:
        """
        if not hack_lines:
            return
        hack_file = f'{self.dates.rpt_month}_{fn}_hack.txt'
        with open(os.path.join(DATA_PATH, hack_file), 'w', newline='\n') as file:
            file.writelines(hack_lines)
        self.build_sh.upload(hack_file, '~/term')
        # now run command to create a temp file
        cmd = f'release; cd {CVS_BASE}/; cvs up'
        self.build_sh.send(cmd)
        insert_file_before_kw(self.build_sh, fn, hack_file, kw, offset=2)
        self.build_sh.download(f'~/term/{fn}')
        # check the result
        diff_check(fn, '~/term', self.build_sh)
        self.confirm_action()
        cmd = f"""cp ~/term/{fn} .; cvs ci -m 'update {fn}'; cp {fn} {BIN_BASE}"""
        self.build_sh.send(cmd)

    def convert_code(self, code_list: set) -> None:
        """
        convert the code so that the price will be picked up properly
        # need to do this if client sent unexpected code (ex1) or unexpected currency (ex2)
        ex1: s/ALEADMXP,/ALEA\/MXP,/;
        ex2: s/AMAD\/ESB,/AMAD\/EUR,/;
        :param code_list: code list needs to be converted
        :return: None
        """
        updated_lines = []
        # read file
        lines = self.session.read_file(f'{BIN_BASE}/{self.translate_file}')
        for code in code_list:
            # check if it is already there
            if '/' not in code:
                ticker, currency = code[:-3], code[-3:]
                if currency == 'ESB':
                    currency = 'EUR'
                new_line = f's/{code},/{ticker}\/{currency},/;\n'
                if new_line in lines:
                    continue
                updated_lines.append(new_line)
            else:
                raise ValueError(f'Please process {code} manually')
        self.update_translate_script(updated_lines, self.translate_file)

    def update_translate_script(self, updated_lines: list, fn: str):
        """
        update translate script by appending updated lines
        :param updated_lines: lines to append to the existing file
        :param fn: specify which file to append to
        :return:
        """
        if not updated_lines:
            return
        logger.info(f'Updating {fn}')
        with open(os.path.join(DATA_PATH, 'new_lines.txt'), 'w', newline='\n') as outfile:
            outfile.writelines(updated_lines)
        # upload file and commit the changes
        self.build_sh.upload('new_lines.txt', '~/term')
        cmd = f'cp {BIN_BASE}/{fn} ~/term; cat ~/term/new_lines.txt >> ~/term/{fn}'
        self.build_sh.send(cmd)
        diff_check(f'{BIN_BASE}/{fn}', '~/term', self.build_sh)
        self.confirm_action()
        cmd = f"""release; cd {CVS_BASE}; cp ~/term/{fn} .;""" \
              f"""cvs ci -m 'update {fn}'; cp {fn} {BIN_BASE}"""
        self.build_sh.send(cmd)

    def process_new_sec(self):
        self.create_new_instrument()
        self.update_mapping()
        self.update_scripts()

    def gen_div_file(self):
        """
        generate dividend data by querying MSCI database to fill the missing piece
        :return:
        """
        df_div = self.dividend
        if df_div.empty:
            return
        logger.info('Generating div file')
        # get amount from div file and CA db
        files = glob(os.path.join(self.dirs.input, 'Divs gone EX*'))
        if len(files) == 0:
            logger.warning('No EX dividend file received, skipping this process')
            return pd.DataFrame()
        # assert len(files) == 1
        ex_div_file = files[0]
        if ex_div_file.endswith('.csv'):
            df_fx_div = pd.read_csv(ex_div_file, header=1)
        elif ex_div_file.endswith('.xlsx'):
            df_fx_div = pd.read_excel(ex_div_file, sheet_name=0, header=1)
        else:
            raise ValueError(f'file name unexpected for {ex_div_file}')
        df_fx_div = df_fx_div[pd.notnull(df_fx_div['Asset ID'])]
        df_fx_div = df_fx_div[['Asset ID', 'Transaction Date', 'Units/Contracts',
                               'O/S Amount (B)', 'Currency Code']]
        df_fx_div = df_fx_div[df_fx_div['Asset ID'].str.contains('/', regex=False)]
        df_fx_div.insert(len(df_fx_div.columns), 'amount',
                         df_fx_div['O/S Amount (B)'].astype(str).str.replace(',', '').astype(float) / df_fx_div[
                             'Units/Contracts'].astype(str).str.replace(',', '').astype(float))
        df_fx_div = df_fx_div[['Asset ID', 'Transaction Date', 'amount']]
        df_fx_div = df_fx_div.rename(columns={'Transaction Date': 'date'})
        df_fx_div.loc[:, 'date'] = df_fx_div['date'].apply(lambda x: pd.to_datetime(x, format='%d-%b-%y'))
        df_div = pd.merge(df_div, df_fx_div, how='left', on=['date', 'Asset ID'])
        # generate query
        mask_div = pd.notnull(df_div['amount'])
        df_div_found, df_div_missing = df_div[mask_div], df_div[~mask_div]
        if not df_div_missing.empty:
            # deal with the missing entries by querying db
            min_date, max_date = df_div_missing.date.min().strftime('%Y%m%d'), df_div_missing.date.max().strftime(
                '%Y%m%d')
            # isin list
            isin_str = ','.join([f"'{x}'" for x in df_div_missing['ISIN'].unique()])
            sql = f"""SELECT a.date, a.amount, a.franked_amount, a.net_amount, b.isin, c.code FROM corporate_action a, security b, currency c where a.security_id=b.id and a.date>='{min_date}' and a.date<='{max_date}' and a.type in ('Special Dividend', 'Dividend') and b.isin in ({isin_str}) and a.provider_id=5 and a.currency_id=c.id"""
            from dbmanager import DBManager
            db_mgr = DBManager('global_db', profile='prod', session=self.session).api
            df = db_mgr.read_sql(sql)
            df = df.rename(columns={'code': 'currency', 'isin': 'ISIN'})
            df.loc[:, 'date'] = df['date'].apply(lambda x: pd.to_datetime(str(x), format='%Y%m%d'))
            df = df[['date', 'amount', 'ISIN']]
            df_div_missing = df_div_missing.drop(columns=['amount'])
            df_div_missing = pd.merge(df_div_missing, df, how='left', on=['date', 'ISIN'])
            # further check
            mask_ca_div = pd.notnull(df_div_missing['amount'])
            df_ca_found, df_ca_missing = df_div_missing[mask_ca_div], df_div_missing[~mask_ca_div]
            pd.options.mode.chained_assignment = None
            df_ca_found.loc[:, 'amount'] = df_ca_found['amount'] / df_ca_found['rate']
            pd.options.mode.chained_assignment = 'warn'
            df_div = pd.concat([df_div_found, df_ca_found], ignore_index=True)
            if not df_ca_missing.empty:
                df_ca_missing.to_csv(os.path.join(self.dirs.output, 'missing_div.csv'), index=False)
                logger.error('Please inspect the missing dividend info in missing_div.csv file of the output folder')
        df_div = df_div[df_div['date'] >= self.dates.month_start]
        df_div.to_csv(os.path.join(self.dirs.output, 'div_report.csv'), index=False)
        # generate the dv csv file
        self.gen_div_csv(df_div)
        return df_div

    def gen_div_csv(self, df_div) -> None:
        """
        generate dividend file based on the div data
        Creates dv file combining manual processed dividend and the dividend data
        Update dv file to remote base
        :param df_div: div data generated in previous process
        :return: None
        """
        columns = ['Asset ID', 'date', 'amount']
        df = df_div.drop_duplicates(subset=columns)
        df = df.groupby(['Asset ID', 'date']) \
            .agg({'amount': 'sum'}).reset_index()
        df = df[columns]
        df.loc[:, 'Asset ID'] = df['Asset ID'].apply(lambda x: 'U_' + x.replace('/', ''))
        df.loc[:, 'amount'] = df['amount'] * 100
        df.loc[:, 'date'] = df['date'].apply(lambda x: x.strftime('%Y%m%d'))
        df.insert(0, 'id', 9)
        df.to_csv(os.path.join(self.dirs.output, 'temp_div.csv'), index=False, header=None)
        div_file = f'{self.dates.bme_YYYYMMDD}dv.csv'
        # append last lines of manual adjustment to the end
        fd_fn = f'{self.dates.rpt_month}_foreign_div.csv'
        if os.path.exists(os.path.join(self.dirs.output, 'missing_div.csv')):
            self.confirm_action(f'confirm that you have created {fd_fn} in {DATA_PATH} folder')
        manual_lines = []
        if os.path.exists(os.path.join(DATA_PATH, fd_fn)):
            logger.info('Applying manually created foreign dividends')
            shutil.copy2(os.path.join(DATA_PATH, fd_fn), os.path.join(self.dirs.output, fd_fn))
            with open(os.path.join(DATA_PATH, fd_fn), 'r') as infile:
                manual_lines = infile.readlines()
        header = f'0,DIVS,0,{self.dates.bme_YYYYMMDD},{len(df) + len(manual_lines)}\n'
        with open(os.path.join(self.dirs.output, 'temp_div.csv'), 'r') as infile:
            lines = infile.readlines()
        with open(os.path.join(self.dirs.output, div_file), 'w', newline='\n') as outfile:
            outfile.write(header)
            for line in lines + manual_lines:
                outfile.write(line)
        self.session.upload(os.path.join(self.dirs.output, div_file), REMOTE_BASE)

    def load_data(self):
        logger.info('Loading dividends')
        cur_date = pd.Timestamp.now().strftime('%Y%m%d')
        sql = f"""DELETE FROM dividend_pc WHERE enter_date={cur_date}"""
        self.db_mgr.exec_sql(sql)
        cmd = f'/projects/attribution/general/bin/newClientDiv -w -s pc {self.dates.bme_YYYYMMDD}dv.csv'
        self.session.send(cmd)
        sql = f"""UPDATE dividend_pc set franked=0 WHERE enter_date={cur_date}"""
        self.db_mgr.exec_sql(sql)
        if self.buybacks:
            if not os.path.exists(os.path.join(DATA_PATH, f'{self.dates.rpt_month}_buyback.json')):
                self.confirm_action(f'Have you added {self.dates.rpt_month}_buyback.json into folder: {DATA_PATH}')
            from clients.coopers.buyback import BuyBack
            BuyBack(self.dates.rpt_month, self.ticket).run()
            logger.info('loading buyback file')
            cmd = f'/projects/attribution/general/bin/loadPerformanceFlows -s pc {self.dates.bme_YYYYMMDD}pf.csv'
            self.session.send(cmd)
        logger.info('Committing data to DB')
        cmd = f'{BIN_BASE}/parseAndLoad.sh -w -d {self.dates.bme_YYYYMMDD}'
        lines = self.session.check_output(cmd)
        for line in lines:
            # ignore futures
            # catch errors
            if 'ERROR' in line:
                display = True
                for future in self.futures:
                    if f'Unknown Code: {future}' in line:
                        display = False
                        break
                if display:
                    logger.error(line)
            # print(line)
        logger.info('loading completed')

    def pre_load(self):
        """
        run command to create trade file and load prices for buybacks and placements
        :return:
        """
        date_str = pd.Timestamp.now().strftime('%Y%m%d%H%M%S')
        pbme_YYYYMM = self.dates.bmonth_end.strftime('%Y%m%d')
        fn = f'{date_str}_prelim_load.log'
        self.session.upload(os.path.join(PACK_PATH, 'coopers', 'resources', 'find_unlisted.sh'), '~/term')
        tickers = ' '.join(self.placements)
        logger.info('Checking unlisted securities')
        cmd = f'sh ~/term/find_unlisted.sh -d {pbme_YYYYMM} -f {fn} {tickers}'
        self.session.send(cmd)
        lines = self.session.read_file(f'~/term/{fn}')
        err_code_set = set()
        df_future = self.df_trade[self.df_trade['Transaction Code'].isin(['FU-PUR', 'FU-SAL'])]
        future_code = set(df_future['Asset ID'])
        import re
        error_code_pat = re.compile("Unknown Code: (.*) At Line")
        for line in lines:
            if 'ERROR' in line:
                err_code = error_code_pat.search(line).group(1)
                err_code_set.update({err_code})
                if err_code in future_code:
                    continue
                logger.error(line)
        logger.warning(f'Ignoring future code: {future_code}')
        err_code_set = err_code_set - future_code
        if len(err_code_set):
            logger.error(err_code_set)
        # assert len(err_code_set) == 0

    def remove_cancel_trade(self):
        """
        check cancelled trade entries and remove in pairs
        :return:
        """
        # populate trade file
        columns = ['idx', 'portfolio', 'security', 'date', 'quantity', 'price', 'unkown1', 'unkown2', 'unkown3',
                   'unkown4', 'unkown5', 'flag']
        trade_file = f'{self.dates.bme_YYYYMMDD}t.csv'
        self.session.download(f'{REMOTE_BASE}/{trade_file}')
        df_trade = pd.read_csv(os.path.join(DATA_PATH, trade_file), skiprows=1, header=None,
                               names=columns)
        df_cancel = df_trade[df_trade['flag'] == 'CANCEL']
        idx_rm = []
        for idx, row in df_cancel.iterrows():
            # find the target one and remove
            idx_rm.append(idx)
            port_mask = df_trade['portfolio'] == row['portfolio']
            sec_mask = df_trade['security'] == row['security']
            flag_mask = pd.isnull(df_trade['flag'])
            date_mask = df_trade['date'] == row['date']
            amount_mask = df_trade['unkown3'] == (row['unkown3'] * -1)
            direction_mask = df_trade['unkown5'] == row['unkown5']
            df_entries = df_trade[port_mask & flag_mask & date_mask & sec_mask & amount_mask & direction_mask]
            if len(df_entries) == 0:
                # price might be different
                # use quantity instead
                quantity_mask = df_trade['quantity'] == (row['quantity'] * -1)
                df_entries = df_trade[port_mask & flag_mask & date_mask & sec_mask & quantity_mask & direction_mask]
            assert len(df_entries) > 0
            idx_rm.append(df_entries.index[0])

        df_ret_trade = df_trade[~df_trade.index.isin(idx_rm)]
        if len(idx_rm) > 0:
            logger.info(f'Found {len(idx_rm) / 2} cancelled trades in trade file')
            df_ret_trade.to_csv(os.path.join(self.dirs.input, 't.csv'), header=None, index=False)
            # remove from header
            # change first line
            with open(os.path.join(DATA_PATH, trade_file), 'r') as header_file:
                src_lines = header_file.readlines()
            with open(os.path.join(self.dirs.input, 't.csv'), 'r') as data_file:
                lines = data_file.readlines()
            header = src_lines[0]
            elements = header.split(',')
            elements[-1] = f'{int(elements[-1].strip()) - len(idx_rm)}\n'
            new_header = ','.join(elements)
            logger.info('Writing modified trade file')
            with open(os.path.join(DATA_PATH, trade_file), 'w', newline='\n') as outfile:
                outfile.write(new_header)
                for line in lines:
                    outfile.write(line)
            self.session.upload(trade_file, REMOTE_BASE)

    def placement_price_adj(self):
        """
        check prices with head stock to cater for cases that previous close might be years ago
        :return:
        """
        if len(self.placements) == 0:
            return
        ticker_str = ','.join([f"'E-U_{x}'" for x in self.placements])
        sql = f"""SELECT instr_id, full_code FROM instrument_pc WHERE full_code in ({ticker_str}) AND end_date>={self.dates.ms_YYYYMMDD}"""
        df_instr = self.db_mgr.read_sql(sql)
        df_instr.loc[:, 'full_code'] = df_instr['full_code'].str.replace('E-U_', '')
        # assert all new placements created
        assert len(df_instr) == len(self.placements)
        for plc_ticker in self.placements:
            head_ticker = plc_ticker[:3]
            sql = f"""SELECT date, first, a.instr_id FROM stockdaily a, instrument b WHERE """\
                  f"""b.full_code='E-{head_ticker}' AND b.end_date>={self.dates.ms_YYYYMMDD} """\
                  f"""AND a.instr_id=b.instr_id AND a.date>={self.dates.ms_YYYYMMDD} """ \
                  f"""AND a.date<={self.dates.me_YYYYMMDD} ORDER BY date"""
            df_prices = self.db_mgr.read_sql(sql)
            df_prices = df_prices.drop_duplicates()
            # get placement prices
            sql = f"""SELECT date, first, a.instr_id FROM stockdaily_pc a, instrument_pc b """ \
                  f"""WHERE  b.full_code='E-U_{plc_ticker}' AND b.end_date>={self.dates.ms_YYYYMMDD}"""\
                  f""" AND a.instr_id=b.instr_id AND a.date>={self.dates.ms_YYYYMMDD} """\
                  f""" AND a.date<={self.dates.me_YYYYMMDD} ORDER BY date"""
            df_plc_prices = self.db_mgr.read_sql(sql)
            df_plc_prices = df_plc_prices.drop_duplicates()
            # check first price
            first_price = df_plc_prices.iloc[0]['first']
            first_date = df_plc_prices.iloc[0]['date']
            instr_id = df_plc_prices.iloc[0]['instr_id']
            # get head stock price
            first_head_price = df_prices[df_prices['date'] == first_date]['first'].values[0]
            if first_head_price != first_price:
                logger.warning(f'Updating prices for {plc_ticker} on {first_date} to {first_head_price}')
                sql = f"""Update stockdaily_pc SET first={first_head_price}, indfirst={first_head_price} """\
                      f""" WHERE instr_id={instr_id} AND date={first_date}"""
                self.db_mgr.exec_sql(sql)

    def run(self):
        self.restore_db()
        self.find_new_sec()
        self.process_new_sec()
        self.gen_div_file()
        self.pre_load()
        self.remove_cancel_trade()
        self.load_data()
        self.placement_price_adj()

    def test_run(self):
        # self.restore_db()
        self.find_new_sec()
        self.process_new_sec()
        self.gen_div_file()
        self.pre_load()
        self.remove_cancel_trade()
        self.load_data()
        self.placement_price_adj()


if __name__ == '__main__':
    Loader().run()
