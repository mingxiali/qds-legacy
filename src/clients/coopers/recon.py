#!/usr/bin/env python3
import os
import shutil
from glob import glob
from pathlib import Path
import pandas as pd
from pandas.tseries.offsets import BDay
from commands import DATA_PATH
from loguru import logger
from clients.base import MonthlyTask
from clients.coopers import TICKET_KW, INIT_COMMAND, PORTFOLIOS, REMOTE_BASE
from clients.util import Session


class Recon(MonthlyTask):
    """
    Generate gbst value data and create recon report
    """

    task_name = TICKET_KW
    gbst_data = pd.DataFrame()
    cust_data = pd.DataFrame()
    diff_report = pd.DataFrame()
    recon_report = pd.DataFrame()
    files = dict()

    def __init__(self, rpt_month: str = '', ticket: str = '', skip_download: bool = False):
        """
        init function
        :param rpt_month: report month to do recon
        :param ticket: ticket to access to remote machine
        :param skip_download: skip download if it is set as True
        """
        client = Path(__file__).parent.name
        super().__init__(client, rpt_month, ticket, self.task_name)
        self.session = Session(reason=self.ticket, display=False)
        self.session.send(INIT_COMMAND)
        self.skip_download = skip_download
        logger.info(f"Task {self.task_name} initialized")

    def check_return_run(self):
        logger.info('Extracting gbst return')
        cmd = f'/projects/attribution/parac/bin/checkMonthlyReturns.sh -d {self.dates.bme_YYYYMMDD}'
        self.session.send(cmd)
        cmd = f'/projects/attribution/parac/bin/checkDailyReturns.sh -b {self.dates.ms_YYYYMMDD} -e {self.dates.me_YYYYMMDD}'
        self.session.send(cmd)

    def check_valuation_run(self):
        logger.info('Extracting GBST valuation')
        for port in PORTFOLIOS:
            cmd = f'cd {REMOTE_BASE}/valuations;/projects/attribution/parac/bin/doValuations.sh P-{port} '\
                  f' {self.dates.ms_YYYYMMDD} {self.dates.me_YYYYMMDD}'
            self.session.send(cmd)

    def get_gbst_returns(self):
        """
        Get gbst return based on input data
        :return:
        """
        df_ret = pd.DataFrame()
        for port in PORTFOLIOS:
            for file in self.files[f'{port}_return']:
                df_daily = pd.read_csv(os.path.join(self.dirs.recon, file))
                df_daily = df_daily[df_daily['Name'] == 'Total Components'][['Date', 'Portfolio',
                                                                             'Total Portfolio Return']]\
                    .drop_duplicates()
                df_ret = pd.concat([df_ret, df_daily], ignore_index=True)
        df_ret = df_ret.rename(columns={'Date': 'date', 'Portfolio': 'portfolio',
                                        'Total Portfolio Return': 'gbst_daily_return'})
        df_ret.loc[:, 'date'] = df_ret['date'].apply(pd.to_datetime, format='%Y%m%d')
        df_ret = df_ret.drop_duplicates(keep='last', subset=['date', 'portfolio'])
        self.gbst_data = df_ret

    def get_cust_returns(self):
        """
        Get custodian return numbers
        :return:
        """
        # file  Perf Est.xls
        perf_file = 'Perf est*.xlsx'
        # read sheet
        files = glob(os.path.join(self.dirs.input, perf_file))
        assert len(files) == 1
        perf_file = files[0]
        df = pd.read_excel(os.path.join(self.dirs.input, perf_file), sheet_name=0)
        portfolios = [x for x in df.columns if str(x).startswith('P')]
        assert portfolios == PORTFOLIOS
        logger.info('Reading perf spreadsheet')
        # split columns
        df = df[pd.notnull(df.iloc[:, 0])]
        df = df[pd.notnull(df['Unnamed: 9'])]
        df_ret = pd.DataFrame()
        pd.options.mode.chained_assignment = None
        for idx, portfolio in enumerate(portfolios):
            data_columns = [1 + x + 3 * idx for x in range(3)]
            data_columns = [0] + data_columns
            df_sub = df.iloc[:, data_columns]
            df_sub.columns = ['date', 'NAS', 'cashflow', 'cus_daily_return']
            df_sub.loc[:, 'date'] = df_sub.apply(pd.to_datetime)
            df_sub.insert(0, 'portfolio', portfolio)
            df_ret = pd.concat([df_ret, df_sub], ignore_index=True)
        pd.options.mode.chained_assignment = 'warn'
        self.cust_data = df_ret

    def return_report(self):
        """
        Generate recon report based on gbst data, cust data and diff report
        :return:
        """
        df_gbst = self.gbst_data
        df_cust = self.cust_data
        df_hf = self.diff_report
        logger.info('Generating recon report')
        df_gbst.loc[:, 'portfolio'] = df_gbst['portfolio'].str.replace('P-', '')
        df_report = pd.merge(df_cust, df_gbst, how='left', on=['date', 'portfolio'])
        df_report.loc[:, 'cus_daily_return'] = df_report['cus_daily_return'].astype(float)
        df_report.loc[:, 'gbst_daily_return'] = df_report['gbst_daily_return'].astype(float) / 100
        diff = df_report['cus_daily_return'] - df_report['gbst_daily_return']
        df_report.insert(len(df_report.columns), 'diff', diff)
        # get total return
        min_width = 15
        df_report.loc[:, 'date'] = df_report['date'].apply(lambda x: x.strftime('%Y%m%d'))
        fn = f'{self.dates.rpt_month} Coopers Attribution Recon report.xlsx'
        f_path = os.path.join(self.dirs.output, fn)
        with pd.ExcelWriter(f_path, engine='xlsxwriter') as writer:
            ws_id = 0
            df_summary = pd.DataFrame(columns=['portfolio', 'cust_monthly', 'gbst_monthly'])
            percent_format = writer.book.add_format({'num_format': '0.00%'})
            num_fmt = writer.book.add_format({'num_format': '#,###'})
            df_report = df_report[pd.notnull(df_report['gbst_daily_return'])]
            concise_meta = dict()
            # find trading days
            td_set = df_report['date'].unique()
            for port, df in df_report.groupby(['portfolio']):
                df.to_excel(writer, sheet_name=f'{port}', freeze_panes=(1, 1), index=False)
                # find the largest break
                concise_meta[port] = df.loc[df['diff'].abs().idxmax()]
                worksheet = writer.book.worksheets_objs[ws_id]
                for col in ['E', 'F', 'G']:
                    worksheet.set_column(f'{col}:{col}', None, percent_format)
                for col in ['C', 'D']:
                    worksheet.set_column(f'{col}:{col}', None, num_fmt)
                # change width
                for idx, col in enumerate(df):  # loop through all columns
                    series = df[col]
                    max_len = max((
                        series.astype(str).map(len).max(),  # len of largest item
                        len(str(series.name)),   # len of column name/header
                        min_width
                    )) + 1
                    worksheet.set_column(idx, idx, max_len)
                cust_monthly_ret = (df['cus_daily_return'] + 1).prod() - 1
                gbst_monthly_ret = (df['gbst_daily_return'] + 1).prod() - 1
                data_dict = {'portfolio': port, 'cust_monthly': cust_monthly_ret, 'gbst_monthly': gbst_monthly_ret}
                df_summary = pd.concat([df_summary, pd.DataFrame([data_dict])], ignore_index=True)
                ws_id += 1
            df_summary.insert(len(df_summary.columns), 'diff', df_summary['cust_monthly'] - df_summary['gbst_monthly'])
            df_summary.to_excel(writer, sheet_name='summary', freeze_panes=(1, 1), index=False)
            worksheet = writer.book.worksheets_objs[ws_id]
            for col in ['B', 'C', 'D']:
                worksheet.set_column(f'{col}:{col}', min_width, percent_format)
            worksheet.activate()
            ws_id += 1
            min_width = 10
            # set colors and add filter
            port_breaks = df_summary[df_summary['diff'].abs() > 0.00024]['portfolio'].to_list()
            # worksheet1.set_tab_color('red')
            df_hf = df_hf[df_hf['date'].isin(td_set)]
            for port, df in df_hf.groupby(['portfolio']):
                # add column of holding diff chg and amount chg
                # duplicate AUD
                df = df.groupby(['portfolio', 'date', 'AssetID', 'SecurityName',
                                 'ex_rate', 'cust_price', 'gbst_price', 'unlisted']) \
                    .agg({'cust_holding': 'sum', 'gbst_holding': 'sum', 'holding_diff': 'sum',
                          'price_diff': 'sum', 'amount_diff': 'sum'}).reset_index()

                df_prev = df.copy()
                df_prev = df_prev[['date', 'AssetID', 'holding_diff', 'amount_diff']]
                df_prev = df_prev.rename(columns={'holding_diff': 'prev_holding_diff', 'amount_diff': 'prev_amount_diff'})
                df_prev.loc[:, 'date'] = df_prev['date'].apply(lambda x: (pd.to_datetime(x, format='%Y%m%d') +
                                                                          BDay(-1)).strftime('%Y%m%d'))
                # select trading days only
                df = pd.merge(df, df_prev, how='left', on=['date', 'AssetID'])
                df = df.fillna(0)
                df.insert(len(df.columns), 'holding_diff_chg', df['holding_diff'] - df['prev_holding_diff'])
                df.insert(len(df.columns), 'amount_diff_chg', df['amount_diff'] - df['prev_amount_diff'])
                df = df.drop(columns=['prev_holding_diff', 'prev_amount_diff'])
                df.to_excel(writer, sheet_name=f'{port}-diff', freeze_panes=(1, 1), index=False)
                worksheet = writer.book.worksheets_objs[ws_id]
                if port in port_breaks:
                    worksheet.set_tab_color('red')
                    # update concise report for that day
                    df_concise = df[df['date'] == concise_meta[port]['date']]
                    mask_diff_chg = (df_concise['holding_diff_chg']) > 5\
                                    & (df_concise['date'] != df_concise['date'].min())
                    df_concise = df_concise[(df_concise['holding_diff'].abs() > 5) |
                                            (df_concise['amount_diff'].abs() > 1000) | mask_diff_chg]
                    concise_meta[port] = df_concise
                else:
                    # remove concise report
                    concise_meta.pop(port)
                    worksheet.set_tab_color('green')
                worksheet.autofilter(0, 0, len(df) - 1, len(df.columns) - 1)
                for col in ['J', 'K', 'I', 'M', 'N', 'O']:
                    worksheet.set_column(f'{col}:{col}', min_width, num_fmt)
                # change width
                for idx, col in enumerate(df):  # loop through all columns
                    series = df[col]
                    max_len = max((
                        series.astype(str).map(len).max(),  # len of largest item
                        len(str(series.name)),   # len of column name/header
                        min_width
                    )) + 1
                    worksheet.set_column(idx, idx, max_len)
                ws_id += 1
            # concise report
            for port in concise_meta.keys():
                df = concise_meta[port]
                df.to_excel(writer, sheet_name=f'{port}-concise', freeze_panes=(1, 1), index=False)
                worksheet = writer.book.worksheets_objs[ws_id]
                worksheet.autofilter(0, 0, len(df) - 1, len(df.columns) - 1)
                for col in ['E', 'F', 'H', 'I', 'N', 'O']:
                    worksheet.set_column(f'{col}:{col}', min_width, num_fmt)
                # change width
                for idx, col in enumerate(df):  # loop through all columns
                    series = df[col]
                    max_len = max((
                        series.astype(str).map(len).max(),  # len of largest item
                        len(str(series.name)),   # len of column name/header
                        min_width
                    )) + 1
                    worksheet.set_column(idx, idx, max_len)
                ws_id += 1
            columns = ['id', 'portfolio', 'AssetID', 'date', 'unkown', 'price', 'quantity', 'unkown2',
                       'amount', 'ori_date', 'type', 'unkown3']
            df_trade = pd.read_csv(os.path.join(self.dirs.recon, f'{self.dates.bme_YYYYMMDD}t.csv'),
                                   skiprows=1, header=None, names=columns)
            df_trade.to_excel(writer, sheet_name='transactions', freeze_panes=(1, 1), index=False)
        shutil.copy2(f_path, os.path.join(DATA_PATH, fn))
        logger.info(f'Please check file: {fn} in {DATA_PATH}')

    def holding_report(self):
        """
        Create holding diff report based on data
        :return:
        """
        # focus on close first
        # read diffeff.txt
        from glob import glob
        df_gbst = pd.DataFrame()
        for port in PORTFOLIOS:
            df_port = pd.DataFrame()
            for file in self.files[f'{port}_value']:
                try:
                    df = pd.read_csv(os.path.join(self.dirs.recon, file))
                except pd.errors.EmptyDataError:
                    continue
                df = df[pd.notnull(df['CloseV'])]
                df.insert(0, 'portfolio', port)
                df_port = pd.concat([df_port, df], ignore_index=True)
            df_gbst = pd.concat([df_gbst, df_port], ignore_index=True)
        # read pv files
        df_cust = pd.DataFrame()
        pat = f'{self.dates.rpt_month}* - attribution pv.csv'
        for file in glob(os.path.join(self.dirs.input, pat)):
            date_str = os.path.basename(file).split('-')[0].strip()
            df = pd.read_csv(file, header=1)
            df.columns = [x.strip() for x in df.columns]
            df = df[['Pfolio', 'AssetID', 'SecurityName', 'UnitHolding', 'MarketPriceLastOther', 'ExchNtvtoBase',
                     'MktAccValueLastOther(B)']]
            df = df.rename(columns={'Pfolio': 'portfolio', 'UnitHolding': 'cust_holding',
                                    'MarketPriceLastOther': 'cust_price',
                                    'ExchNtvtoBase': 'ex_rate', 'MktAccValueLastOther(B)': 'amount'})
            df = df[df['portfolio'].isin(PORTFOLIOS)]
            df.loc[:, 'cust_holding'] = df['cust_holding'].astype(str).str.strip().replace('', '0').astype(float)
            df.loc[:, 'cust_price'] = df['cust_price'].astype(str).str.strip().replace('', '0').astype(float)
            df.loc[:, 'ex_rate'] = df['ex_rate'].astype(str).str.strip().replace('', '0').astype(float)
            df.loc[:, 'cust_price'] = df['cust_price'] / df['ex_rate']
            df.insert(0, 'date', pd.to_datetime(date_str, format='%Y%m%d'))
            df_cust = pd.concat([df_cust, df], ignore_index=True)
        # merge table
        # OMV, EMV
        df_gbst.loc[:, 'OpenV'] = df_gbst['OpenV'].astype(float)
        df_gbst.loc[:, 'CloseV'] = df_gbst['CloseV'].astype(float)
        df_gbst_daily = df_gbst.groupby(
            ['portfolio', 'Date']) \
            .agg({'OpenV': 'sum', 'CloseV': 'sum'}).reset_index()
        df_gbst_daily.to_csv(os.path.join(self.dirs.output, f'{self.dates.rpt_month}_daily_amount.csv'), index=False)
        # validate overnight mv
        df_val = df_gbst[['portfolio', 'Date', 'Code',
                          'OpenQuant', 'OpenPrice',
                          'CloseQuant', 'ClosePrice',
                          'OpenV', 'CloseV']].copy()
        df_val.loc[:, 'Date'] = df_val['Date'].apply(lambda x: pd.to_datetime(x, format='%d/%m/%Y'))
        # loop over portfolio
        df_val_re = pd.DataFrame()
        df_val = df_val.sort_values(by=['portfolio', 'Date', 'Code'])
        for port, df_port in df_val.groupby(['portfolio']):
            df_prev_day = pd.DataFrame()
            for _, df_day in df_port.groupby(['Date']):
                # find price diff
                if df_prev_day.empty:
                    df_prev_day = df_day[['portfolio', 'Date', 'Code', 'ClosePrice', 'CloseQuant', 'CloseV']]\
                        .rename(columns={'Date': 'prev_date', 'ClosePrice': 'prev_close',
                                         'CloseQuant': 'prev_quant', 'CloseV': 'prev_closeV'})
                    continue
                # inner merge on prices
                df_new = pd.merge(df_day, df_prev_day, how='outer', on=['portfolio', 'Code'])
                df_new = df_new.fillna(0)
                df_new.insert(len(df_new.columns), 'price_diff',
                              (df_new['OpenPrice'].astype(float) - df_new['prev_close'].astype(float)).round(3))
                df_new.insert(len(df_new.columns), 'quant_diff',
                              (df_new['OpenQuant'].astype(float) - df_new['prev_quant'].astype(float)).round(3))
                df_new.insert(len(df_new.columns), 'val_diff',
                              (df_new['OpenV'].astype(float) - df_new['prev_closeV'].astype(float)).round(3))
                df_val_re = pd.concat([df_val_re, df_new], ignore_index=True)
                df_prev_day = df_day[['portfolio', 'Date', 'Code', 'ClosePrice', 'CloseQuant', 'CloseV']] \
                    .rename(columns={'Date': 'prev_date', 'ClosePrice': 'prev_close',
                                     'CloseQuant': 'prev_quant', 'CloseV': 'prev_closeV'})

        df_val_re.to_csv(os.path.join(self.dirs.output, f'{self.dates.rpt_month}_price_validation.csv'), index=False)

        # extract amount
        df_cust.loc[:, 'amount'] = df_cust['amount'].astype(float)
        df_cust_daily = df_cust.groupby(['portfolio', 'date']).agg({'amount': 'sum'}).reset_index()
        df_cust_daily.to_csv(os.path.join(self.dirs.output,
                                          f'{self.dates.rpt_month}_cust_daily_amount.csv'), index=False)

        # compare mv
        df_gbst = df_gbst[['portfolio', 'Date', 'Code', 'CloseQuant', 'ClosePrice']]
        df_gbst = df_gbst.rename(columns={'Date': 'date', 'Code': 'AssetID',
                                          'CloseQuant': 'gbst_holding', 'ClosePrice': 'gbst_price'})
        df_gbst.loc[:, 'date'] = df_gbst['date'].apply(lambda x: pd.to_datetime(x, format='%d/%m/%Y'))
        # rename and merge
        df_cust.loc[:, 'AssetID'] = df_cust['AssetID'].str.strip()
        df_gbst.loc[:, 'AssetID'] = df_gbst['AssetID'].str.strip()
        df_gbst.loc[:, 'gbst_holding'] = df_gbst['gbst_holding'].apply(lambda x: pd.to_numeric(x))
        # flag unlisted
        df_gbst.insert(len(df_gbst.columns), 'unlisted', df_gbst['AssetID'].str.startswith('U_'))
        # convert custodian Asset ID
        df_cust.loc[:, 'AssetID'] = df_cust['AssetID'].apply(lambda x: x if '/' not in x else 'U_' + x.replace('/', ''))

        df_hf = pd.merge(df_cust, df_gbst, how='outer', on=['portfolio', 'date', 'AssetID'])
        df_hf = df_hf.fillna(0)
        # TODO: fix duplicates
        #
        df_hf.insert(len(df_hf.columns), 'holding_diff', df_hf['cust_holding'] - df_hf['gbst_holding'])
        df_hf.insert(len(df_hf.columns), 'price_diff', df_hf['cust_price'] - df_hf['gbst_price'])
        df_hf.insert(len(df_hf.columns), 'amount_diff',
                     df_hf['cust_holding'] * df_hf['cust_price'] - df_hf['gbst_holding'] * df_hf['gbst_price'])
        df_hf.loc[:, 'amount_diff'] = df_hf['amount_diff'].apply(lambda x: 0 if abs(x) < 50 else x)
        df_hf.loc[:, 'date'] = df_hf.date.apply(lambda x: x.strftime('%Y%m%d'))
        self.diff_report = df_hf

    def fetch_files(self):
        """
        Fetch files to recon folder including valuation files, return files and trade file
        :return:
        """
        # fetch portfolio daily file
        logger.info('Fetching files to recon folder')
        for portfolio in PORTFOLIOS:
            pattern = f'P-{portfolio}-{self.dates.rpt_month}*{self.dates.rpt_month}*.csv'
            if self.skip_download:
                port_files = sorted(glob(os.path.join(self.dirs.recon, pattern)))
                port_files = [os.path.basename(x).strip() for x in port_files]
            else:
                cmd = f'cd {REMOTE_BASE}; ls {pattern} | sort'
                output = self.session.check_output(cmd)
                port_files = [x.strip() for x in output]
            self.files[f'{portfolio}_return'] = port_files
            if not self.skip_download:
                for file in port_files:
                    self.session.download(f'{REMOTE_BASE}/{file}')
                    shutil.move(os.path.join(DATA_PATH, file), os.path.join(self.dirs.recon, file))
        for portfolio in PORTFOLIOS:
            pattern = f'P-{portfolio}-{self.dates.rpt_month}*.csv'
            if self.skip_download:
                port_files = sorted(glob(os.path.join(self.dirs.recon, pattern)))
                port_files = [os.path.basename(x).strip() for x in port_files][1::2]
            else:
                cmd = f'cd {REMOTE_BASE}/valuations; ls {pattern} | sort'
                output = self.session.check_output(cmd)
                port_files = [x.strip() for x in output]
            self.files[f'{portfolio}_value'] = port_files
            if not self.skip_download:
                for file in port_files:
                    self.session.download(f'{REMOTE_BASE}/valuations/{file}')
                    shutil.move(os.path.join(DATA_PATH, file), os.path.join(self.dirs.recon, file))
        if not self.skip_download:
            trade_file = f'{self.dates.bme_YYYYMMDD}t.csv'
            self.session.download(f'{REMOTE_BASE}/{trade_file}')
            shutil.move(os.path.join(DATA_PATH, trade_file), os.path.join(self.dirs.recon, trade_file))

    def run(self):
        self.check_return_run()
        self.check_valuation_run()
        self.fetch_files()
        self.get_cust_returns()
        self.get_gbst_returns()
        self.holding_report()
        self.return_report()

    def test_run(self):
        self.skip_download = True
        self.fetch_files()
        self.get_cust_returns()
        self.get_gbst_returns()
        self.holding_report()
        self.return_report()


if __name__ == '__main__':
    Recon().test_run()

