#!/usr/bin/env python3
import os
import shutil
from glob import glob
from pathlib import Path
from xlrd import XLRDError
import pandas as pd
from commands import DATA_PATH
from loguru import logger
from clients.base import MonthlyTask
from clients.coopers import TICKET_KW, BIN_BASE, REMOTE_BASE
from clients.util import Session


class Reformat(MonthlyTask):
    """
    Reformat coopers file and upload to the remote machine
    """

    task_name = TICKET_KW

    def __init__(self, rpt_month: str = '', ticket: str = ''):
        client = Path(__file__).parent.name
        super().__init__(client, rpt_month, ticket, self.task_name)
        self.session = Session(reason=self.ticket)
        self.rpt_month_bY = self.dates.month_start.strftime('%b %Y')
        logger.info(f"Task {self.task_name} initialized")

    def unzip(self):
        """
        Find file in DATA_PATH and extract the files to input folder
        :return:
        """
        import zipfile
        # input folder
        file_date = self.dates.month_start.strftime('%b %Y')
        filename = os.path.join(DATA_PATH, f'Attribution files {file_date}.zip')
        if not os.path.exists(filename):
            raise ValueError(f'Please download the file "Attribution files {file_date}.zip" to {DATA_PATH}')
        with zipfile.ZipFile(filename, 'r') as zip_ref:
            zip_ref.extractall(os.path.join(self.dirs.input))
        logger.info('Files unzipped')

    def rename_files(self):
        """
        Rename files: .CSV -> .csv
                      Trade Report - ->  NCS Trades
        :return:
        """
        logger.info('Renaming files')
        for file in os.listdir(os.path.join(self.dirs.input)):
            # rename CSV to csv
            if file.endswith('.CSV'):
                target_file_name = file.replace('.CSV', '.csv')
                shutil.move(os.path.join(self.dirs.input, file), os.path.join(self.dirs.input, target_file_name))
            if '  ' in file:
                target_file_name = ' '.join(file.split())
                shutil.move(os.path.join(self.dirs.input, file), os.path.join(self.dirs.input, target_file_name))
        # replace Trade Report - August 2021.csv with NCS Trades Aug 2021.csv
        for file in glob(os.path.join(self.dirs.input, "Trade Report*.csv")):
            target_file_name = file.replace('Trade Report -', 'NCS Trades')
            shutil.move(os.path.join(self.dirs.input, file), os.path.join(self.dirs.input, target_file_name))
        logger.info("file names reformatted")

    def file_check(self):
        """
        Check all weekday files are present for attribution pv file and NCS PORTFOLIO file
        :return:
        """
        # rule one: check weekdays for daily files
        logger.info('Validating files')
        bdays_set = set(self.dates.bdays)
        attr_file_set = set()
        for file in glob(os.path.join(self.dirs.input, '20[0-9]* - attribution pv.csv')):
            fn = os.path.basename(file)
            # if not
            attr_file_set.add(pd.to_datetime(fn.replace(' - attribution pv.csv', ''), format='%Y%m%d').date())
        if bdays_set != attr_file_set:
            missing_files = bdays_set - attr_file_set
            if len(missing_files):
                # file_str = ','.join(list(missing_files))
                logger.error(f'Files not matching for attribution pv files: {missing_files}')
                raise

        portfolio_set = set()
        for file in glob(os.path.join(self.dirs.input, 'NCS PORTFOLIO [0-9]*.csv')):
            fn = os.path.basename(file)
            pd.to_datetime(fn.replace('NCS PORTFOLIO ', '').replace('.csv', ''), format='%d%m%y')
            portfolio_set.add(pd.to_datetime(fn.replace('NCS PORTFOLIO ', '').replace('.csv', ''), format='%d%m%y').date())
        if bdays_set != portfolio_set:
            missing_files = bdays_set - portfolio_set
            if len(missing_files):
                # file_str = ','.join(missing_files)
                logger.error(f'Files not matching for NCS PORTFOLIO files: {missing_files}')
                raise
        # column format
        logger.info('File check completed')

    def eom_file(self):
        """
        Check end of month file. It might have different date format in the file name
        Refactor the last day trade file to create end of month file
        Remove column C and K
        :return:
        """
        # identify eom file for portfolio file
        logger.info('Processing EOM file')
        rpt_month_BY = self.dates.month_start.strftime('%B %Y')
        rpt_month_bY = self.dates.month_start.strftime('%b %Y').upper()
        rpt_month_by = self.dates.month_start.strftime('%b %y').upper()
        monthly_file = os.path.join(self.dirs.input, f'NCS PORTFOLIO {rpt_month_BY}.csv')
        if os.path.exists(os.path.join(self.dirs.input, f'NCS PORTFOLIO {rpt_month_bY}.csv')):
            os.rename(os.path.join(self.dirs.input, f'NCS PORTFOLIO {rpt_month_bY}.csv'),
                      monthly_file)
            logger.info('Renaming eom file')
        if os.path.exists(os.path.join(self.dirs.input, f'NCS PORTFOLIO {rpt_month_by}.csv')):
            os.rename(os.path.join(self.dirs.input, f'NCS PORTFOLIO {rpt_month_by}.csv'),
                      monthly_file)
            logger.info('Renaming eom file')
        month_end_dmy = self.dates.month_end.strftime('%d%m%y')
        month_end_file = os.path.join(self.dirs.input, f'NCS PORTFOLIO {month_end_dmy}.csv')
        if os.path.exists(monthly_file):
            # replace data to overwrite eom file
            import csv
            with open(monthly_file, "r") as source:
                # rdr = csv.DictReader(source)
                rdr = csv.reader(source)
                with open(os.path.join(self.dirs.input, month_end_file), "w", newline='') as result:
                    wtr = csv.writer(result)
                    for r in rdr:
                        # skip column C and column K
                        del r[10]
                        del r[2]
                        wtr.writerow(r)
        else:
            logger.error('EOM file not found')
            raise ValueError('EOM file not found')
        logger.info('EOM file created')

    def generate_fx_file(self):
        """
        Refactor FX file to get rid of duplicate data
        :return:
        """
        logger.info("Removing duplicate FX entries")
        # find the file and remove empty lines, drop duplicate
        files = glob(os.path.join(self.dirs.input, 'Forex*.xlsx'))
        assert len(files) == 1
        fn = files[0]
        xl = pd.ExcelFile(os.path.join(self.dirs.input, fn))
        sn = 'Forex'
        for index, sheet_name in enumerate(xl.sheet_names):
            if 'Forex' in sheet_name:
                sn = sheet_name
                break
        # if sheet name not provided, use the first sheet
        try:
            df = pd.read_excel(os.path.join(self.dirs.input, fn), sheet_name=sn)
        except (XLRDError, ValueError):
            df = pd.read_excel(os.path.join(self.dirs.input, fn), sheet_name=0)
        # drop duplicates and remove empty
        df = df[pd.notnull(df.iloc[:, 1])]
        df.loc[:, "Unnamed: 0"] = df["Unnamed: 0"].apply(lambda x: pd.to_datetime(x).strftime('%m/%d/%Y'))
        df = df.drop_duplicates(subset=["Unnamed: 0"])
        df = df.rename(columns={"Unnamed: 0": ""})
        df.to_csv(os.path.join(self.dirs.input, 'forex.csv'), index=False)

    def reformat_trade_file(self):
        """
        Inspect trade file and remove Primary Asset ID column if present
        :return:
        """
        logger.info("Inspecting trade files")
        trade_files = glob(os.path.join(self.dirs.input, 'NCS*rade*.csv'))
        if len(trade_files) != 1:
            raise ValueError('Trade file not found')
        trade_file = trade_files[0]
        target_file_name = f'NCS Trades {self.rpt_month_bY}.csv'
        # change the file name
        des = os.path.join(self.dirs.input, target_file_name)
        if des != trade_file:
            shutil.move(trade_file, os.path.join(self.dirs.input, target_file_name))
        df = pd.read_csv(des, header=1)
        if 'Primary Asset ID' in df.columns:
            # column number to remove
            col_to_rm = list(df.columns).index('Primary Asset ID')
            import csv
            with open(des, "rt") as source:
                rdr = csv.reader(source)
                with open(os.path.join(self.dirs.input, 'temp.csv'), "w", newline='') as result:
                    wtr = csv.writer(result)
                    for r in rdr:
                        del r[col_to_rm]
                        wtr.writerow(r)
            shutil.copy(os.path.join(self.dirs.input, 'temp.csv'), des)

        target_columns = ['Portfolio Code', 'Settlement Indicator', 'Asset ID', 'Transaction Date',
                          'Original Settlement Date', 'Units/Contracts', 'Unit Price (N)', 'Brokerage (N)',
                          'Transaction Code']
        if not set(target_columns).issubset(set(df.columns)):
            columns_not_found = ','.join(set(target_columns) - set(df.columns))
            logger.error(f'Columns {columns_not_found} not found in trade file')
            raise ValueError('Unexpected trade file')

    def replace_security_id(self):
        """
        replace SEEK with SEK for all csv files
        :return:
        """
        logger.info("Replacing SEEK with SEK")
        import fileinput
        for file in glob(os.path.join(self.dirs.input, '*.csv')):
            with open(f'{file}.tmp', 'w') as tmp_file:
                for line in fileinput.FileInput(file, inplace=1):
                    tmp_file.write(line.replace("SEEK", "SEK"))
            shutil.move(os.path.join(self.dirs.input, f'{file}.tmp'), os.path.join(self.dirs.input, file))

    def backup_files(self):
        """
        Run command to back up files on remote machine
        :return:
        """
        logger.info("Backing up files and DB")
        cmd = f"{BIN_BASE}/backup_and_clean.sh {self.dates.pbme_YYYYMMDD}"
        self.session.send(cmd)

    def copy2drive(self):
        """
        zip the folder and upload and unzip; copy file to different folder
        :return:
        """
        # copy the folder to origin
        orig_folder = f'{REMOTE_BASE}/orig/{self.dates.rpt_month}'
        output = self.session.check_output(f'[ -d {orig_folder} ] && echo OK', last_line_only=True)
        if output == 'OK':
            # orig folder already exist
            # rename to archive folder
            dt = pd.Timestamp.now().strftime('%m%d%H%m%S')
            cmd = f'mv {REMOTE_BASE}/orig/{self.dates.rpt_month} {REMOTE_BASE}/orig/{self.dates.rpt_month}-archive-{dt}'
            self.session.send(cmd)
        # zip input folder and unzip to orig folder and rm the zip file
        logger.info('Zipping files from input folder')
        os.chdir(self.dirs.output)
        shutil.make_archive(f'{self.dates.rpt_month}_input', 'zip', self.dirs.input)
        self.session.upload(os.path.join(self.dirs.output, f'{self.dates.rpt_month}_input.zip'), '~/term')
        # create folder and unzip
        cmd = f"""mkdir {orig_folder}; cd {orig_folder};"""\
              f"""unzip -o ~/term/{self.dates.rpt_month}_input.zip"""
        self.session.send(cmd)
        # copy NCS files
        cmd = f'cp NCS*.csv {REMOTE_BASE}'
        self.session.send(cmd)
        # copy attribution files
        cmd = f'cp *attribution*pv.csv {REMOTE_BASE}/PV'
        self.session.send(cmd)
        cmd = f'cp forex.csv {REMOTE_BASE}'
        self.session.send(cmd)
        logger.info('Copied reformatted folder to drive')

    def run(self):
        self.unzip()
        self.rename_files()
        self.file_check()
        self.eom_file()
        self.generate_fx_file()
        self.reformat_trade_file()
        self.replace_security_id()
        self.backup_files()
        self.copy2drive()

    def test_run(self):
        self.rename_files()
        self.eom_file()
        self.generate_fx_file()
        self.reformat_trade_file()
        self.replace_security_id()
        self.backup_files()
        self.copy2drive()


if __name__ == '__main__':
    Reformat().run()
