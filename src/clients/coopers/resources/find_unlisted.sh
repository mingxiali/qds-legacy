#!/bin/sh

Usage="$0: [-d date] [-f filename] [tickers]
    -d: date: last weekday of attribution month
    -f: filename: filename to store logs in ~/term folder
    tickers: placement tickers found in trade file
"

while getopts "d:f:" c ; do
    case $c in
        d) yyyymmdd=$OPTARG;;
        f) filename=$OPTARG;;
        *) echo "$Usage"
        exit 1;;
    esac
done

shift $((OPTIND-1))
[ "${1:-}" = "--" ] && shift

echo "Current date: $yyyymmdd, Filename: $filename, placement tickers: $@"
# Takes arguments and feed to parse_and_load.sh
export LOGNAME=parac
cd /data/attribution/parac/data
/projects/attribution/parac/bin/parseAndLoad.sh -d $yyyymmdd <<HERE > ~/term/$filename 2>&1
$@
HERE
echo "Please inspect the log in ~/term/$filename"
