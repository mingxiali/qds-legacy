#!/usr/bin/env python3
import pandas as pd
from loguru import logger
from clients.util import get_instr_id
from session import Session


class UnlistedSecurity:
    """
    Process unlisted security for coopers and solaris in legacy system
    """
    def __init__(self, ticker: str, client: str, start_date: str, session: Session):
        self.ticker = ticker
        self.client = client
        self.start_date = start_date
        self.session = session
        if client == 'coopers':
            self.client = 'pc'
        elif client == 'solaris':
            self.client = 'sw'
        else:
            raise ValueError(f'Unexpected client as {client}. Please use coopers or solaris instead')

    def create(self) -> bool:
        """
        Create new security in target client env
        if already exists, return False
        otherwise, create it and return True
        :return:
        """
        if self.ticker.startswith('U_'):
            new_ticker = self.ticker
        else:
            new_ticker = f'U_{self.ticker}'
        instr_id = get_instr_id(new_ticker, self.session)
        if instr_id is None:
            logger.info(f'Creating new security {new_ticker} in instruments_{self.client}')
            # create new with U_{ticker}
            # check description to make sure it is less than 6 digits
            date_str = pd.Timestamp.now().strftime('%Y%m%d-%H%M%S')
            fn = f'{date_str}_new_instr_{self.ticker}.log'
            des = new_ticker
            if len(des) > 8:
                des = new_ticker.replace("U_", "")[:6]
            cmd = f"""sh /data/client/mingl/scripts/new_instr.sh -s {self.start_date}""" \
                  f""" -t {self.ticker} -d {des} -f {fn} -c {self.client}"""
            lines = self.session.check_output(cmd)
            assert 'COMMIT' in lines[-1]
            logger.info(f'Security {new_ticker} created in {self.client} env')
            return True
        else:
            logger.info(f'Security {new_ticker} already exists')
            return False


if __name__ == '__main__':
    # ex: create security ZZZ for solaris that starts from 20991001
    session = Session()
    UnlistedSecurity('ZZZ', 'solaris', '20991001', session)

