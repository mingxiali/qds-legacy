#!/bin/sh

Usage="$0: [-s start] [-e end] [-t ticker] 
    -s: start start date of security in YYYYMMDD
    -e: end end date of security in YYYYMMDD
    -t: ticker security code in U_[A-Z]*
    -d: description default as ticker; remove U_ if longer than 6 digits
    -f: filename: filename to store logs in ~/term folder
    -c: client: client to use (pc for coopers and sw for solaris)
"

filename="new_instr.log"
end_date="99999999"
client=pc
while getopts "s:e:t:f:d:c:" c ; do
    case $c in
        s) start_date=$OPTARG;;
        e) end_date=$OPTARG;;
        t) ticker=$OPTARG;;
        d) des=$OPTARG;;
        f) filename=$OPTARG;;
        c) client=$OPTARG;;
        *) echo "$Usage"
        exit 1;;
    esac
done
/projects/attribution/general/bin/newInstr -s $client <<HERE > ~/term/$filename 2>&1
E-$ticker
$start_date $end_date
$des
$des
$des
HERE
