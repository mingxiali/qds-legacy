import pathlib

PACK_PATH = pathlib.Path(__file__).parent.resolve()
REMOTE_BASE = '/data/attribution/swealth/data'
BIN_BASE = '/projects/attribution/swealth/bin'
CVS_BASE = 'src/attribution/swealth/bin.sh'
TICKET_KW = 'solaris'
CLIENT = 'solaris'
INIT_COMMAND = f'export LOGNAME=swealth;cd {REMOTE_BASE};'

__all__ = [PACK_PATH, REMOTE_BASE, BIN_BASE, TICKET_KW, INIT_COMMAND, CLIENT, ]
