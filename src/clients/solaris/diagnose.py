#!/usr/bin/env python3
import pandas as pd
import os
from loguru import logger
from pandas.tseries.offsets import BDay

from clients.base import Request
from clients.solaris import INIT_COMMAND, CLIENT
from session import Session
from dbmanager import DBManager
from utils.funcs import get_issue_key


class Diagnose(Request):
    """
    Diagnose errors in FileChecksAndReformat.log
    """
    log_file = 'FileChecksAndReformat.log'
    log_dir = '/data/client/log/morning'
    data_dir = '/data/attribution/swealth/data/orig'

    def __init__(self, cur_date: str = ''):
        """
        init the process
        :param cur_date: cur date
        """
        if not cur_date:
            cur_date = (pd.Timestamp.now() + BDay(-1)).strftime('%Y%m%d')
        self.cur_date = cur_date
        reason = get_issue_key(kw=f'{CLIENT} error', default=f'{CLIENT} error')
        self.session = Session(reason=reason)
        self.session.send(INIT_COMMAND)
        self.db_mgr = DBManager('live', profile='prod', session=self.session).api
        self._issues = []
        logger.info('Diagnostic program initialized')

    def find_new_unlisted(self):
        """
        inspect the log file and find errors and create task file accordingly
        :return:
        """
        # download the file
        cmd = f"""egrep -i 'WARN|ERROR|Cannot find instr_id of' {self.log_dir}/{self.log_file} | egrep -v "WARNING: There is no transactions" """
        lines = self.session.check_output(cmd)
        if not lines:
            # get the errors and do the next step accordingly
            return self._issues
        for line in lines:
            logger.error(line)
            # add logic here to create fix tasks
            if 'Cannot find instr_id of' in line:
                # ex: Cannot find instr_id of U_D1NTO_S
                code = line.strip().split(' ')[-1]
                item = ('Unkown Code', code)
                if item not in self._issues:
                    self._issues.append(item)
            elif 'ERROR: Unknown Code: U_' in line:
                # ex: ERROR: Unknown Code: U_CMW_S At Line: 124
                code = 'U_' + line.split('U_')[1].split(' ')[0]
                item = ('Unkown Code', code)
                if item not in self._issues:
                    self._issues.append(item)
            elif 'ERROR: Zero Quantity' in line:
                # ex:  ERROR: Zero Quantity:  Line 112
                line_num = line.strip().split('Line')[-1]
                item = ('Zero Quantity', line_num)
                if item not in self._issues:
                    self._issues.append(item)
            else:
                item = ('Unkown issue', line)
                if item not in self._issues:
                    self._issues.append(item)
        return self._issues

    def find_missing_analyst(self):
        """
        find missing analyst based on the date
        :return:
        """
        sql = f"""SELECT c.instr_id, d.analyst FROM (select distinct(b.instr_id) """ \
              f""" from instrument_sw a, portfolio_holdings_sw b where a.full_code like 'P-%'"""\
              f""" and {self.cur_date} between a.start_date and a.end_date and b.date_type = 'E' """\
              f"""and {self.cur_date} between b.start_date and b.end_date and b.weight != 0)""" \
              f""" c LEFT JOIN (SELECT instr_id, analyst from analyst_sw """\
              f""" where {self.cur_date} between start_date and end_date) d """\
              f""" ON c.instr_id=d.instr_id"""
        df_port = self.db_mgr.read_sql(sql)
        sql = f"""SELECT a.instr_id, b.analyst FROM (SELECT instr_id FROM indexparticipant WHERE index_id = 11000520"""\
              f""" and {self.cur_date} between start_date and end_date )"""\
              f"""  a LEFT JOIN (SELECT instr_id, analyst from analyst_sw"""\
              f""" where {self.cur_date} between start_date and end_date) b ON a.instr_id=b.instr_id"""
        df_index = self.db_mgr.read_sql(sql)
        df = pd.concat([df_port, df_index], ignore_index=True)
        df = df[pd.isnull(df['analyst'])]
        if not df.empty:
            # instr_id list
            for instr_id in df['instr_id'].unique():
                cmd = f'instr -c {instr_id}'
                code = self.session.check_output(cmd, last_line_only=True)
                item = ('Unknown analyst', code)
                if item not in self._issues:
                    self._issues.append(item)

    def find_missing_file(self):
        """
        Check files in data folder with expected file list
        Add to issue if missing file found
        :return:
        """
        # find file list of today
        expected_files = {f'{self.cur_date}_TXN.csv', f'{self.cur_date}_SECURITY.csv', f'{self.cur_date}_PERFFLOW.csv',
                          f'{self.cur_date}_LOSTCRED.csv', f'{self.cur_date}_CASH.csv', f'{self.cur_date}_BALOPEN.csv',
                          f'{self.cur_date}_BALCLOSE.csv'}
        cmd = f'ls {self.data_dir}/{self.cur_date}_*.csv'
        lines = self.session.check_output(cmd)
        lines = [os.path.basename(line.strip()) for line in lines]
        missing_files = ','.join(expected_files - set(lines))
        if missing_files:
            logger.error(f'Missing file found as {missing_files} for {self.cur_date}')
            self._issues.append(('Missing files', missing_files))

    def find_abnormal_trade(self):
        files = ['diffacc.txt', 'diffeff.txt', 'diffeffacc.txt']
        # refine lines
        ret_lines = set()
        for file in files:
            cmd = f"""egrep -v "\-\-\-" {self.data_dir}/{file} | egrep -v "Database" | egrep -v "File:" """ \
                  f"""| egrep -v "The following" """
            lines = self.session.check_output(cmd)
            # should be empty
            for line in lines:
                if line.strip():
                    logger.error('Unexpected data found from the input file')
                    logger.error(line)
                    ret_lines.add(line)

        if ret_lines:
            msg = '|'.join(ret_lines)
            self._issues.append(('unexpected data', msg))

    @property
    def issues(self):
        return self._issues

    def run(self):
        self.find_missing_file()
        self.find_new_unlisted()
        self.find_missing_analyst()
        self.find_abnormal_trade()
        return self._issues


if __name__ == '__main__':
    Diagnose().run()
