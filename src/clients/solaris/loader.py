#!/usr/bin/env python3
import pandas as pd
from pandas.tseries.offsets import BDay
from loguru import logger

from clients import DEFAULT_KWS
from clients.base import Request
from clients.solaris import INIT_COMMAND, CLIENT
from session import Session
from utils.funcs import get_issue_key


class Loader(Request):
    # Load data and inspect the log
    log_file = 'FileChecksAndReformat.log'
    log_dir = '/data/client/log/morning'

    def __init__(self, cur_date: str = ''):
        if not cur_date:
            cur_date = (pd.Timestamp.now() + BDay(-1)).strftime('%Y%m%d')
        self.cur_date = cur_date
        reason = get_issue_key(kw=f'{CLIENT} error', default=f'{CLIENT} error')
        self.session = Session(reason=reason)
        self.session.send(INIT_COMMAND)
        logger.info(f'Loader initialized for {self.cur_date}')

    def run(self):
        # run the command and inspect the result
        self.session.send(f'/projects/attribution/swealth/bin/FileChecksAndReformat.sh -w -d {self.cur_date}'
                          f' > {self.log_dir}/{self.log_file} 2>&1')
        # check output
        from clients.solaris.diagnose import Diagnose
        issues = Diagnose(self.cur_date).issues
        if len(issues):
            for issue in issues:
                # resolve issues accordingly
                #
                logger.error('Please inspect issue: ', issue)
            return
        # rerun the process
        self.check_process(DEFAULT_KWS, self.session)
        cmd = f'/projects/attribution/swealth/bin/DailyReports.sh -d {self.cur_date}'
        self.session.screen_run(cmd)


if __name__ == '__main__':
    Loader().run()
