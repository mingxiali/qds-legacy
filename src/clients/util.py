from importlib import import_module
from typing import Optional

import fiscalyear
import pandas as pd
from loguru import logger
from pandas.tseries.offsets import MonthEnd, BMonthEnd, BMonthBegin
from exchange_calendars import get_calendar
import os

from commands import DATA_PATH
from dbmanager.manager import get_db_creds
from session import Session


class MonthlyReportDates:
    """
    Create date variables in different format in one class
    including:
        previous month
        current month
        fiscal year
        holidays
    """

    def __init__(self, rpt_month: str = ''):
        if not rpt_month:
            rpt_month = (pd.Timestamp.now() - pd.DateOffset(months=1)).strftime('%Y%m')
        self.rpt_month = rpt_month
        self.month_start = pd.to_datetime(rpt_month, format="%Y%m")
        self.month_end = MonthEnd().rollforward(self.month_start)
        self.bmonth_start = BMonthBegin().rollforward(self.month_start)
        self.bmonth_end = BMonthEnd().rollforward(self.month_start)
        # previous month
        self.previous_month_start = self.month_start - pd.DateOffset(months=1)
        self.pre_rpt_month = self.previous_month_start.strftime('%Y%m')
        self.previous_month_end = MonthEnd().rollback(self.month_start)
        self.previous_bmonth_start = BMonthBegin().rollforward(self.previous_month_start)
        self.previous_bmonth_end = BMonthEnd().rollforward(self.previous_month_start)
        # next month
        self.next_month_start = self.month_start + pd.DateOffset(months=1)
        self.nms_YYYYMMDD = self.next_month_start.strftime('%Y%m%d')
        self.month_MMYYYY = self.month_start.strftime('%m%Y')
        self.ms_YYYYMMDD = self.month_start.strftime('%Y%m%d')
        self.me_YYYYMMDD = self.month_end.strftime('%Y%m%d')
        self.me_DDMMYYYY = self.month_end.strftime('%d%m%Y')
        self.pme_DDMMYYYY = self.previous_month_end.strftime('%d%m%Y')
        self.pbme_DDMMYYYY = self.previous_bmonth_end.strftime('%d%m%Y')
        self.pbme_YYYYMMDD = self.previous_bmonth_end.strftime('%Y%m%d')
        self.pbms_YYYYMMDD = self.previous_bmonth_start.strftime('%Y%m%d')
        self.bms_YYYYMMDD = self.bmonth_start.strftime('%Y%m%d')
        self.bme_YYYYMMDD = self.bmonth_end.strftime('%Y%m%d')
        self.bms_DDMMYYYY = self.bmonth_start.strftime('%d%m%Y')
        self.bme_DDMMYYYY = self.bmonth_end.strftime('%d%m%Y')
        self.ms_DDMMYYYY = self.month_start.strftime('%d%m%Y')
        # fiscal year
        fiscalyear.START_MONTH = 7
        self.fy = fiscalyear.FiscalDate(self.month_start.year,
                                        self.month_start.month, self.month_start.day).fiscal_year

        self.fy_start = BMonthBegin().rollforward(pd.to_datetime(f'0107{self.fy - 1}', format='%d%m%Y'))
        self.fy_start_YYYYMMDD = self.fy_start.strftime('%Y%m%d')
        self.fy_start_YYYYMM = self.fy_start.strftime('%Y%m')
        self.fy_start_DDMMYYYY = self.fy_start.strftime('%d%m%Y')
        self.fy_end = BMonthEnd().rollforward(pd.to_datetime(f'0106{self.fy}', format='%d%m%Y'))
        self.fy_end_YYYYMMDD = self.fy_end.strftime('%Y%m%d')
        self.fy_end_YYYYMM = self.fy_end.strftime('%Y%m')
        self.fy_end_DDMMYYYY = self.fy_end.strftime('%d%m%Y')

        # holidays
        self.bdays = [x.date() for x in pd.bdate_range(start=self.month_start, end=self.month_end)]
        aux = get_calendar('XASX')
        aux_trade_days = pd.DataFrame(data={'date': [pd.to_datetime(str(x)[:10]) for x in aux.schedule.index]})
        aux_trade_days.index = aux_trade_days.date.values
        self.trading_days = [pd.to_datetime(str(x)[:10])
                             for x in aux_trade_days.loc[self.month_start:self.month_end].index]
        self.holidays = [pd.to_datetime(str(x)[:10])
                         for x in pd.bdate_range(start=self.month_start, end=self.month_end)
                         if x not in self.trading_days]


class MonthlyReportDirs:
    """
    Create input and output folder
    """

    def __init__(self, client: str, rpt_month: str = ''):
        # define variables in
        if not rpt_month:
            rpt_month = (pd.Timestamp.now() - pd.DateOffset(months=1)).strftime('%Y%m')
        self.client = client
        self.rpt_month = rpt_month
        self.local_base = getattr(import_module(f'clients.{self.client}'), 'PACK_PATH')
        self.remote_base = getattr(import_module(f'clients.{self.client}'), 'REMOTE_BASE')
        self.bin_base = getattr(import_module(f'clients.{self.client}'), 'BIN_BASE')
        self.create_folders()

    def create_folders(self):
        """
        create input and output folders if not exist
        :return: None
        """
        for folder in ['input', 'output']:
            folder_path = self.local_base / folder / self.rpt_month
            if not folder_path.exists():
                os.makedirs(folder_path, exist_ok=True)
            self.__dict__[folder] = folder_path
        if self.client == 'coopers':
            # create recon folder
            folder_path = self.local_base / 'output' / self.rpt_month / 'recon'
            if not folder_path.exists():
                os.makedirs(folder_path, exist_ok=True)
            self.__dict__['recon'] = folder_path


def get_instr_id(ticker: str, session: Optional[Session] = None, cmd_prefix: str = '') -> Optional[str]:
    """
    get instr id based on ticker
    :param ticker: ticker or instr_id
    :param session: session instance to connect to remote machine
    :param cmd_prefix: export variables in prefix ex: LOGNAME=swealth;
    :return: instr_id or none
    """
    if session is None:
        session = Session()
        session.send(cmd_prefix)
    cmd = f'instr {ticker}'
    output = session.check_output(cmd)
    try:
        if output[-1].startswith('Cannot find instr_id'):
            return None
        elements = output[0].split(' ')
        elements = list(filter(None, elements))
        return elements[1]
    except IndexError:
        return None


def diff_check(src: str, des: str, session: Session) -> None:
    """
    Diff check between files
    :param src: source file
    :param des: des file
    :param session: session to run commands
    :return: None
    """
    cmd = f'diff {src} {des}'
    output = session.check_output(cmd)
    for line in output:
        logger.warning(line)


def insert_file_before_kw(session: Session, fn: str, src: str, kw: str, offset=0) -> None:
    """
    Insert file to src file after kw line num + offset
    create a file in ~/term with the same fn
    :param session: Session to send command
    :param fn: file name to update
    :param src: hack snippet to insert into the script
    :param kw: keyword to find the target line with grep command
    :param offset: offset line number
    :return: None
    """
    output = session.check_output(f"""grep -n "{kw}" {fn} | cut -d ':' -f1""")
    line_num = int(output[-1]) + offset
    output = session.check_output(f"""wc -l < {fn}""")
    end_line = int(output[-1]) - int(line_num) + 1
    cmd = f"""(head -{int(line_num) - 1} {fn} && cat ~/term/{src} && tail -{end_line} {fn}) > ~/term/{fn} 2>&1"""
    session.send(cmd)


class Session:

    def __init__(self, reason='auto', display=True, login_user='qdsclient'):
        self.reason = reason
        self.display = display
        self.login_user = login_user

    def send(self, cmd):
        print(f"Please send command as {self.login_user}: => ")
        print(cmd)
        print('Please do it manually')

    def check_output(self, cmd, last_line_only=True):
        print(f"get result from cmd as {self.login_user} => ")
        print(cmd)
        return cmd

    @staticmethod
    def upload(file, des):
        print(f'upload file {file} to {des}')
        print('Please do it manually')

    @staticmethod
    def download(file):
        print(f'download file {file} to DATA_PATH')
        print('Please do it manually')

    @staticmethod
    def read_file(file):
        Session().download(file)
        fn = os.path.basename(file)
        with open(os.path.join(DATA_PATH, fn), 'r') as f:
            lines = f.readlines()
        return lines


class DBManager:

    def __init__(self, name='live', profile='prod'):
        self.name = name
        self.profile = profile
        self.cred = get_db_creds(f'{name}_{profile}')

    def read_sql(self, sql):
        print(f'execute this line and get data from {self.name} of {self.profile} =>  '
              f'{sql}')
        return pd.DataFrame()

    def exec_sql(self, sql):
        print(f'execute this line and get data from {self.name} of {self.profile} =>  '
              f'{sql}')
        return pd.DataFrame()


if __name__ == '__main__':
    # md = MonthlyReportDates()
    dirs = MonthlyReportDirs('coopers')
