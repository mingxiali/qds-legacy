#!/usr/bin/env python3
from clients.base import Request
from loguru import logger
from clients.vanguard import CLIENT
from dbmanager import DBManager
from session import Session
import pandas as pd
from commands import DATA_PATH
import os
# Fetch data from holding mismatches table
# check the latest date and fetch previous changes
# attach security id
# attach corporate action and security


class Recon(Request):
    unit_filter = 1000
    # TODO: set this to empty to do recon on all portfolios
    # portfolios = ["8510", "8500", "8529", "A229"]
    portfolios = ["8500"]
    top = 10

    def __init__(self, profile: str = 'uat', refine: bool = True, reason: str = ''):
        """
        init recon instance
        :param profile: profile to access to remote machine database
        :param refine: if refine as true, only breaks with greater scale than unit filter will be addressed
        :param reason: reason to access
        """
        self.profile = profile
        self.refine = refine
        if not reason:
            reason = f'{CLIENT} recon'
        self.reason = reason
        if self.profile == 'uat':
            db_name = f'{CLIENT.lower()}uat'
        else:
            db_name = CLIENT.lower()
        self.session = Session(machine='qds-devel2', reason=self.reason)
        self.client_mgr = DBManager(db_name, profile=self.profile, session=self.session).instance
        self.glb_mgr = DBManager('global_db', profile=self.profile, session=self.session).instance
        self.data = self.get_data()
        self.db_trade = pd.DataFrame()
        self.file_trade = pd.DataFrame()
        self.sec_mapping = pd.DataFrame()
        logger.info(f'Recon initialized for {CLIENT}')

    def download_files(self):
        file_folder = '/home/qdsbatch/uat/vanguard/loader/data'
        # TODO: find correspondent files automatically
        files = [
            'vanguard_Holdings_20220405_20220406.csv',  # holding file
            'vanguard_Tax_Lots_Total_20220331_20220406.csv',  # CGT tax lots
            'vanguard_Mapping_20220331_20220406.csv',  # CGT mapping file
            'vanguard_Historical_Tax_Lots_Total_20211231_20220105.csv',  # concatenated file for franking picture
            # only trade date > 20220331 applied
            'vanguard_Hist_Transactions_20211231_20220405_20220406.csv',  # franking picture transactions
            'vanguard_Historical_Mapping_20211231_20220107.csv'    # franking picture mapping file
        ]
        for file in files:
            self.session.download(f'{file_folder}/{file}', file, login_user='qdsbatch', machine='qds-taxanalyser-dr-2')
        logger.info('files downloaded to DATA PATH')

    def get_data(self):
        # TODO: find the name automatically
        self.sec_mapping = pd.read_csv(os.path.join(DATA_PATH, 'vanguard_Historical_Mapping_20211231_20220107.csv'))
        self.sec_mapping = self.sec_mapping.rename(columns={'Security Number': 'sec_num', 'ISIN': 'isin',
                                                            'Vanguard Identifier': 'v_id'})
        # test duplicates
        self.sec_mapping = self.sec_mapping.dropna(how='any', axis='rows')
        self.sec_mapping = self.sec_mapping.drop_duplicates(subset=['isin', 'sec_num'], keep='first')
        # sec_duplicates = self.sec_mapping.duplicated(subset=['isin'])
        # dup_isins = self.sec_mapping[sec_duplicates]['isin'].unique()
        self.sec_mapping = self.sec_mapping[['sec_num', 'isin', 'v_id']].drop_duplicates(subset=['isin'], keep='first')
        sql = """SELECT a.portfolio, c.code as portfolio_code, a.date, a.securityid, b.code, b.company,b.isin, b.provider, a.type, """\
              """a.gbstholding, a.custholding FROM holdingmismatches a, securitycode b, portfolio c """ \
              """WHERE to_char(a.date, 'YYYYMMDD')::integer BETWEEN b.startdate AND b.enddate """ \
              """ AND a.securityid=b.securityid AND b.provider not in ('sandp', 'asx_temp') AND a.portfolio=c.id"""
        df = self.client_mgr.read_sql(sql)
        # merge with sec mapping
        # df = pd.merge(df, self.sec_mapping, how='left', on='isin')
        # test_isin = df[pd.isnull(df['isin'])]
        # assert len(test_isin) == 0
        df = df.fillna(0)
        df.insert(len(df.columns), 'diff', df['custholding'] - df['gbstholding'])
        if self.refine:
            if self.portfolios:
                df = df[df['portfolio_code'].isin(self.portfolios)]
            # only find securities with max date
            max_date = df['date'].max()
            df_breaks = df[(df['date'] == max_date) & (df['diff'].abs() > self.unit_filter)]
            # unique check for portfolio and security
            df_unique = df_breaks[['portfolio_code', 'securityid', 'type']].drop_duplicates()
            assert (len(df_breaks) == len(df_unique))
            # if top not zero, select up to top amount of securities to check
            df_breaks = df_breaks.sort_values(by='diff', key=abs, ascending=False)
            if self.top > 0:
                selected_idx = min(self.top, len(df_breaks))
                df_breaks = df_breaks.iloc[:selected_idx, :]
            # based on breaks, find historical changes
            df_recon = pd.DataFrame()
            df_breaks = df_breaks.sort_values(by='date')
            for idx, row in df_breaks.iterrows():
                portfolio = row['portfolio_code']
                security_id = row['securityid']
                type_str = row['type']
                entries = df[(df['portfolio_code'] == portfolio) &
                             (df['securityid'] == security_id) &
                             (df['type'] == type_str)]

                df_recon = pd.concat([df_recon, entries], ignore_index=True)
            df = df_recon
        return df

    def validate_trade(self):
        # TODO: work out how to find the file name accordingly
        # filter out the rows with trading date less than 20211231
        fn = 'vanguard_Hist_Transactions_20211231_20220405_20220406.csv'
        df_trans = pd.read_csv(os.path.join(DATA_PATH, fn), dtype=str)
        # columns
        columns = ['PfolioID', 'Security Code', 'ISIN',
                   'TradeDate', 'EntryDate', 'TradePrice',
                   'Quantity', 'PurchaseOrSaleIndicator',
                   'ReversalCrossRefMemo', 'Corp Action Type', 'Brokerage']
        if self.refine and self.portfolios:
            df_trans = df_trans[df_trans['PfolioID'].isin(self.portfolios)]
        df_trans = df_trans[df_trans['PurchaseOrSaleIndicator'].isin(('P', 'S'))]
        df_trans = df_trans[columns]
        # using trade date in the system
        # date_mismatch = df_trans[df_trans['TradeDate'] != df_trans['EntryDate']]
        # empty_isin = df_trans[pd.isnull(df_trans['ISIN'])]
        # reformat the columns
        df_trans.loc[:, 'Quantity'] = df_trans['Quantity'].astype(float)
        df_trans.loc[:, 'TradePrice'] = df_trans['TradePrice'].astype(float)
        df_trans.loc[:, 'ReversalCrossRefMemo'] = df_trans['ReversalCrossRefMemo'].astype(int)
        self.file_trade = df_trans

    def find_inspecie(self):
        # check the transactions in the file
        df_pairs = list()
        for meta, df in self.file_trade.groupby(['TradeDate', 'PfolioID', 'Security Code']):
            if len(df) < 2:
                continue
            looped_idx = []
            for idx, row in df.iterrows():
                # the same security of opposite quantity
                sec_mask = df['Security Code'] == row['Security Code']
                quantity_mask = df['Quantity'] == -1 * row['Quantity']
                price_mask = df['TradePrice'] == row['TradePrice']
                visited_mask = ~df.index.isin(looped_idx)
                if idx not in looped_idx:
                    # search for the target
                    target_rows = df[sec_mask & quantity_mask & visited_mask & price_mask]
                    if len(target_rows):
                        target_idx = target_rows.index[0]
                        looped_idx.append(idx)
                        looped_idx.append(target_idx)
                        rev_list = sorted([(idx, df.loc[idx, 'ReversalCrossRefMemo']),
                                           (target_idx, df.loc[target_idx, 'ReversalCrossRefMemo'])],
                                          key=lambda x: x[1])
                        if rev_list[0][1] == 0 and rev_list[1][1] > 0:
                            # second idx needs to be updated
                            fix_idx = rev_list[0][0]
                            self.file_trade.loc[fix_idx, 'ReversalCrossRefMemo'] = 9999
                        df_pairs.append(df.loc[[idx, target_idx]])
        if len(df_pairs):
            df_total = pd.concat(df_pairs, ignore_index=True)
            df_total.to_csv(os.path.join(DATA_PATH, 'reversal.csv'), index=False)
        self.file_trade.to_csv(os.path.join(DATA_PATH, 'transaction.csv'), index=False)
        return df_pairs

    def report(self):
        # loop over each break
        # attach transactions and prelimtrade
        # corporate actions
        df_report = pd.DataFrame()
        with pd.ExcelWriter(os.path.join(DATA_PATH, 'Vanguard Recon report.xlsx'),
                            engine='xlsxwriter') as writer:
            self.data.to_excel(writer, sheet_name='summary', freeze_panes=(1, 1), index=False)
            for idx, row in self.data.iterrows():
                tx_date = row['date'].replace('-', '')
                portfolio = row['portfolio_code']
                portfolio_id = row['portfolio']
                security_id = row['securityid']
                portfolio_mask = portfolio == self.file_trade['PfolioID']
                isin_mask = row['isin'] == self.file_trade['ISIN']
                date_mask = self.file_trade['TradeDate'].values[0] <= tx_date
                trans = self.file_trade[portfolio_mask & isin_mask & date_mask]
                trans = trans.sort_values(by=['TradeDate', 'Quantity'])
                start_date = trans['TradeDate'].min()
                # db
                tl_type = row['type'].split(' ')[0].lower()
                sql = f"""SELECT date, filled, isbuy, price, quantity, brokerage, securityid FROM prelimtrade WHERE dtype='{tl_type}'""" \
                      f""" AND date<='{tx_date}' AND date>='{start_date}' AND portfolio={portfolio_id} """ \
                      f""" and securityid={security_id}"""
                db_trans = self.client_mgr.read_sql(sql)
                db_trans = db_trans.rename(columns={'date': 'TradeDate', 'isbuy': 'PurchaseOrSaleIndicator',
                                                    'brokerage': 'Brokerage', 'price': 'TradePrice',
                                                    'quantity': 'Quantity'})
                db_trans.loc[:, 'TradeDate'] = db_trans['TradeDate'].apply(lambda x: pd.to_datetime(x).strftime('%Y%m%d'))
                db_trans.loc[:, 'PurchaseOrSaleIndicator'] = db_trans['PurchaseOrSaleIndicator']\
                    .apply(lambda x: 'P' if x == 't' else 'S')
                db_trans.insert(0, 'source', 'db')
                trans.insert(0, 'source', 'file')
                df_tab = pd.concat([db_trans, trans], ignore_index=True)
                df_tab = df_tab.sort_values(by=['TradeDate', 'PurchaseOrSaleIndicator', 'Quantity'])
                tab_name = f'{portfolio}_{tl_type}_{security_id}'
                df_tab.to_excel(writer, sheet_name=f'{tab_name}', freeze_panes=(1, 1), index=False)
                df_report = pd.concat([df_report, df_tab], ignore_index=True)
        return df_report

    def run(self):
        # self.download_files()
        self.validate_trade()
        self.find_inspecie()
        self.report()


if __name__ == '__main__':
    Recon(refine=True).run()
