### Warakirri instructions
Instruction on how to process warakirri requests
#### Pre-requests
1. Install dependencies
2. Set up settings.ini
3. Set up db.json for live db in prod
#### Inspecie
1. download the inspecie file to $DATA_PATH
2. trigger the command with command
    ```bash
    bash task -n wara_inspecie -m "{\"bk_code\": \"TEST\", \"bk_stock_date\": \"date_in_YYYYMMDD\"}"
    ```
3. Alternatively, update the parameter in inspecie.py under __main__ section and debug the script