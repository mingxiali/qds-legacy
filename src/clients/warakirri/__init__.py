import pathlib

CLIENT = 'Warakirri'
DATA_BASE = '/data/aftertax/warakirri'
BIN_BASE = '/projects/aftertax/bin'
COMMON_BASE = '/data/common'
SOURCE_PATH = 'src/aftertax/scripts'
PACK_PATH = pathlib.Path(__file__).parent.resolve()
BUYBACK_MAIL = {
    'to': 'Huan.Bui@warakirri.com.au',
    'cc': 'QDS@gbst.com',
    'greeting': 'Huan',
    'body': 'The buybacks have now been processed.  Let me know if there are any issues.',
    'subject': 'Buyback updates for {{code}}'
}

CASHFLOW_MAIL = {
    'to': 'Huan.Bui@warakirri.com.au',
    'cc': 'QDS@gbst.com',
    'greeting': 'Huan',
    'body': 'Those cashflows have now been processed and the results uploaded to the FTP site.  Let me know if there are any issues.',
    'subject': 'Cashflow updates for {{date}}'
}

METHODS = ['FIFO', 'LOW_GAIN', 'HIFO', 'HC_12M', 'HIGH_GAIN']

__all__ = ['CLIENT', 'DATA_BASE', 'BIN_BASE', 'COMMON_BASE', 'PACK_PATH', 'BUYBACK_MAIL',
           'SOURCE_PATH', 'CASHFLOW_MAIL', 'METHODS', ]
