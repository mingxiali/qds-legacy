import os

import pandas as pd
from loguru import logger
from clients.warakirri import CLIENT, PACK_PATH, METHODS
from clients.base import Request
from commands import DATA_PATH
from clients.util import Session, DBManager
from pandas.tseries.offsets import BDay
from clients.util import get_instr_id
from clients import RELEASE_ROOT
from utils.funcs import get_issue_key, decode_pass


def clean_up(bk_id):
    # clean up the instr
    logger.info(f'Cleaning up {bk_id}')
    acc_bk_id = int(bk_id) + 20000
    sql = f"""delete from indexprovider where instr_id = {bk_id};
                delete from  indexdaily where instr_id = {bk_id};
                delete from  indexdaily where instr_id = {acc_bk_id};
                delete from  histavail where instr_id = {bk_id};
                delete from  histavail where instr_id = {acc_bk_id};
                delete from indexparticipant where index_id = {bk_id};
                delete from quantdb..atb_cash_float where index_id = {bk_id};
                delete from quantdb..atb_portfolio_amv where index_id = {bk_id};
                delete from quantdb..atb_tax_lots where index_id = {bk_id};
                delete from quantdb..atb_tax_rates where index_id={bk_id};
                delete from instrument where instr_id={bk_id};
                delete from instrument where instr_id={acc_bk_id}"""
    # session = Session(reason='automate')
    db_mgr = DBManager('live', profile='prod')
    db_mgr.exec_sql(sql)
    logger.warning(f'Please check the folder for {bk_id} under /data/aftertax/warakirri and remove it;'
                   f' And remove the row in /data/aftertax/warakirri/benchmark.list')
    logger.info(f'{bk_id} cleaned up successfully!')


class BenchmarkRequest(Request):
    """
    Process warakirri new benchmark request
    the tax lot file should be downloaded to DATA_DIR, and provide corresponding meta data
    ex:
        bk_code: SUA42
        start_date: 20211103
        performance_date: 20211104
        benchmark: 'S&P/ASX300'
        tax_lot_selection_method: 'HC_12M'
        tax_code: SUPER
        file: SUA43_031121_Startup.csv
    All dates should be in YYYYMMDD
    If cashflow file provided, process as cashflow request after benchmark has been set up properly
    Note:
        for tax selection method, 'LT' will be regarded as HC_12M
        for tax code parameter,'exempt' will be interpreted as PENSION
    """

    def __init__(self, bk_code: str, start_date: str, perf_date: str, benchmark: str,
                 tax_lot_selection_method: str, tax_code: str, bk_instr: str = '', file: str = ''):
        """
        init the request
        :param bk_code: new warakirri benchmark code
        :param start_date: start date
        :param perf_date: performance date
        :param benchmark: S&P benchmark code
        :param tax_lot_selection_method: tax lot selection method
        :param tax_code: tax code
        :param bk_instr: benchmark instrument id
        :param file: file
        """
        self.bk_code = bk_code
        self.start_date = start_date
        self.perf_date = perf_date
        self.benchmark = benchmark
        self.base_bk_code = ''
        self.tax_lot_selection_method = tax_lot_selection_method
        self.tax_code = tax_code
        self.bk_instr = bk_instr
        self.file = file
        ticket = get_issue_key(kw=f'{CLIENT} benchmark', default=f'{CLIENT} benchmark')
        self.session = Session(reason=ticket)
        self.build_sh = Session(login_user='qdsbuild', reason=ticket)
        self.db_mgr = DBManager()
        self.validate()
        logger.info(f'{CLIENT} new benchmark initialized for {self.bk_code}')

    def validate(self) -> None:
        """
        check class members, rename input fields
        :return:
        """
        if not self.file:
            # SUA43_031121_Startup.csv
            start_dt = pd.to_datetime(self.start_date, format='%Y%m%d').strftime('%d%m%y')
            self.file = f'{self.bk_code}_{start_dt}_Startup.csv'
        if not os.path.exists(os.path.join(DATA_PATH, self.file)):
            logger.error(f'Please download the tax lot file to {DATA_PATH} as {self.file}')
            raise FileNotFoundError(self.file)
        if self.start_date >= self.perf_date:
            logger.warning('Performance date should be later than the start date,'
                           ' now moving it to the next business day')
            self.perf_date = (pd.to_datetime(self.start_date, format='%Y%m%d') + BDay(1)).strftime('%Y%m%d')
        # check if bk code already exists
        instr_id = get_instr_id(f'B-{self.bk_code}', self.session)
        if instr_id is not None:
            raise ValueError(f'Code {self.bk_code} already exists in the system. Please use another code instead')
        if self.tax_lot_selection_method == 'LT':
            self.tax_lot_selection_method = 'HC_12M'
        if 'exempt' in self.tax_code.lower():
            self.tax_code = 'PENSION'
        if not self.bk_instr:
            cmd = 'index_id=$(($(instr -i `instr -N 12 | tail -1`) + 1 )); echo $index_id'
            self.bk_instr = self.session.check_output(cmd, last_line_only=True)
        # fetch base benchmark code
        bk_des = self.benchmark.replace('ASX', 'ASX ') \
            .replace('Accumulation Index', '') \
            .replace('Small Ordinaries', 'SMALL ORDINARIES').replace('  ', ' ').strip()
        sql = f"""SELECT instr_id, full_code, sec_desc FROM instrument where sec_desc like '{bk_des}' and not sec_desc like '%)' and full_code like 'I-%'  and not full_code like '%2'"""
        df = self.db_mgr.read_sql(sql)
        assert len(df) == 1
        full_code = df.loc[0, 'full_code']
        self.base_bk_code = full_code
        # tax lot files
        with open(os.path.join(DATA_PATH, self.file), 'r') as infile:
            lines = infile.readlines()
        # header
        header = lines[0].split(',')
        if header[0].strip() != self.bk_code:
            logger.warning(f'Please check the tax parcel as the benchmark code {header[0]} is not expected')
        # assert header[0].strip() == key
        assert type(float(header[1])) == float
        assert type(float(header[2])) == float
        # body
        with open(os.path.join(DATA_PATH, self.file), 'w') as outfile:
            for line in lines:
                empty_num = 9 - len(line.strip().split(','))
                if empty_num:
                    line = ','.join(line.strip().split(',') + empty_num * ['']) + '\n'
                line = line.replace('/', '-')
                outfile.write(line)
        df_tl = pd.read_csv(os.path.join(DATA_PATH, self.file),
                            header=None, skiprows=1)
        # assert set(df_tl[0].unique()) in {key, self.rename[key]}
        assert str(df_tl[1].astype(str).dtype) == 'object'
        assert str(df_tl[2].astype(float).dtype) in ('float', 'float64')
        col_date = df_tl[3].apply(lambda x: pd.to_datetime(x, format='%d-%m-%Y'))
        assert str(col_date.dtype) == 'datetime64[ns]'
        assert str(df_tl[4].astype(float).dtype) in ('float', 'float64')

    def process_files(self):
        # cp tax lot file
        self.session.upload(self.file, '/data/common/')
        cmd = 'release; cd src/indexparticipants; cvs up'
        self.build_sh.send(cmd)
        perf_date = self.perf_date
        bk_code = self.base_bk_code
        tm = self.tax_lot_selection_method
        tax_code = self.tax_code
        dt = self.build_sh.check_output(f'dateformat -sP {perf_date}', last_line_only=True)
        # more variables
        index2 = str(int(self.bk_instr) + 20000)
        constituent_start_date = self.build_sh.check_output(f'dateformat -P {perf_date}', last_line_only=True)
        index_start_date = self.build_sh.check_output(f'dateformat -PP {perf_date}', last_line_only=True)
        today = self.build_sh.check_output('dateformat', last_line_only=True)
        yesterday = self.build_sh.check_output(f'dateformat -P {today}', last_line_only=True)
        # alternatively run command in proc2
        # cmd = f"""python /data/client/mingl/bau/warakirri/benchmark/new_benchmark.py -s {perf_date}
        # -i {bk_instr} -c {key} -b "{benchmark}" -m {tm} -t {tax_code}"""
        # qdsbuild actions
        # create file
        # self.session.upload(str(PACK_PATH / 'resources' / 'ftp_uploader.exp'), '~/term')
        file_def = 'B-' + self.bk_code + '.def'
        with open(PACK_PATH / 'resources' / 'index_template.def', 'r') as infile:
            def_lines = infile.readlines()
            with open(os.path.join(DATA_PATH, file_def), 'w', newline='\n') as f:
                new_sh_lines = []
                for sh_line in def_lines:
                    new_line = sh_line.replace('$code', self.bk_code)\
                        .replace('$dt', dt).replace('$index', str(self.bk_instr))
                    new_sh_lines.append(new_line)
                f.writelines(new_sh_lines)
        self.build_sh.upload(file_def, '~/term')
        cmd = f'cp ~/term/{file_def} {RELEASE_ROOT}/src/indexparticipants/defs'
        self.build_sh.send(cmd)
        cmd = f'cd {RELEASE_ROOT}/src/indexparticipants/defs;' + f'cvs add {file_def};'\
              + ' cvs ci -m "New benchmark" B-{code}.def'.format(code=self.bk_code)
        self.build_sh.send(cmd)
        # create new instr
        cmd_str = f'indexnew -D live < B-{self.bk_code}.def'
        self.build_sh.send(cmd_str)
        # insertions
        sql = """insert into indexprovider values ({index},'S',{index_start_date},99999999);
        insert into indexdaily values ({index},{index_start_date},1000.0,-1,-1,-1,0.0,0,0.0);
        insert into indexdaily values ({index2},{index_start_date},1000.0,-1,-1,-1,0.0,0,0.0);
        insert into histavail  values ({index},1,{index_start_date},{index_start_date},'A','Y');
        insert into histavail  values ({index2},1,{index_start_date},{index_start_date},'A','Y')""".format(
            index=self.bk_instr, index2=index2, index_start_date=index_start_date)
        self.db_mgr.exec_sql(sql)
        # create sh file
        file_sh = 'B-' + self.bk_code + '.sh'
        with open(PACK_PATH / 'resources' / 'bk_template.sh', 'r') as infile:
            sh_lines = infile.readlines()
            with open(os.path.join(DATA_PATH, file_sh), 'w', newline='\n') as f:
                new_sh_lines = []
                for sh_line in sh_lines:
                    new_line = sh_line.replace('$code', self.bk_code).replace('$benchmark', bk_code)
                    new_sh_lines.append(new_line)
                f.writelines(new_sh_lines)
        self.build_sh.upload(file_sh, '~/term')
        cmd = f'cd {RELEASE_ROOT}/src/indexparticipants/bin/warakirri/; cvs up; ' \
              f'cp ~/term/{file_sh} .; chmod +x {file_sh};' \
              f' cvs add {file_sh};cvs ci -m "New benchmark" {file_sh};' \
              f' cp {file_sh} /projects/indexparticipants/bin'
        self.build_sh.send(cmd)
        assert pd.Timestamp.now().weekday() != 6
        file_test = f'{self.bk_code}_test.sh'
        with open(PACK_PATH / 'resources' / 'test_template.sh', 'r') as infile:
            sh_lines = infile.readlines()
            with open(os.path.join(DATA_PATH, file_test), 'w', newline='\n') as f:
                new_sh_lines = []
                for sh_line in sh_lines:
                    new_line = sh_line.replace('$constituentStartDate', constituent_start_date)\
                        .replace('$index', self.bk_code)\
                        .replace('$today', today)\
                        .replace('$yesterday', yesterday)
                    new_sh_lines.append(new_line)
                f.writelines(new_sh_lines)
        self.build_sh.upload(file_test, '~/term')
        cmd = f'sh ~/term/{self.bk_code}_test.sh'
        self.build_sh.send(cmd)
        # Insert tax rates
        # get tax rate
        first_rate, second_rate, third_rate = 0.3, 0.3, 0.3
        if tax_code == 'SUPER':
            second_rate, third_rate = 0.15, 0.1
        elif tax_code == 'COMPANY':
            pass
        elif tax_code == 'PENSION':
            second_rate, third_rate = 0, 0
        else:
            raise
        sql = f"""insert into atb_tax_rates values ({self.bk_instr},0,99999999,{first_rate},{second_rate},{third_rate})"""
        quant_mgr = DBManager('quantdb', profile='prod')
        quant_mgr.exec_sql(sql)
        # now switch to qdsclient
        cmd_str = f"mkdir /data/aftertax/warakirri/B-{self.bk_code};" \
                  f" cd /data/aftertax/warakirri/B-{self.bk_code}; " \
                  f"mkdir /data/aftertax/warakirri/B-{self.bk_code}/initialHoldings; " \
                  f"cp /data/common/{self.file}" \
                  f" /data/aftertax/warakirri/B-{self.bk_code}/initialHoldings/{self.bk_code}{constituent_start_date}.csv"
        self.session.send(cmd_str)
        cmd_str = f"cd /data/aftertax/warakirri/B-{self.bk_code}/initialHoldings;" \
                  f"/projects/aftertax/bin/makeFirstATBTaxLots.py" \
                  f" -f {self.bk_code}{constituent_start_date}.csv -i B-{self.bk_code} -d {perf_date}"
        self.session.send(cmd_str)
        pwd = decode_pass(quant_mgr.cred['pwd'])
        db_user = quant_mgr.cred['db_user']
        db_host_name = quant_mgr.cred['db_host_name']
        cmd_str = f"bcp quantdb..atb_cash_float in CF-{self.bk_instr}-{perf_date}.csv" \
                  f" -Y -b 1000 -c -t, -S{db_host_name} -U{db_user} -P{pwd};" \
                  f" bcp quantdb..atb_portfolio_amv in PA-{self.bk_instr}-{perf_date}.csv" \
                  f" -Y -b 1000 -c -t, -S{db_host_name} -U{db_user} -P{pwd};" \
                  f"bcp quantdb..atb_tax_lots in TL-{self.bk_instr}-{perf_date}.csv -Y -b 1000" \
                  f" -c -t, -S{db_host_name} -U{db_user} -P{pwd}"
        self.session.send(cmd_str)
        sql = f"select * from atb_tax_lots where index_id = {self.bk_instr}; " \
              f"-m pretty | sql_to_csv | unix2dos > {self.bk_instr}-{constituent_start_date}.csv"
        quant_mgr.exec_sql(sql)
        # TODO: update the file
        cmd_str = "expect /data/client/mingl/projects/bash_scripts/wara-ftp.exp" \
                  f" /data/aftertax/warakirri/B-{self.bk_code}/initialHoldings {self.bk_instr}-{constituent_start_date}.csv"
        self.session.send(cmd_str)
        tm = tm.replace('LG', 'LOW_GAIN')
        met_code = str(METHODS.index(tm))
        new_line = f"""B-{self.bk_code} {bk_code} Y {met_code}"""
        # logger.error('Remove the empty line at the end')
        cmd_str = f'echo "{new_line}" >> /data/aftertax/warakirri/benchmark.list'
        self.session.send(cmd_str)
        # update the src
        cmd_str = """release; cd src/aftertax/scripts/"""
        self.build_sh.send(cmd_str)
        self.diff_check('/data/aftertax/warakirri/benchmark.list', f'{RELEASE_ROOT}/src/aftertax/scripts/',
                        self.build_sh)
        self.confirm_action('Please confirm the changes to benchmark.list file')
        cmd_str = """cp /data/aftertax/warakirri/benchmark.list ."""
        self.build_sh.send(cmd_str)
        cmd_str = """cvs up; cvs ci -m 'update benchmark list for warakirri'"""
        self.build_sh.send(cmd_str)
        cmd_str = f"""/projects/aftertax/bin/wara_rerun.sh -I -b B-{self.bk_code} -s {perf_date}"""
        self.session.screen_run(cmd_str)
        cmd_str = f"grep -i error /data/aftertax/warakirri/DUMP_B-{self.bk_code}_{perf_date}-{yesterday}"
        err_msg = self.session.check_output(cmd_str)
        # check error
        if err_msg:
            logger.error(err_msg)

    def run(self):
        self.process_files()


if __name__ == '__main__':
    # bk_code = 'TSIAD'
    # start_date = '20210630'
    # perf_date = '20210701'
    # benchmark = 'S&P/ASX200'
    # tax_lot_selection_method = 'LG'
    # tax_code = 'SUPER'

    # bk_code = 'TSIAQ1'
    # start_date = '20210630'
    # perf_date = '20210701'
    # benchmark = 'S&P/ASX200'
    # tax_lot_selection_method = 'LG'
    # tax_code = 'SUPER'
    #
    # bk_code = 'TSIAS1'
    # start_date = '20210630'
    # perf_date = '20210701'
    # benchmark = 'S&P/ASX SMALL ORDINARIES'
    # tax_lot_selection_method = 'LG'
    # tax_code = 'SUPER'

    # bk_code = 'TSAAGA'
    # start_date = '20210630'
    # perf_date = '20210701'
    # benchmark = 'S&P/ASX 300'
    # tax_lot_selection_method = 'LG'
    # tax_code = 'SUPER'

    # bk_code = 'TSBSAE'
    # start_date = '20210630'
    # perf_date = '20210701'
    # benchmark = 'S&P/ASX 300'
    # tax_lot_selection_method = 'LG'
    # tax_code = 'SUPER'
    #
    # bk_code = 'TSBTAE'
    # start_date = '20210630'
    # perf_date = '20210701'
    # benchmark = 'S&P/ASX 300'
    # tax_lot_selection_method = 'LG'
    # tax_code = 'SUPER'
    #
    # bk_code = 'TSFSAG'
    # start_date = '20210630'
    # perf_date = '20210701'
    # benchmark = 'S&P/ASX 300'
    # tax_lot_selection_method = 'LG'
    # tax_code = 'SUPER'

    # bk_code = 'TSOTVA'
    # start_date = '20210630'
    # perf_date = '20210701'
    # benchmark = 'S&P/ASX 300'
    # tax_lot_selection_method = 'LG'
    # tax_code = 'SUPER'
    #
    # bk_code = 'TSSPHR'
    # start_date = '20210630'
    # perf_date = '20210701'
    # benchmark = 'S&P/ASX SMALL ORDINARIES'
    # tax_lot_selection_method = 'LG'
    # tax_code = 'SUPER'

    bk_code = 'TSGAEQ'
    start_date = '20211217'
    perf_date = '20211218'
    benchmark = 'S&P/ASX 300'
    tax_lot_selection_method = 'LG'
    tax_code = 'SUPER'

    BenchmarkRequest(bk_code=bk_code, start_date=start_date, perf_date=perf_date, benchmark=benchmark,
                     tax_lot_selection_method=tax_lot_selection_method, tax_code=tax_code).run()
    # clean_up(12001821)
