#!/usr/bin/env python3
import os
import pandas as pd
from loguru import logger

from clients import DEFAULT_KWS
from session import Session
from utils.funcs import get_issue_key, get_config
from commands import DATA_PATH
from clients.base import Request
from clients.warakirri import CLIENT, PACK_PATH, BUYBACK_MAIL, DATA_BASE, SOURCE_PATH
from utils.mails import send_gbst_mail


class BuyBack(Request):
    """
    Warakirri buyback request. Need to put the buyback csv file to the DATA_PATH and start the process
    """

    buyback_file = 'buyBackFile'

    def __init__(self, code: str, eff_date: str, file: str = '', cur_date: str = ''):
        """
        constructor
        :param code: ticker code for buyback
        :param eff_date: eff date of the buyback
        :param file: if not provided, use convention name
        :param cur_date: if not provided, use today
        """
        # naming convention: {bk_code}_BuyBack_ddmmyy.csv
        self.code = code
        self.eff_date = eff_date
        if not file:
            file_date = (pd.to_datetime(self.eff_date, format='%Y%m%d')).strftime('%d%m%y')
            file = f'{code}_BuyBack_{file_date}.csv'
        self.file = file
        if not cur_date:
            cur_date = pd.Timestamp.now().strftime('%Y%m%d')
        self.cur_date = cur_date
        if not os.path.exists(os.path.join(DATA_PATH, self.file)):
            logger.error(f'Please download {self.file} to {DATA_PATH}')
            exit(1)
        self.ticket = get_issue_key(kw=f'{CLIENT} Buyback', default=f'{CLIENT} buyback')
        self.session = Session(reason=self.ticket)
        self.buyback = pd.DataFrame()
        logger.info(f'{CLIENT} buyback initialized for {self.code}')

    def update_file(self):
        """
        Validate files and commit changes
        ex:
        # 20171113,12001504,1001069,16.79,9.44,54.23,54.23,0.1067
        :return:
        """
        # get lines
        # cols = ['portfolio_code', 'start_date', 'security_id', 'cgt_cash', 'received_cash',
        #           'dividend_amt', 'franked_amt', 'percent_sold', 'index_id']
        cols = ['portfolio_code', 'security_id', 'start_date', 'cgt_cash', 'received_cash',
                  'dividend_amt', 'franked_amt', 'percent_sold', 'index_id']
        df = pd.read_csv(os.path.join(DATA_PATH, self.file),
                         header=0, names=cols, skiprows=1,
                         dtype={'portfolio_code': str, 'security_id': int, 'start_date': str, 'cgt_cash': float,
                                'received_cash': float, 'dividend_amt': float, 'franked_amt': float,
                                'percent_sold': float, 'index_id': int})
        df = df.round(3)
        # if not in the list, append file
        # convert to lines
        lines = list()
        df = df[['start_date', 'index_id', 'security_id', 'cgt_cash', 'received_cash',
                 'dividend_amt', 'franked_amt', 'percent_sold']]
        for idx, row in df.iterrows():
            index_id, sec_id, date = row['index_id'], row['security_id'], row['start_date']
            cmd = f"""grep {date} {DATA_BASE}/{self.buyback_file} | grep {index_id} | grep {sec_id}"""
            output = self.session.check_output(cmd)
            if len(output):
                logger.error(f'Already found entry in buyback file: {output}')
                continue
            line = ','.join([str(x) for x in row.values]) + '\n'
            lines.append(line)
        hack_file = f'{self.code}_bb_hack.txt'
        with open(os.path.join(DATA_PATH, hack_file), 'w') as file:
            file.writelines(lines)
        self.session.upload(hack_file, '~/term')
        cmd = f"""cp {DATA_BASE}/{self.buyback_file} ~/term; cat ~/term/{hack_file} >> ~/term/{self.buyback_file}"""
        self.session.send(cmd)
        self.diff_check(f'~/term/{self.buyback_file}', f'{DATA_BASE}/{self.buyback_file}', self.session)
        self.confirm_action('Apply the above changes?')
        cmd = f"""cp ~/term/{self.buyback_file} {DATA_BASE}/{self.buyback_file}"""
        self.session.send(cmd)
        # commit to csv
        build_sh = Session(login_user='qdsbuild', reason=self.ticket)
        cmd = f"""release; cd {SOURCE_PATH}; cvs up; cp {DATA_BASE}/{self.buyback_file} .; """
        f""" cvs ci -m "new buyback details for {self.code}" {self.buyback_file}"""
        build_sh.send(cmd)
        self.buyback = df

    def reprocess(self):
        """
        Loop over the file and rerun each benchmark that has participated the buyback
        :return:
        """
        # get command
        logger.info('Executing command to apply buybacks')
        # rerun the process
        df = self.buyback
        df = df.groupby('index_id').agg(
            {'start_date': 'min'}
        ).reset_index()
        from pandas.tseries.offsets import BDay
        end_date = (pd.to_datetime(self.cur_date, format='%Y%m%d') + BDay(-1)).strftime('%Y%m%d')
        # upload scripts
        self.session.upload(str(PACK_PATH / 'resources' / 'ftp_uploader.exp'), '~/term')
        self.session.upload(str(PACK_PATH / 'resources' / 'wara_rerun.sh'), '~/term')
        self.check_process(DEFAULT_KWS, self.session)
        cmd = ''
        config = get_config()
        mail_addr = config.get('JIRA', 'USER')
        for idx, row in df.iterrows():
            start_date = row['start_date']
            index_id = row['index_id']
            cmd += f'sh ~/term/wara_rerun.sh -s {start_date} -e {end_date} -m {mail_addr} {index_id};'
        logger.info(f'Rerunning calcs for {self.code}')
        self.session.screen_run(cmd)
        logger.info('Rerun completed for given benchmarks')

    def send_email(self):
        """
        Send mail to notify the client about the task
        :return:
        """
        logger.info('Sending emails to notify the client')
        to = BUYBACK_MAIL['to'].split(',')
        cc = BUYBACK_MAIL['cc'].split(',')
        body = BUYBACK_MAIL.get('body')
        greeting = BUYBACK_MAIL.get('greeting')
        subject = BUYBACK_MAIL.get('subject').replace('{{code}}', self.code)
        send_gbst_mail(to, subject, greeting, body, cc=cc)

    def run(self):
        self.update_file()
        self.reprocess()
        self.send_email()


if __name__ == '__main__':
    BuyBack(code='QUB', eff_date='20220516').run()
