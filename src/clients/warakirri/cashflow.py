#!/usr/bin/env python3
import pandas as pd
from commands import DATA_PATH
from pandas.tseries.offsets import BDay
from clients.base import Request
from loguru import logger
import os
from clients.warakirri import CLIENT, CASHFLOW_MAIL
from clients import DEFAULT_KWS
from session import Session
from utils.funcs import get_issue_key
from utils.mails import send_gbst_mail


class CashFlowUpdate(Request):
    # portfolio list
    minutes_to_run = 2

    def __init__(self, file: str, cur_date: str = ''):
        # TODO: config the time window to run
        # TODO: restore previous task
        # TODO: validate result for each portfolio run
        # TODO: do not send mail until all tasks are completed
        if not os.path.exists(os.path.join(DATA_PATH, file)):
            raise ValueError(f'File {file} not found in folder {DATA_PATH}')
        self.file = file
        self.ports = []
        self.ticket = get_issue_key(kw=f'{CLIENT} cashflow', default=f'{CLIENT} cashflow')
        self.session = Session(reason=self.ticket)
        if cur_date:
            self.cur_date = cur_date
        else:
            self.cur_date = pd.Timestamp.now().strftime('%Y%m%d')
        # find successful jobs
        self.skipped = set()
        # successful rerun will add a new row to this file in ~/term folder
        self.task_file = f'{file}_completed.txt'
        if self.session.check_file(f'~/term/{self.task_file}'):
            lines = self.session.read_file(f'~/term/{self.task_file}')
            self.skipped = set([line.strip() for line in lines])
        logger.info(f'Cashflow updater initialized for {fn}')

    def validate_data(self):
        """
        Validate data and print weekend rows if any
        :return:
        """
        names = ['code', 'date', 'amount']
        try:
            cf = pd.read_csv(os.path.join(DATA_PATH, self.file), header=None, names=names,
                             dtype={'code': str, 'date': 'int64', 'amount': 'float64'})
        except ValueError:
            cf = pd.read_csv(os.path.join(DATA_PATH, self.file), header=None, names=names,
                             dtype={'code': str, 'date': str, 'amount': 'float64'})
            cf.loc[:, 'date'] = cf['date'].apply(lambda x: pd.to_datetime(x, format='%d/%m/%Y').strftime('%Y%m%d')).astype(int)
        weekends = cf.date.apply(lambda x: True if pd.to_datetime(x, format='%Y%m%d').weekday() in (5, 6) else False)
        df_we = cf[weekends]
        if len(df_we):
            for _, row in df_we.iterrows():
                logger.error(f'Found cashflow on weekends for {row["code"]} on {row["date"]}')
            raise ValueError('Please verify the input file to make sure all cashflow happens on weekdays')
        # check code
        assert len(cf[~cf['code'].str.isalnum()]) == 0
        # split data for each portfolio
        total_hours = 0
        for port, df in cf.groupby(['code']):
            self.ports.append(port)
            # time estimate
            days = len(pd.bdate_range(str(df.date.min()), self.cur_date, freq=BDay()))
            time_needed = days * self.minutes_to_run
            logger.info(f'Need approx {time_needed} min to process {port}')
            total_hours += time_needed
            df.to_csv(os.path.join(DATA_PATH, f'{port}_{self.cur_date}.csv'),
                      header=None, line_terminator='\n', index=False)
            self.session.upload(f'{port}_{self.cur_date}.csv', '~/term')
        logger.warning(f'May need {total_hours/60} hours in total to finish the process')

    def copy_and_exec(self):
        """
        copy the files and execute command
        :return:
        """
        command = ""
        # TODO: create a script to rerun the process and check result and then echo successful list to the output
        # TODO: split tasks to update files and rerun benchmarks
        for port in self.ports:
            if port in self.skipped:
                continue
            fn = f'{port}_{self.cur_date}.csv'
            cmd = f"""cp ~/term/{fn} /data/common/cashflow_updates && /projects/aftertax/bin/update_cashflows.sh """ + \
                  f"""/data/common/cashflow_updates/{fn} 2>&1 | tee ~/term/{fn}_cf.log;"""
            command += cmd
        self.check_process(DEFAULT_KWS, self.session)
        self.session.screen_run(command)

    def send_email(self):
        """
        Send mail to notify the client about the task
        :return:
        """
        logger.info('Sending emails to notify the client')
        to = CASHFLOW_MAIL['to'].split(',')
        cc = CASHFLOW_MAIL['cc'].split(',')
        body = CASHFLOW_MAIL.get('body')
        greeting = CASHFLOW_MAIL.get('greeting')
        subject = CASHFLOW_MAIL.get('subject').replace('{{date}}', self.cur_date)
        send_gbst_mail(to, subject, greeting, body, cc=cc)

    def run(self):
        self.validate_data()
        self.copy_and_exec()
        self.send_email()

    def test_run(self):
        self.send_email()


if __name__ == '__main__':
    fn = 'CF20221117143615'
    # cur_date = '20220713'
    CashFlowUpdate(fn).run()
