#!/usr/bin/env python3
from loguru import logger
from session import Session
from dbmanager import DBManager


def start_over(bk_code: str):
    # clean up the instr
    session = Session(reason='automate')
    db_mgr = DBManager('live', profile='prod', session=session).instance
    bk_id = session.check_output(f'instr -i B-{bk_code}', last_line_only=True)
    assert 10000000 < int(bk_id) < 20000000
    logger.info(f'Cleaning up {bk_id}')
    acc_bk_id = int(bk_id) + 20000
    sql = f"""delete from indexprovider where instr_id = {bk_id};
                delete from  indexdaily where instr_id = {bk_id};
                delete from  indexdaily where instr_id = {acc_bk_id};
                delete from  histavail where instr_id = {bk_id};
                delete from  histavail where instr_id = {acc_bk_id};
                delete from indexparticipant where index_id = {bk_id};
                delete from quantdb..atb_cash_float where index_id = {bk_id};
                delete from quantdb..atb_portfolio_amv where index_id = {bk_id};
                delete from quantdb..atb_tax_lots where index_id = {bk_id};
                delete from quantdb..atb_tax_rates where index_id={bk_id};
                delete from instrument where instr_id={bk_id};
                delete from instrument where instr_id={acc_bk_id}"""

    db_mgr.exec_sql(sql)
    logger.warning(f'Please check the folder for {bk_id} under /data/aftertax/warakirri and remove it;'
                   f' And remove the row in /data/aftertax/warakirri/benchmark.list')
    logger.info(f'{bk_id} cleaned up successfully!')


def rerun_clean(bk_code: str, start_date: str):
    client_sh = Session(reason='automate', display=True)
    build_sh = Session(login_user='qdsbuild', reason='automate', display=True)
    db_mgr = DBManager('live', profile='prod', session=client_sh).instance
    bk_id = client_sh.check_output(f'instr -i B-{bk_code}', last_line_only=True)
    acc_bk_id = int(bk_id) + 20000
    prev_day = client_sh.check_output(f'dateformat -P {start_date}', last_line_only=True)
    cmd = f'/projects/aftertax/bin/clean.sh -i B-{bk_code} -d {start_date}'
    client_sh.send(cmd)
    cmd = f'/projects/indexparticipants/bin/wipe_indexmem.sh {start_date} B-{bk_code}'
    client_sh.send(cmd)
    sql = f"""delete from indexdaily where instr_id={bk_id} and date >= {start_date};
                delete from indexdaily where instr_id={acc_bk_id} and date >= {start_date};
                update histavail set end_date = {prev_day} where instr_id = {bk_id};
                update histavail set end_date = {prev_day} where instr_id = {acc_bk_id}"""
    db_mgr.exec_sql(sql)
    cmd = f'sh /data/client/mingl/scripts/wara_fix.sh -d {start_date} -i {bk_code}'
    build_sh.send(cmd)
    cmd = f'/projects/aftertax/bin/wara_rerun.sh -I -b B-{bk_code} -s {start_date}'
    print(f'Now please run command: {cmd}')


if __name__ == '__main__':
    # rerun_clean('TSIAQ1', '20211220')
    rerun_clean('TSSPHR', '20210701')
    # start_over('TSAAGA')
