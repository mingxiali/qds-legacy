#!/usr/bin/env python3
import os

import pandas as pd
from pandas.tseries.offsets import BDay

from clients.base import Request
from utils.funcs import get_issue_key
# from session import Session
from loguru import logger
# from dbmanager import DBManager
from clients.warakirri import CLIENT, BIN_BASE, DATA_BASE, COMMON_BASE
from commands import DATA_PATH
from clients.util import DBManager, Session


class InspecieRequest(Request):
    """
    Process warakirri inspecie request
    the file should be downloaded to DATA_DIR, and provide corresponding benchmark code, stock date (EOD)
    bk_stock_date should be in YYYYMMDD
    Need to process cashflow separately
    """

    def __init__(self, bk_code: str, bk_stock_date: str, file: str = ''):
        """
        input parameters for inspecie requests
        :param bk_code: benchmark code like REE9
        :param bk_stock_date: COB of when tax lots are added
        :param file: inspecie file
        """
        self.bk_code = bk_code
        self.bk_stock_date = bk_stock_date
        self.bk_cash_date = (pd.to_datetime(self.bk_stock_date, format='%Y%m%d') + BDay(1)).strftime('%Y%m%d')
        if not file:
            # ex:     file = 'HENQ_Inspecie_090322.csv'
            file_date = (pd.to_datetime(self.bk_stock_date, format='%Y%m%d')).strftime('%d%m%y')
            file = f'{bk_code}_Inspecie_{file_date}.csv'
        self.file = file
        if not os.path.exists(os.path.join(DATA_PATH, self.file)):
            if os.path.exists(os.path.join(DATA_PATH, f'{bk_code}_Inspecie_{self.bk_stock_date}.csv')):
                os.rename(os.path.join(DATA_PATH, f'{bk_code}_Inspecie_{self.bk_stock_date}.csv'),
                          os.path.join(DATA_PATH, self.file))
            else:
                logger.error(f'Please download {self.file} to {DATA_PATH}')
                exit(1)
        self.meta = {}
        ticket = get_issue_key(kw=f'{CLIENT} Inspecie', default=f'{CLIENT} inspecie')
        self.session = Session(reason=ticket)
        # self.db_mgr = DBManager('live', profile='prod', session=self.session).instance
        self.db_mgr = DBManager()
        logger.info(f'{CLIENT} Inspecie initialized for {self.bk_code}')
        self.validate()

    def validate(self) -> None:
        """
        Read file, validate fields and update meta
        :return: None
        """
        # validate format and copy files
        logger.info(f'Validating data for {self.bk_code}')
        # inspecie file like this
        # Header should be: [benchmark name],[stock amount],[cash amount]
        # Body should be: [benchmark name],[stock name],[amount],[dd-mm-yyyy],[return]
        # AIFM5,1095027085.77,33000000.00,,,,,,
        # AIFM5     ,PG0008579883,144546517.45,19-02-2018,0.018912252,,,,
        # AIFM5     ,PG0008579883,91209240.69,16-02-2018,0.016269945,,,,
        with open(os.path.join(DATA_PATH, self.file), 'r', encoding='utf-8-sig') as infile:
            content = infile.readlines()
        # validate header
        header = content[0].strip().split(',')
        assert len(header) >= 2
        assert header[0].strip() == self.bk_code
        assert type(float(header[1])) is float
        assert type(float(header[2])) is float
        self.meta['stockflow'] = header[1]
        self.meta['cash_amount'] = header[2]
        # validate body
        df = pd.read_csv(os.path.join(DATA_PATH, self.file), header=None,
                         names=['bk', 'security', 'amount', 'date', 'return'], skiprows=1)
        assert df.bk.str.strip().unique().tolist() == [self.bk_code]
        # check fields
        df.loc[:, 'security'] = df.security.astype(str)
        df.loc[:, 'amount'] = df.amount.astype(float)
        df.loc[:, 'date'] = df.date.apply(lambda x: pd.to_datetime(x, format='%d/%m/%Y'))
        df.loc[:, 'return'] = df['return'].astype(float)
        # copy file
        self.session.upload(os.path.join(DATA_PATH, self.file), '~/term')
        cmd = f'cp ~/term/{self.file} {COMMON_BASE}/{self.bk_code}{self.bk_stock_date}.csv'
        self.session.send(cmd)
        logger.info('All files validated and copied')

    def load_data(self) -> None:
        """
        Load data into the database
        :return: None
        """
        logger.info('Loading data')
        # set up dates
        sod_date = self.bk_cash_date
        eod_date = self.bk_stock_date
        bk_id = self.session.check_output(f'instr -i B-{self.bk_code}', last_line_only=True)
        prev_day = self.session.check_output(f'dateformat -P {sod_date}', last_line_only=True)
        # clean up since sod
        cmd = f'cd {DATA_BASE}/B-{self.bk_code}/initialHoldings;' \
              f'{BIN_BASE}/clean.sh -i B-{self.bk_code} -d {sod_date}'
        self.session.send(cmd)
        # copy to initial holding folder
        cmd = f'cp {COMMON_BASE}/{self.bk_code}{eod_date}.csv {DATA_BASE}/B-{self.bk_code}/initialHoldings'
        self.session.send(cmd)
        # create tax lots
        cmd = f'{BIN_BASE}/makeFirstATBTaxLots.py' \
              f' -f {self.bk_code}{eod_date}.csv -i B-{self.bk_code} -d {sod_date} -S'
        self.session.send(cmd)
        # copy output file to database
        cmd = f'bcp quantdb..atb_tax_lots in TL-{bk_id}-{sod_date}.csv' \
              f' -Y -b 1000 -c -t, -SQDS2_PROD -Uupdater -Pupdate05'
        self.session.send(cmd)
        # clean up if already exists
        cmd = f"""[ -f {DATA_BASE}/B-{self.bk_code}/initialHoldings/{bk_id}-{sod_date}.csv ] &&""" \
              + f""" rm {DATA_BASE}/B-{self.bk_code}/initialHoldings/{bk_id}-{sod_date}.csv"""
        self.session.send(cmd)
        # extract tax lots
        sql = f"""select * from atb_tax_lots where index_id = {bk_id} and start_date = {sod_date};""" \
              + f""" -m pretty | sql_to_csv | unix2dos > {bk_id}-{sod_date}.csv"""
        cmd = 'sqsh -Uupdater -Pupdate05 -SQDS2_PROD -Dquantdb -C "{sql}"'.format(sql=sql)
        self.session.send(cmd)
        # assure the file is generated
        cmd = f"""[ -f {DATA_BASE}/B-{self.bk_code}/initialHoldings/{bk_id}-{sod_date}.csv ] && echo OK"""
        output = self.session.check_output(cmd, last_line_only=True)
        assert 'OK' == output
        # Get long term tax rate
        sql = """select long_term_rate from quantdb..atb_tax_rates """ + \
              f"""where index_id = {bk_id} and {sod_date} between start_date and end_date"""
        df_rate = self.db_mgr.read_sql(sql)
        rate = df_rate['long_term_rate'].values[0]
        # Calculate UTL adjust
        cmd = f'{BIN_BASE}/calculate_utl_adjust.py -d {prev_day} -i ./{bk_id}-{sod_date}.csv -t {rate}'
        # get the number
        output = self.session.check_output(cmd, last_line_only=True)
        utl_adjust = output.strip().split(' - ')[-1].strip()
        stockflow = self.meta['stockflow']
        # Update atb_portfolio_amv with for (stockflow and utl_adjust)
        sql = f"""update atb_portfolio_amv set stockflow={stockflow}, utl_adjust={utl_adjust} """ \
              f"""where index_id = {bk_id} and date = {sod_date}"""
        cmd = 'sqsh -Uupdater -Pupdate05 -SQDS2_PROD -Dquantdb -C "{sql}"'.format(sql=sql)
        self.session.send(cmd)
        # append cashflow
        # loop over cashflow and append entry
        # append the initial cashflow
        cash_amount = self.meta['cash_amount']
        cmd = f"""grep -v {self.bk_code} {DATA_BASE}/Cashflows/Cashflows_{sod_date}.csv """ + \
              f"""> ~/term/tmp_Cashflows_{sod_date}.csv; """ + \
              f"""echo "{self.bk_code},{sod_date},{cash_amount}" >> """ + \
              f"""~/term/tmp_Cashflows_{sod_date}.csv"""
        self.session.send(cmd)
        # diff check and confirm
        # self.diff_check(f'~/term/tmp_Cashflows_{sod_date}.csv',
        #                 f'{DATA_BASE}/Cashflows/Cashflows_{sod_date}.csv', self.session)
        # self.confirm_action('Overwrite cashflow file')
        cmd = f"cp ~/term/tmp_Cashflows_{sod_date}.csv" \
              f" {DATA_BASE}/Cashflows/Cashflows_{sod_date}.csv"
        self.session.send(cmd)
        # upload to the ftp
        cmd = f'expect /data/client/mingl/projects/bash_scripts/wara-ftp.exp' \
              f' {DATA_BASE}/B-{self.bk_code}/initialHoldings {bk_id}-{sod_date}.csv'
        self.session.send(cmd)
        logger.info("Data loaded successfully")

    def reprocess(self) -> None:
        """
        Rerun calculation up to date
        :return: None
        """
        # rerun calc
        cmd = f'{BIN_BASE}/wara_rerun.sh -I -b B-{self.bk_code} -s {self.bk_cash_date}'
        print(cmd)
        # rerun command
        self.session.send(cmd)
        # check results
        yesterday = (pd.Timestamp.now() + BDay(-1)).strftime('%Y%m%d')
        output_fn = f'{DATA_BASE}/DUMP_B-{self.bk_code}_{self.bk_cash_date}-{yesterday}'
        cmd = f'grep -i error {output_fn}'
        output = self.session.check_output(cmd)
        for line in output:
            logger.error(line)
        logger.info('Process completed')

    def run(self):
        self.load_data()
        self.reprocess()

    def test_run(self):
        # self.load_data()
        # self.reprocess()
        pass


if __name__ == '__main__':
    # bk_code = 'HEXC'
    # bk_stock_date = '20220408'
    # bk_code = 'HEXC'
    # bk_stock_date = '20220426'
    # bk_code = 'HEXC'
    # bk_stock_date = '20220720'
    # bk_code = 'SUAM1'
    # bk_stock_date = '20220520'
    # bk_code = 'SUIB'
    # bk_stock_date = '20220513'
    # bk_code = 'TSAAGA'
    # bk_stock_date = '20210820'

    # bk_code = 'TSBSAE'
    # bk_stock_date = '20211216'
    #
    # bk_code = 'TSFSAG'
    # bk_stock_date = '20211216'
    #
    # bk_code = 'TSFSAG'
    # bk_stock_date = '20211221'
    #
    # bk_code = 'TSOTVA'
    # bk_stock_date = '20211216'
    #
    # bk_code = 'TSSPHR'
    # bk_stock_date = '20210707'
    #
    # bk_code = 'TSSPHR'
    # bk_stock_date = '20210723'
    #
    #
    bk_code = 'TSGAEQ'
    bk_stock_date = '20211221'

    InspecieRequest(bk_code, bk_stock_date).run()
