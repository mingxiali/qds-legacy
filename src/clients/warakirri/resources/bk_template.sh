#! /bin/sh
#
# $Header: /import/cvs/indexparticipants/bin/warakirri/B-$code.sh,v 1.1 2021/07/28 01:18:14 qdsbuild Exp $
#
#
#----------------------------------------------------------------------------
#  SYNOPSIS
#       B-$code.sh [dd/mm/yyyy]
#
#----------------------------------------------------------------------------
#  DESCRIPTION
#    Update indexparticipant of B-$code daily (Warakirri ATB $code)
#               B-$code = $benchmark
#
#----------------------------------------------------------------------------
#  EXAMPLES
#
#----------------------------------------------------------------------------

#set +x
#debug=-vn

date=`dateformat -i today`
if [ $# -ge 1 ]; then
    date=`dateformat -i $1`
fi

indexpartdaily $debug -d $date B-$code "$benchmark"
