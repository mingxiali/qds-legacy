#! /bin/sh
#----------------------------------------------------------------------------
# set -x
indices=$index
# Populate indexpartcipants
d=$constituentStartDate
while [ $d -le $today ]
do
    for i in $indices
    do
        echo ${d}
        /projects/indexparticipants/bin/B-${i}.sh ${d}
    done
    d=`dateformat -N $d`
done

# Calculate index values
d=$constituentStartDate
enddate=$yesterday
if [ `date +\%m\%d` -gt 1645 ]; then
    enddate=$today
fi
while [ $d -le $enddate ]
do
    for i in $indices
    do
        echo ${d}
        indexdaily -d ${d} B-${i}
    done
    d=`dateformat -N $d`
done
