#! /bin/sh
PATH=$PATH:/projects/qds/bin
export PATH
Usage="Usage: $0 [-s start] [-e end] [-f ftp] [-m mail_address] [index_list]"
# set up defaults
ftp=0
index=0
while getopts "s:e:m:f" c ; do
        case $c in
            s) start=$OPTARG;;
            e) end=$OPTARG;;
            f) ftp=1;;
            m) mail_addr=$OPTARG;;
            *) echo "$Usage";
            exit 1;;
        esac
done

CONFIGURATION=/projects/common/configuration.xml:au:melbourne
host=`read_config.py -s ftp:warakirri:host`
user=`read_config.py -s ftp:warakirri:username`
password=`read_config.py -s ftp:warakirri:password`
remote_path="pub/aftertax/dailyfiles"

shift $((OPTIND-1))
index=$@

if [ -z $index ]; then
  echo "Looping over all benchmarks"
fi

if [ $ftp -eq 1 ]; then
        d=$start
        while [ $d -le $end ]
        do
            echo "Uploading files for ${d}"
            cd /data/aftertax/warakirri/output
            for file in cash_float_${d}.csv ${d}.csv ${d}_out.csv  tax_lots_${d}.csv
            do
                expect ~/term/ftp_uploader.exp /data/aftertax/warakirri/output $file $user $password $remote_path $host
            done
            d=`dateformat -N $d`
        done
else
        if [ -z $index ]; then
                d=$start
                while [ $d -le $end ]
                do
                    echo "Running calcs for all benchmarks on $d"
                    (/projects/aftertax/bin/waraBatchRunNew.sh -d $d >/data/aftertax/warakirri/DUMP_$d 2>&1); egrep -i "no|err|fail|illegal|una|seg|warn|trace|almost full|can''t" /data/aftertax/warakirri/DUMP_$d | egrep -v "Ooh, err|selling stocks no longer in index|keyboard|native python datetime will|DeprecationWarning: native python datetime|^\+|^indices=|^nontrading_file|\-n does not send data to clients|\-n: noUpdate               = False|C do NOT clean up" | /usr/bin/mailx -s "ATB - Wara - potential errors for $d"  $mail_addr
                    d=`dateformat -N $d`
                done
        else
                failed_idx=""
                for idx in $index
                do
                        d=$start
                        while [ $d -le $end ]
                        do
                            if [[ "$failed_idx" == *"$idx"* ]]; then
                                continue
                            fi
                            echo "Running calcs for $idx on $d"
                            (/projects/aftertax/bin/waraBatchRunNew.sh -d $d -I $idx >/data/aftertax/warakirri/DUMP_${idx}_${d} 2>&1)
                            err_msg=`egrep -i "no|err|fail|illegal|una|seg|warn|trace|almost full|can''t" /data/aftertax/warakirri/DUMP_${idx}_${d} | egrep -v "Ooh, err|selling stocks no longer in index|keyboard|native python datetime will|DeprecationWarning: native python datetime|^\+|^indices=|^nontrading_file|\-n does not send data to clients|\-n: noUpdate               = False|C do NOT clean up"`
                            if [ ! -z $err_msg ]; then
                                echo $err_msg | /usr/bin/mailx -s "ATB - Wara - potential errors for $idx on  $d" $mail_addr
                                failed_idx="${failed_idx} ${idx}"
                                failed_idx=`echo ${failed_idx} | xargs`
                            fi
                            d=`dateformat -N $d`
                        done
                done
                [ ! -z $failed_idx ] && echo $failed_idx | /usr/bin/mailx -s "ERROR: Wara - failed index since $start" $mail_addr
        fi
fi
