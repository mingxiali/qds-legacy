import pathlib
import colorama
import sys

__all__ = ['PACK_PATH', 'CONFIG_PATH', 'DATA_PATH', ]

# enable color in console for windows
colorama.init()
PACK_PATH = pathlib.Path(__file__).parent.resolve()
CONFIG_PATH = PACK_PATH / 'settings.ini'
# data folder to contain downloaded files
# for dev env
if (PACK_PATH.parent.parent / 'data').exists():
    # git repo
    DATA_PATH = (PACK_PATH.parent.parent / 'data').resolve()
else:
    if (pathlib.Path(sys.exec_prefix) / 'qds_data').exists():
        # installed under env root
        DATA_PATH = (pathlib.Path(sys.exec_prefix) / 'qds_data').resolve()
    else:
        if (pathlib.Path('/app/data')).exists():
            # docker env
            DATA_PATH = (pathlib.Path('/app/data')).resolve()
        else:
            DATA_PATH = (PACK_PATH.parent / 'data').resolve()
