import argparse

from termcolor import colored


def get_args():
    """
    subparsers
        config: edit or show current data_file
        run: send command/download file/upload file to remote machine
    """
    parser = argparse.ArgumentParser(description='qds-legacy command line', prog='bau')
    subparser = parser.add_subparsers(dest='command')
    # config command parser will edit the data_file with edit command and will print the current
    # settings with show command
    config_cmd = subparser.add_parser('config', help='configuration: use command show or edit')
    config_cmd.add_argument(
        "config_action", help="edit or show current data_file file", choices=["edit", "show"],
        type=str, default="show"
    )
    run_cmd = subparser.add_parser('run', help='run command on remote machine')
    run_cmd.add_argument('-m', '--machine', type=str, default='qds-proc2', help="Remote machine to access to")
    run_cmd.add_argument('-u', '--user', type=str, default='qdsclient', help="User to login to remote machine")
    run_cmd.add_argument('-r', '--reason', type=str, default='automation', help="Reason to login to remote machine")
    run_cmd.add_argument('-s', '--send', type=str, help="Send command to remote machine")
    run_cmd.add_argument('-d', '--download', action='store_true',
                         help="if set true, download from src to des; Otherwise upload file to des. Please "
                              "note that local dir should be relative path from DATA_DIR and remote path should "
                              "be absolute path on remote machine")
    # if uploading path not specific, will use ~/term
    run_cmd.add_argument('-f', '--srcPath', type=str, help="source path of the file")
    run_cmd.add_argument('-t', '--desPath', type=str, default='', help="des path of the file")
    # db command
    db_cmd = subparser.add_parser('db', help='access to databases on remote machine')
    db_cmd.add_argument('-n', '--name', type=str, required=True, help="db name to access to")
    db_cmd.add_argument('-p', '--profile', type=str, default='uat', help="profile of database")
    db_cmd.add_argument('-r', '--reason', type=str, default='DB access', help="Reason to access to database")
    db_cmd.add_argument('-a', '--action', type=str,
                        choices=('remove-credentials', 'dump', 'restore', 'exec', 'extract'),
                        help="Action to conduct on database")
    db_cmd.add_argument('-s', '--sql', type=str, help="sql command")
    db_cmd.add_argument('-f', '--file', type=str, help="redirect output to file and download to DATA_PATH")
    # task command
    task_cmd = subparser.add_parser('task', help='exec task on remote machine')
    task_cmd.add_argument('-n', '--name', type=str, choices=['wara_inspecie'], help="task name")
    task_cmd.add_argument('-p', '--profile', type=str, default='prod', help="Profile to run the task")
    task_cmd.add_argument('-r', '--reason', type=str, default='automation', help="Reason to login to remote machine")
    task_cmd.add_argument('-m', '--meta', type=str, help='meta data to feed into request class.\n'
                                                         'example: bau task -n wara_inspecie -m "{\"bk_code\": \"TEST\", \"bk_stock_date\": \"20220401\"}"')

    args = parser.parse_args()
    # validations on conditional mandatory fields
    if args.command == 'run' and args.send is None and args.srcPath is None:
        parser.error("--srcPath required for upload/download command")
    if args.command == 'db' and args.action in ('exec', 'extract') and args.sql is None:
        parser.error("--sql required for exec/extract command")
    return args


def config_file(config_action):
    """
    Edit the config file and save
    """
    from utils.funcs import get_config, write_config
    config = get_config()
    if config_action == 'show':
        # print out all elements if it is not password
        print("Current settings as below: ")
        for section in config.sections():
            for (key, val) in config.items(section):
                if key.strip().lower() not in ['password', 'token']:
                    print(colored(f'[{section}] ', 'red') + key + ' :' + colored(
                        val, 'green'))

    elif config_action == 'edit':
        # encrypt password and store it
        import getpass
        from utils.funcs import encode_pass
        print("Please edit user/pass fields below: ")
        for section in config.sections():
            for (key, val) in config.items(section):
                if val:
                    continue
                print("Setting value " + colored(f'[{section}]-> ', 'red') + key + ':')
                if 'pass' in key.lower():
                    new_val = getpass.getpass('Encrypted:').strip()
                    # encode password
                    if key.strip().lower() == 'password':
                        new_val = encode_pass(new_val)
                else:
                    new_val = input(f"{key} :").strip()
                config[section][key] = new_val
        write_config(config)
    else:
        raise ValueError(f"Unexpected config task as {config_action}")


def main():
    """Main function for bau command"""
    args = get_args()
    if args.command == 'config':
        config_file(args.config_action)
    elif args.command == 'run':
        # check access if trying to connect to remote machine
        from session import Session
        session = Session(args.user, args.machine, reason=args.reason, display=False)
        if args.send is not None:
            # send command to remote machine
            print("Running command: " + colored(args.send, 'red'))
            output = session.check_output(args.send)
            for line in output:
                print(line)
        elif args.srcPath is not None and args.desPath is not None:
            if args.download:
                session.download(args.srcPath, args.desPath)
            else:
                session.upload(args.srcPath, args.desPath)
    elif args.command == 'db':
        from dbmanager import DBManager
        db_mgr = DBManager(args.name, args.profile, reason=args.reason)
        if args.action == 'remove-credentials':
            db_mgr.remove_cred()
        elif args.action == 'dump':
            db_mgr.instance.create_dump(args.file)
        elif args.action == 'restore':
            db_mgr.instance.restore_db(args.file)
        elif args.action == 'exec':
            db_mgr.instance.exec_sql(args.sql)
        elif args.action == 'extract':
            db_mgr.instance.read_sql(args.sql, fn=args.file)
            if args.file:
                print("Please check the output in $DATA_PATH/" + colored(args.file, 'red'))
            else:
                print(f"Please check the output from latest {args.name} in $DATA_PATH/")
    elif args.command == 'task':
        if args.name == 'wara_inspecie':
            # example: bau task -n wara_inspecie -m "{\"bk_code\": \"TEST\", \"bk_stock_date\": \"20220401\"}"
            from clients.warakirri.inspecie import InspecieRequest
            import json
            meta_dict = json.loads(args.meta)
            InspecieRequest(**meta_dict).run()
        else:
            raise ValueError
    else:
        raise ValueError


if __name__ == '__main__':
    main()
