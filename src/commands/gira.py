import argparse
from termcolor import colored
from utils.funcs import get_jira

# QDS client field
CLIENT_FIELD = 'customfield_17676'


def get_args():
    """
    subparsers
        create: create jira ticket
        list: list jira issues assigned to me
        update: update specific issue
        comment: comment on the issue
        view: view the issue
    """
    parser = argparse.ArgumentParser(description='Gira command line', prog='gr ')
    parser.add_argument('-p', '--project', type=str, choices=['QBAU', "QCE", "QE"], help='project key', default='QBAU')
    cmd_parser = parser.add_subparsers(dest='command')
    # commands
    create_cmd = cmd_parser.add_parser('create', help='create ticket')
    list_cmd = cmd_parser.add_parser('list', help='list all issues assigned to me')
    update_cmd = cmd_parser.add_parser('update', help='update specific issue')
    comment_cmd = cmd_parser.add_parser('comment', help='comment on the issue')
    view_cmd = cmd_parser.add_parser('view', help='view the issue')
    # sub parsers
    create_cmd.add_argument('-s', '--summary', type=str, help='issue summary')
    create_cmd.add_argument('-d', '--description', type=str, help='issue description', default='')
    create_cmd.add_argument('-c', '--client', choices=["Legacy System", "Solaris", "Warakirri", "Mercer", "Qantas", "Russell", "Coopers", "Sunsuper", "Unisuper", "CBUS", "StateSuper", "BT", "Perpetual", "MyWealthMailbox"], help='client name', required=True)
    create_cmd.add_argument('-w', '--working', action='store_true', help='Set resolution as 3rd status which should be in progress')
    list_cmd.add_argument('-v', '--verbose', action='store_true', help='verbose')
    list_cmd.add_argument('-t', '--top', type=int, help='top n issues, default as 1', default=10)
    list_cmd.add_argument('-p', '--project', type=str, choices=['QBAU', "QCE", "QE"], help='project key')
    create_cmd.add_argument('-p', '--project', type=str, choices=['QBAU', "QCE", "QE"], help='project key', default='QBAU')
    update_cmd.add_argument('-t', '--ticket', type=str, help='ticket key', required=True)
    update_cmd.add_argument('-s', '--status', type=str, help='status', choices=["Backlog", "Selected for Development", "In Progress", "Done", "To Do", "Ready", "Testing", "Code Review"])
    update_cmd.add_argument('-w', '--work', type=str, help='log work ex: 30m')
    update_cmd.add_argument('-m', '--message', type=str, help='log work message ex: develop')
    comment_cmd.add_argument('-t', '--ticket', type=str, help='ticket key', required=True)
    comment_cmd.add_argument('-m', '--message', type=str, help='comment on ticket')
    view_cmd.add_argument('ticket', type=str, help='ticket key')
    args = parser.parse_args()
    return args


def main():
    """main function for gira command"""
    args = get_args()
    jira, JIRA_ACCOUNT = get_jira()
    if args.command == 'list':
        jql = 'assignee = currentUser() AND resolution = Unresolved and status != Done and status != "Close Task" ORDER BY updatedDate DESC, lastViewed DESC'
        if args.project is not None:
            jql = f'project = {args.project} and {jql}'
        my_issues = jira.search_issues(jql, maxResults=args.top)
        for issue in my_issues:
            if args.verbose:
                print(colored(issue.key, 'red') + ' ' + issue.fields.summary + ' #' + colored(issue.fields.status.name, 'green'))
            else:
                print(issue.key)

    elif args.command == 'create':
        if args.project in ['QCE', 'QE']:
            issue_type = 'Story'
            issue_dict = {
                'project': {'key': args.project},
                'summary': args.summary,
                'description': args.description,
                'issuetype': {'name': issue_type},
                'assignee': {'accountId': JIRA_ACCOUNT},
            }
        elif args.project == 'QBAU':
            # QBAU
            # need client name
            issue_type = 'Task'
            # client name in customfield_17676 value as Legacy System
            issue_dict = {
                'project': {'key': args.project},
                'summary': args.summary,
                'description': args.description,
                'issuetype': {'name': issue_type},
                'assignee': {'accountId': JIRA_ACCOUNT},
                CLIENT_FIELD: {'value': args.client}
            }
        else:
            raise ValueError('Unexpected project as: ' + args.project)

        new_issue = jira.create_issue(fields=issue_dict)
        if args.working:
            # set resolution as in progress
            transitions = jira.transitions(new_issue)
            jira.transition_issue(new_issue, transition=transitions[1]['id'])
            jira.transition_issue(new_issue, transition=transitions[2]['id'])
        print('New issue created as: ', colored(new_issue.key, 'red'))

    elif args.command == 'update':
        # issue.fields.timetracking.timeSpent
        issue = jira.issue(args.ticket)
        # different status for different projects
        if args.status is not None:
            # transition
            transitions = jira.transitions(issue)
            resolution, cur_res = 0, 0
            for t in transitions:
                if t['name'] == args.status:
                    resolution = t['id']
            for t in transitions:
                if t['name'] == issue.fields.status.name:
                    cur_res = t['id']
            if int(resolution) * int(cur_res) == 0:
                print(f"No status found for {args.project} as {args.status}")
            jira.transition_issue(issue, transition=resolution)
        if args.work is not None:
            jira.add_worklog(issue, args.work, comment=args.message)
        print(f"{args.ticket} updated")

    elif args.command == 'comment':
        jira.add_comment(args.ticket, args.message)
        print(f"{args.ticket} updated")

    elif args.command == 'view':
        issue = jira.issue(args.ticket)
        print(colored(issue.key, 'red'))
        print('Summary: ' + str(issue.fields.summary))
        print('Description: ' + str(issue.fields.description))
        print('Link: ' + str(issue._options['server']) + '/browse/' + args.ticket)

    else:
        jql = 'assignee = currentUser() AND resolution = Unresolved and status != Done and status != "Close Task" ORDER BY lastViewed DESC'
        if args.project is not None:
            jql = f'project = {args.project} and {jql}'
        my_issues = jira.search_issues(jql, maxResults=1)
        for issue in my_issues:
            print(issue.key)


if __name__ == '__main__':
    main()
