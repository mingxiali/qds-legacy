import argparse


def get_args():
    """Get args for gm command"""
    parser = argparse.ArgumentParser(description='go to remote machine', prog='gm')
    parser.add_argument('-u', '--user', type=str, choices=('qdsclient', "qdsbuild", "qdsbatch", "jboss", "qdsload"),
                        help='user', default='qdsclient')
    parser.add_argument('-m', '--machine', type=str,
                        choices=('proc2', 'devel2', "taxanalyser-prod-2", "taxanalyser-dr-2"), help='machine',
                        default='proc2')
    parser.add_argument('-c', '--command', type=str, help='command to run on remote machine', default=None)
    parser.add_argument('-t', '--ticket', type=str,
                        help='ticket ID, if not found will search current jira tickets', default=None)
    args = parser.parse_args()
    return args


def main():
    """main function for gm command"""
    from utils.funcs import check_network
    if not check_network():
        print("Unable to connect to general folder, please check your network")
        exit(1)
    args = get_args()
    login_user = args.user
    machine = f"qds-{args.machine}"
    cmd = args.command
    from utils.funcs import get_issue_key
    if args.ticket is None:
        # use latest modified ticket
        ticket = get_issue_key()
    else:
        # if not valid ticket, search in tickets; if still nothing found, use 'automation'
        if args.ticket.startswith('QBAU-') or args.ticket.startswith('QE-'):
            ticket = args.ticket
        else:
            ticket = get_issue_key(kw=args.ticket)
    print(f'Logging in to {login_user}@{machine} with ticket: {ticket}')
    from utils.funcs import pexpect_run_command, pexpect_to_machine, pexpect_to_general
    child = pexpect_to_general(login_user)
    child = pexpect_to_machine(child, login_user, machine, ticket)
    if cmd is None:
        greetings = child.before.decode('utf-8').split('\r\n')[-1]
        print(f'{greetings}\r\n{login_user}$ ', end='')
        child.interact()
    else:
        pexpect_run_command(child, machine, cmd, login_user)


if __name__ == '__main__':
    main()
