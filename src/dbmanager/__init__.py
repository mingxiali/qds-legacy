import pathlib
from importlib import import_module

PACK_PATH = pathlib.Path(__file__).parent.resolve()
CRED_PATH = PACK_PATH / 'db.json'
DB_FIELDS = ['db_host_name', 'db_host_path', 'db_name', 'db_user', 'port', 'pwd', 'profile', 'engine']
DBManager = getattr(import_module('dbmanager.manager'), 'DBManager')
__all__ = ['PACK_PATH', 'CRED_PATH', 'DB_FIELDS', 'DBManager']

