import os
from abc import ABC, abstractmethod
from typing import Optional

import pandas as pd
from loguru import logger
from pandas.errors import EmptyDataError

from commands import DATA_PATH
from session import Session


class SqlException(Exception):
    """
    Exception raised when unexpected return found when executing the command on remote machine
    """
    def __init__(self, sql, message="Error found when executing command on remote"):
        self.sql = sql
        self.message = message + ": " + self.sql
        super().__init__(self.message)


class API(ABC):
    def __init__(self, session: Session, cred: dict):
        self.session = session
        self.cred = cred

    @abstractmethod
    def generate_command(self, sql: str, des: str = '') -> str:
        pass

    def exec_sql(self, sql: str, fn: str = '') -> str:
        """
        Execute command on remote machine
        :param sql: sql command to run remotely
        :param fn: file name to contain output
        :return: fn
        """
        if sql.endswith(';'):
            sql = sql[:-1]
        if not fn:
            date_str = pd.Timestamp.now().strftime('%Y%m%d-%H%M%S')
            fn = f'{date_str}_{self.cred["db_name"]}.csv'
        des = os.path.join('~/term', fn).replace("\\", '/')
        cmd = self.generate_command(sql, des)
        self.send_cmd(cmd)
        return fn

    @abstractmethod
    def restore_db(self, fp: str):
        pass

    @abstractmethod
    def create_dump(self, fp: str):
        pass

    def read_sql(self, sql: str, fn: Optional[str] = None):
        """
        read sql and copy date to ~/term/[fn] and then download to DATA_PATH
        :param sql: sql to exec
        :param fn: filename to store the output
        :return: fn
        """
        fn = self.exec_sql(sql, fn)
        self.session.download(f'~/term/{fn}')
        try:
            df = pd.read_csv(os.path.join(DATA_PATH, fn))
        except EmptyDataError:
            df = pd.DataFrame()
        return df

    def send_cmd(self, cmd) -> None:
        """
        Send command to session and check output
        output will be redirected to the file so only errors will print out
        :param cmd: command to run on remote machine
        :return: None
        :exception: SqlException
        """
        output = self.session.check_output(cmd)
        for line in output:
            logger.error(line)
        if output:
            logger.error('Unexpected return found while executing command on remote machine')
            raise SqlException(cmd)
