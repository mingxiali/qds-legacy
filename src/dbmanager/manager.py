import getpass
import json
import os
from string import Template
from dbmanager.base import API
from dbmanager import CRED_PATH, DB_FIELDS
from loguru import logger
from typing import Optional
from session import Session
from utils.funcs import encode_pass, decode_pass


def get_db_creds(db_key: str, prompt: bool = True, save: bool = True) -> dict:
    """
    Read db.json and return credentials based on given db name
    The key should be [DBNAME]_[PROFILE]
    :param save: if true will save to the file
    :param db_key: database name
    :param prompt: if True will ask for password, else throw error
    :return: credential dict
    :exception: ValueError
    """
    with open(CRED_PATH, "r") as read_content:
        dbs = json.load(read_content)
    if db_key not in dbs.keys():
        if not prompt:
            raise ValueError(f"DB key {db_key} not found in current credentials")
        else:
            # ask for credentials
            print(f"Please provide credentials for {db_key}")
            new_cred = dict()
            for key in DB_FIELDS:
                if key == 'pwd':
                    pwd = getpass.getpass('Password: ')
                    value = encode_pass(pwd.strip())
                else:
                    value = input(f'{key}: ').strip()
                new_cred[key] = value
            if save:
                # write to the file
                dbs.update({db_key: new_cred})
                with open(CRED_PATH, 'w') as fp:
                    json.dump(dbs, fp, sort_keys=True, indent=4)
            return new_cred
    else:
        return dbs[db_key]


def remove_credential(db_key: str) -> None:
    """
    remove credential based on db_key
    :param db_key: key to rm in db.json file
    :return: None
    """
    with open(CRED_PATH, "r") as fp:
        dbs = json.load(fp)
    with open(CRED_PATH, "w") as fp:
        if db_key in dbs.keys():
            del dbs[db_key]
        json.dump(dbs, fp, sort_keys=True, indent=4)


class SybaseDB(API):
    """
    Access to sybase database
    """

    def __init__(self, session: Session, cred: dict):
        """
        init with Session instance and credentials of the database
        :param session: session instance
        :param cred: credentials
        """
        super().__init__(session, cred)
        logger.info(f'Sybase db manager initialized for {self.cred["db_name"]}')

    def generate_command(self, sql: str, des: str = '') -> str:
        cmd = f"""sqsh -U{self.cred['db_user']} -P{decode_pass(self.cred['pwd'])} -S{self.cred['db_host_name']} """\
              f"""-D{self.cred['db_name']} -L bcp_colsep=',' -m csv -C  """
        if des:
            cmd = cmd + f'"{sql}" > {des}'
        return cmd

    def restore_db(self, fp: str):
        pass

    def create_dump(self, fp):
        pass


class PostgresDB(API):

    def __init__(self, session: Session, cred: dict):
        """
        init with Session instance and credentials of the database
        :param session: session instance
        :param cred: credentials
        """
        super().__init__(session, cred)
        self.bin_path = os.path.dirname(self.cred['db_host_path'])
        logger.info(f'Postgres db manager initialized for {self.cred["db_name"]}')

    def generate_command(self, sql: str, des: str = '') -> str:
        db_prefix = Template(
            f"""export PGPASSWORD=$db_pass; {self.cred['db_host_path']} -h {self.cred['db_host_name']} """
            f"""-p {self.cred['port']} -U $db_user -d $db_name -c """)
        db_post = Template(
            r""""\copy ($sql) TO STDOUT WITH DELIMITER AS ',' NULL AS ''  CSV HEADER"  > $ufile_path""")
        db_prefix_str = db_prefix.substitute(db_user=self.cred['db_user'], db_pass=decode_pass(self.cred['pwd']),
                                             db_name=self.cred['db_name'])
        if not des:
            des = '~/term/tmp.csv'
        sql_str = db_post.substitute(sql=sql, ufile_path=des)
        cmd = db_prefix_str + sql_str
        return cmd

    def restore_db(self, fp: str):
        pass

    def create_dump(self, fp: str):
        pass


class DBManager:
    """
    Manage databases on remote machine
    """

    def __init__(self, name: str, profile: str = 'uat', session: Optional[Session] = None,
                 reason: str = 'DB access'):
        self.name = name
        self.cred = get_db_creds(f'{name}_{profile}')
        self.reason = reason
        self.engine = self.cred.get('engine', 'psql')
        self.profile = profile
        if self.engine not in ('psql', 'sqsh'):
            logger.error("Only psql/sqsh accepted as engine type")
            raise ValueError(f'Unexpected engine type as {self.engine} for db: {name} of {profile} found!')
        if session is not None:
            # turn off display to prevent password leakage
            session.set_display(False)
            self.session = session
        else:
            machine = 'qds-proc2' if profile == 'prod' else 'qds-devel2'
            self.session = Session(machine=machine, reason=self.reason, display=False)
        if self.engine == 'psql':
            self.api = PostgresDB(self.session, self.cred)
        else:
            self.api = SybaseDB(self.session, self.cred)

    @property
    def instance(self):
        return self.api

    def remove_cred(self):
        remove_credential(f'{self.name}_{self.profile}')


if __name__ == '__main__':
    # get_db_creds('sample_uat')
    # db_mgr = DBManager('global_db', profile='prod').instance
    # df = db_mgr.read_sql('SELECT * FROM corporate_action limit 1')
    # raise exception
    # df = db_mgr.read_sql('SELECT * FROM corporate_action1 limit 1')
    db_mgr = DBManager('live', profile='prod').instance
    df = db_mgr.read_sql('SELECT top 1 * FROM SecurityCode')
    print(df)
    # db_mgr.read_sql('SELECT * FROM corporate_action where provider_id=3 and date>=20220301')

