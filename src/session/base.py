from abc import ABC, abstractmethod


class GeneralTerm(ABC):

    def __init__(self, timeout: int = 5, reason: str = 'automation', display: bool = True):
        """
        :param reason: ticket number to access to remote machine, default as automation
        :param timeout: default unit time for each command to response
        :param display: display the output if set as true
        """
        self.timeout = timeout
        self.reason = reason
        self.display = display
        self.output = []

    @abstractmethod
    def _init(self):
        pass

    @abstractmethod
    def _clean_up(self):
        pass

    @abstractmethod
    def _login(self):
        pass

    @abstractmethod
    def download(self, src_path: str, des_path: str = '', login_user: str = 'qdsclient',
                 machine: str = 'qds-proc2') -> None:
        pass

    @abstractmethod
    def upload(self, src_path: str, des_path: str, login_user: str, machine: str) -> None:
        pass

    def _output_callback(self, x):
        if self.display:
            print(x, end='')
        if x.endswith(']$ '):
            elements = list(filter(lambda x: x, list(map(lambda x: x.strip(), x.split('\n')[:-1]))))
        else:
            elements = list(filter(lambda x: x, [e.strip() for e in x.split('\n\n')[:-1]]))
        # split the line
        ret = []
        for line in elements:
            # skip lines for new mail alert
            if line.startswith('You have new mail.'):
                continue
            ret += line.split('\n')
        self.output += ret


class RemoteTerm(GeneralTerm, ABC):
    @abstractmethod
    def send(self, cmd: str):
        pass

    @abstractmethod
    def get_cwd(self):
        pass
