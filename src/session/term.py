import os
import socket
import time
from platform import system

import typing
from paramiko.ssh_exception import AuthenticationException, SSHException
import pexpect

from utils.funcs import get_config, decode_pass, check_network
from session.base import GeneralTerm, RemoteTerm
import paramiko
from paramiko_expect import SSHClientInteraction
from loguru import logger
from commands import DATA_PATH
from shutil import copy2
from datetime import datetime
from typing import Union


# Use a decorator to check connection before running commands
def connected(func):
    def inner(*args, **kwargs):
        if not check_network():
            raise OSError("Failed to connect to remote host")
        return func(*args, **kwargs)
    return inner


class XTN(GeneralTerm):
    """
    class to connect to XTN machine and download/upload files from/to remote/local
    """

    def __init__(self, timeout: int = 5, reason: str = 'automation', display: bool = True):
        """
        construction function for XTN class
        :param timeout: timeout before raising errors
        :param reason: reason to download/upload files to remote machine
        :param display: display the output if set as true
        """
        super().__init__(timeout, reason, display)
        config = get_config()
        self._ssh_root = config['GENERAL'].get('root')
        self.user = config['SSH'].get('username')
        self._host = config['SSH'].get('host')
        self._cred = decode_pass(config['CREDENTIALS'].get('password'))
        # config
        self._reason_line = config['GENERAL'].get('reason')
        self._pass_line = config['GENERAL'].get('pass')
        # default value
        self._client = None
        self._channel = None
        self.folders = ['uploads', 'downloads', 'tasks']
        self.sys_is_linux = system() == 'Linux'
        # connect to remote
        self._login()
        # create folders if not exist
        self._init()
        logger.info(f"Connected to {self._host} with user {self.user}")

    @connected
    def _init(self) -> None:
        """
        create folder upload download tasks if not exist
        :return: None
        """
        folder_str = ' '.join(f'~/{x}' for x in self.folders)
        cmd = f"mkdir -p {folder_str}"
        self._channel.send(cmd + '\n')
        # now echo TERM variable
        cmd = """export TERM=xterm"""
        self._channel.send(cmd + '\n')

    def _clean_up(self) -> None:
        """
        clean tmp files (older than 3 days) when the session is over
        :return: None
        """
        for folder in self.folders:
            cmd = r"find ~/$FOLDER -type f -mtime +3 -exec rm -f {} \;".replace("$FOLDER", folder)
            self._channel.send(cmd + '\n')

    @connected
    def _login(self) -> None:
        """
        Login with credentials in the config
        Throw errors for auth/socket errors
        :return:
        """
        self._client = paramiko.SSHClient()
        self._client.load_system_host_keys()
        self._client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            self._client.connect(self._host, username=self.user, password=self._cred)
            # set up channel
            self._channel = self._client.invoke_shell()
        except AuthenticationException as e:
            logger.error(f"Authentication failed when accessing to {self.user}@{self._host}")
            raise e
        except SSHException as e:
            logger.error(f"Errors found when connecting or establishing an SSH session to {self.user}@{self._host}")
            raise e
        except socket.error as e:
            logger.error(f"Socket error found when accessing to {self.user}@{self._host}")
            raise e
        self._clean_up()

    @property
    def client(self):
        return self._client

    @connected
    def download(self, src_path: str, des_path: str = '', login_user: str = 'qdsclient',
                 machine: str = 'qds-proc2') -> None:
        """
        user paramiko expect to download and then copy to local
        :param src_path: src path in remote machine
        :param des_path: des path in DATA_PATH
        :param login_user: user to login to remote
        :param machine: machine to login
        :return: None
        """
        self._xtn_file(src_path, des_path, login_user, machine, download=True, redirect=False, display=self.display)
        self.copy2local(os.path.basename(src_path), des_path)
        if not os.path.basename(src_path).endswith('_cmd.txt'):
            logger.info(f"File {src_path} copied from {login_user}@{machine}:{src_path}"
                        f" to {os.path.join(DATA_PATH, des_path)}")

    @connected
    def upload(self, src_path: str, des_path: str = '', login_user: str = 'qdsclient',
               machine: str = 'qds-proc2') -> None:
        """
        user paramiko expect to upload files to remote machine
        :param src_path: src path in DATA_PATH
        :param des_path: des path in remote machine
        :param login_user: user to login to remote
        :param machine: machine to login
        :return: None
        """
        self.copy2remote(src_path, os.path.basename(src_path))
        self._xtn_file(src_path, des_path, login_user, machine, download=False, redirect=False, display=self.display)
        logger.info(f"File {src_path} copied from {os.path.join(DATA_PATH, src_path)}"""
                    f""" to {login_user}@{machine}:{des_path}""")

    def get_remote(self, login_user: str, machine: str, display: bool = True):
        """
        jump to remote machine with login user
        :param login_user: user to login to remote machine
        :param machine: remote machine to access to
        :return: paramiko client instance to interact
        :param display: display the output if set as true
        """
        return self._xtn_file('', '', login_user=login_user, machine=machine, download=False, redirect=True,
                              display=display)

    def _xtn_file(self, src_path: str, des_path: str, login_user: str, machine: str,
                  download: bool, redirect: False, display: True) -> SSHClientInteraction:
        """
        Run xtn command to upload/download files
        :param redirect:
        :param src_path: local dir from DATA_PATH
        :param des_path: remote machine path
        :param login_user: login user to transfer the data
        :param machine: remote machine
        :param download: flag to decide if uploading or downloading
        :param redirect: go to remote machine if true
        :param display: display the output if set as true
        :return: interact instance after logging in
        """
        fn = os.path.basename(src_path)
        interact = SSHClientInteraction(self._client, timeout=self.timeout, display=display, tty_width=800)
        assert interact.expect(f'{self.user}\$\s', timeout=10 * self.timeout) == 0
        if redirect:
            interact.send(f'xtn {login_user}@{machine}')
        else:
            if download:
                interact.send(f'xtn {login_user}@{machine}:{src_path} /home/{self.user}/downloads')
            else:
                interact.send(f'xtn /home/{self.user}/uploads/{fn} {login_user}@{machine}:{des_path}')
        assert interact.expect(f'.*{self._reason_line}.*', timeout=15 * self.timeout) == 0
        interact.send(self.reason)
        assert interact.expect(f'.*{self._pass_line}.*', timeout=15 * self.timeout) == 0
        self.output = []
        interact.send(self._cred)
        interact.expect([f'{self.user}\$\s', f'{login_user}\$\s', f'.*\\[{login_user}@{machine} .*\\]\\'],
                               timeout=7200, output_callback=self._output_callback)
        return interact

    @connected
    def copy2remote(self, src_path: str, des_path: str = '') -> None:
        """
        copy from local dir to xtn box
        use pexpect + scp for linux and copy command on windows
        by default will use DATA_PATH
        if des_path basename empty, use basename of src_path
        :param src_path: relative path of DATA_PATH
        :param des_path: relative path of xtn home dir; if basename empty, use basename of src
        :return:
        """
        # use pexpect + scp for linux and copy command on windows
        if os.path.exists(src_path):
            abs_src_path = src_path
        elif os.path.exists(os.path.join(DATA_PATH, src_path)):
            abs_src_path = os.path.join(DATA_PATH, src_path)
        else:
            raise ValueError(f'Unexpected file path {src_path}')
        if not src_path:
            raise ValueError('Source path cannot be both empty')
        if self.sys_is_linux:
            from pexpect import spawn
            cmd = f'scp -o StrictHostKeyChecking=no ' \
                  f'{os.path.join(DATA_PATH, src_path)} {self.user}@{self._host}:/home/{self.user}/uploads/{des_path}'
            child = spawn(cmd)
            child.expect('Password:', timeout=self.timeout)
            child.sendline(self._cred)
            child.expect(pexpect.EOF, timeout=self.timeout)
        else:
            # copy to des with copy2
            copy2(abs_src_path,
                  os.path.join(self._ssh_root, 'uploads', os.path.basename(src_path)))
        logger.info(f'File {src_path} copied to xtn box @ {des_path}')

    @connected
    def copy2local(self, src_path: str, des_path: str = '') -> None:
        """
        copy from xtn box to local dir
        use pexpect + scp for linux and copy command on windows
        by default will use DATA_PATH
        if des_path empty, use basename of src_path
        :param src_path: relative path of xtn home dir
        :param des_path: relative path of DATA_PATH; if empty, use basename of src
        :return: None
        """
        if not src_path:
            raise ValueError('Source path cannot be both empty')
        if not des_path:
            des_path = os.path.basename(src_path)
        des_path = os.path.join(DATA_PATH, des_path)
        if self.sys_is_linux:
            from pexpect import spawn
            cmd = f'scp -o StrictHostKeyChecking=no ' \
                  f'{self.user}@{self._host}:/home/{self.user}/downloads/{src_path} {des_path}'
            child = spawn(cmd)
            child.expect('Password:', timeout=self.timeout)
            child.sendline(self._cred)
            child.expect(pexpect.EOF, timeout=self.timeout)
        else:
            # copy to des with copy2
            copy2(os.path.join(self._ssh_root, 'downloads', os.path.basename(src_path)),
                  os.path.join(DATA_PATH, des_path))
        if not os.path.exists(des_path):
            raise FileNotFoundError(f"{des_path} cannot be found in {DATA_PATH}")
        if not os.path.basename(src_path).endswith('_cmd.txt'):
            logger.info(f'File {src_path} copied to local machine {des_path} from xtn box')


class Session(RemoteTerm):
    """
    Session class extending RemoteTerm to connect to remote machine and run commands
    """
    def __init__(self, login_user: str = 'qdsclient', machine: str = 'qds-proc2',
                 reason: str = 'automation', timeout: int = 5, display: bool = False):
        """
        Session class to manage session with remote machine based on paramiko
        :param login_user: user used to login to remote machine
        :param machine: remote machine
        :param reason: ticket number to access to remote machine, default as automation
        :param timeout: default unit time for each command to response
        :param display: display the output if set as true
        """
        super().__init__(timeout, reason, display)
        # set up parameters
        self.login_user = login_user
        self.machine = machine
        self.base_dir = '~/term'
        # xtn session to download files
        self._xtn = XTN(reason=self.reason, display=self.display)
        self._channel = None
        self.prompt = [f'{self.login_user}\$\s', f'.*\\[{self.login_user}@{machine} .*\\]\\']
        # store output from session
        self._login()
        # create folders if not exist in ~/term folder so that there won't be file permission issue
        self._init()
        logger.info(f"Connected to {self.machine} with user {self.login_user}")

    def _init(self) -> None:
        """
        Create base dir in remote machine if not exist
        :return: None
        """
        cmd = f'[ ! -d {self.base_dir} ] && mkdir -p {self.base_dir}'
        self._channel.send(cmd)
        self._channel.expect(self.prompt, timeout=self.timeout * 1000)

    def _clean_up(self) -> None:
        """
        clean tmp files (older than 3 days) when the session is over
        :return: None
        """
        cmd = r"find $FOLDER -type f -mtime +3 -exec rm -f {} \;".replace("$FOLDER", self.base_dir)
        self._send(cmd)

    def _send(self, cmd: str, timeout: typing.Optional[int] = None) -> None:
        """
        send command and wait for prompt
        :param cmd: command to run
        :param timeout: timeout for the command
        :return: None
        """
        print(f"NOW send command => {cmd}")
        # self._channel.send(cmd)
        # if timeout is not None:
        #     self._channel.expect(self.prompt, timeout)
        # else:
        #     # use default
        #     self._channel.expect(self.prompt, self.timeout * 1000)

    def _login(self) -> None:
        """
        Access to remote machine from xtn
        :return: None
        """
        self._channel = self._xtn.get_remote(self.login_user, self.machine, self.display)
        self._clean_up()

    def set_display(self, display: bool) -> None:
        """
        set display for xtn
        :param display: set display
        :return: None
        """
        self._xtn.display = display

    def send(self, cmd: str, timeout: typing.Optional[int] = None) -> None:
        """
        send command to xtn; If display not True, need to set it to True when running the commands;
        set it back to original value after the process
        no output for this command
        :param cmd: command to send
        :param timeout: timeout for this command
        :return: None
        """
        # send command to remote
        # Will block the process
        # Need to set display as True; otherwise it won't print out the output
        # clean up
        logger.info(f'Running command: {cmd} on {self.machine}')
        self._send(cmd, timeout)

    def check_output(self, cmd: str, output_fn: str = '', last_line_only: bool = False) -> Union[list, str]:
        """
        run command and redirect output to the file and then read the file
        :param cmd: command to run
        :param output_fn: if given, then download the file and read data; otherwise, redirect output to specific file
        :param last_line_only: if true return the string in last line
        :return: lines of output
        """
        if not output_fn:
            ts = round(datetime.now().timestamp())
            output_fn = f'~/term/{ts}_cmd.txt'
        if cmd.endswith(';'):
            cmd = cmd[:-1]
        cmd = f"""({cmd}) > {output_fn} 2>&1"""
        self.send(cmd)
        self.download(output_fn)
        with open(os.path.join(DATA_PATH, os.path.basename(output_fn)), 'r') as file:
            lines = file.readlines()
            if lines and lines[-1].startswith('You have new mail.'):
                lines = lines[:-1]
        # clean up
        cmd = f'rm {output_fn}'
        self._send(cmd)
        os.remove(os.path.join(DATA_PATH, os.path.basename(output_fn)))
        if last_line_only:
            if lines:
                return lines[-1].strip()
            return ''
        return lines

    def check_file(self, file_path: str) -> bool:
        """
        check if filename exists
        NOTE: this function is not designed to be accurate but we need it so that we don't keep downloading
        files and creating flags for each check
        :param file_path: file path to check
        :return: True of False
        """
        cmd = f'[ -f {file_path} ] && echo $HOSTNAME'
        prev_display = self.display
        # clean up output
        self.output = []
        self._channel.send(cmd)
        self._channel.display = True
        self._channel.expect([f'{self.login_user}\$\s', f'.*\\[{self.login_user}@{self.machine} .*\\]\\'],
                               timeout=self.timeout, output_callback=self._output_callback)
        self._channel.display = prev_display
        if len(self.output) < 1:
            return False
        return self.output[-1].strip() == self.machine

    def wait_for_file(self, file_path: str, internal: int = 5, timeout: int = 7200) -> bool:
        """
        wait for file to be created, sleep for internal if not exist and expires when timeout exceeded
        typically, use it for screen session jobs when the job is completed and flag file touched
        :param file_path: file path to check
        :param internal: internal to sleep
        :param timeout: total timeout
        :return: if the file gets created within timeout
        """
        time_lapsed = 0
        file_found = False
        while time_lapsed < timeout:
            file_found = self.check_file(file_path)
            if file_found:
                logger.info(f'File {file_path} found on {self.machine}')
                break
            time.sleep(internal)
        return file_found

    def screen_run(self, cmd: str, screen_name: str = '', blocked: bool = True, timeout: int = 7200) -> None:
        """
        run command in screen session
        if blocked, wait for the file to show up
        :param cmd: command to run on remote screen session
        :param screen_name: new screen name to start
        :param blocked: wait for process to complete if blocked as True
        :param timeout: timeout for command
        :return: None
        """
        dt = datetime.now().strftime('%Y%m%d_%H%M%S')
        if not screen_name:
            screen_name = f'task_{dt}'
        screen_file = f'~/term/{screen_name}'
        self.send(f'[ -f {screen_file} ] && rm {screen_file}')
        postfix = f'; touch ~/term/{screen_name}'
        cmd = cmd.strip(';')
        cmd_screen = f"screen -dmS {screen_name} sh -c '{cmd + postfix}'"
        self.send(cmd_screen)
        if blocked:
            if not self.wait_for_file(screen_file, timeout=timeout):
                logger.error(f'{cmd} may not complete on time')
        self.kill_detached_screen(screen_name)

    def kill_detached_screen(self, name) -> None:
        """
        kill detached screen session
        :param name: screen session to kill on remote machine
        :return: None
        """
        cmd = f'screen -X -S {name} quit'
        self.send(cmd)

    def upload(self, src_path: str, des_path, login_user: str = '', machine: str = '', convert: bool = True) -> None:
        """
        upload to remote machine with xtn
        :param src_path: source path
        :param des_path: des path
        :param login_user: if not given, use member login user
        :param machine: if not given, use member machine
        :param convert: if True convert to unix
        :return: None
        """
        # upload to remote machine via xtn
        login_user = self.login_user if login_user == '' else login_user
        machine = self.machine if machine == '' else machine
        if des_path.startswith('~/'):
            des_path = f'/home/{login_user}/' + des_path[2:]
        des_path = des_path.replace("\\", '/')
        self._xtn.upload(src_path, des_path, login_user, machine)
        # validate the process
        if des_path.endswith('/'):
            des_path = des_path[:-1]
        if os.path.basename(des_path) != os.path.basename(src_path):
            des_path += f'/{os.path.basename(src_path)}'
        cmd = f'[ -f {des_path} ] && echo $USER'
        output = self.check_output(cmd)
        if output[-1].strip() != login_user:
            logger.error('File uploading not successful')
            raise ValueError(f'File {src_path} uploading failed')
        if convert:
            self.send(f'dos2unix {des_path}')

    def download(self, src_path: str, des_path: str = '', login_user: str = '',
                 machine: str = '') -> None:
        """
        download to remote machine with xtn
        :param src_path: source path
        :param des_path: des path
        :param login_user: if not given, use member login user
        :param machine: if not given, use member machine
        :return: None
        """
        # use xtn to download from basedir
        # clean up
        login_user = self.login_user if login_user == '' else login_user
        machine = self.machine if machine == '' else machine
        if src_path.startswith('~/'):
            src_path = f'/home/{login_user}/' + src_path[2:]
        src_path = src_path.replace("\\", '/')
        self._xtn.download(src_path, des_path, login_user, machine)

    def get_cwd(self):
        ret = self.check_output('echo $PWD')
        return ret[-1]

    def read_file(self, f_path: str) -> list:
        """
        read lines from remote machine
        :param f_path: path of the file in remote machine
        :return: lines of remote file
        """
        fn = os.path.basename(f_path)
        # back up file
        cmd = f'cp {f_path} ~/term/'
        self.send(cmd)
        self.download(f'~/term/{fn}')
        with open(os.path.join(DATA_PATH, fn), 'r') as f:
            lines = f.readlines()
        return lines


if __name__ == '__main__':
    # XTN().copy2local('.bashrc')
    # XTN().copy2remote('data_file')
    # XTN().download('/data/client/mingl/files/data_file_test')
    # XTN().upload('data_file', '/data/client/mingl/files')
    # Session(login_user='qdsbatch', machine='qds-taxanalyser-dr-2').send('echo $USER')
    # Session().send('echo $USER')
    session = Session()
    # session.send('cd /data/client/mingl')
    # session.upload('data_file', '/data/client/mingl/files')
    # session.download('/data/client/mingl/files/data_file_test')
    # print(session.get_cwd())
    # del session
    # output = session.check_output('echo HI')
    # print(output)
    # session.check_file('/data/client/mingl/files/data_file')
    # session.wait_for_file('/data/client/mingl/files/data_file_test1')
    session.screen_run('sleep 20 && echo OK')
    print('done')
