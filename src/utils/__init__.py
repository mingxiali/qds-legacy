import pathlib

PACK_PATH = pathlib.Path(__file__).parent.resolve()

__all__ = ['PACK_PATH', ]

