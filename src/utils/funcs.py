#!/usr/bin/env python3
from configparser import ConfigParser, ExtendedInterpolation
from commands import CONFIG_PATH, DATA_PATH
from jira import JIRA
import base64
import requests


def get_config() -> ConfigParser:
    """Return: dictionary of current config in data_file"""
    config = ConfigParser(interpolation=ExtendedInterpolation())
    config.read(CONFIG_PATH)
    return config


def write_config(config: ConfigParser) -> None:
    """Write to the config file"""
    with open(CONFIG_PATH, 'w') as configfile:  # save
        config.write(configfile)


def get_jira():
    """Get jira instance based on access token
        Return: Jira instance for other utils and jira account
    """
    config = get_config()
    site = config.get('JIRA', 'SITE')
    user = config.get('JIRA', 'USER')
    token = config.get('JIRA', 'TOKEN')
    url = f"""{site}/rest/api/latest/user/search?query={user}"""
    api_token = encode_pass(f"{user}:{token}")
    headers = {'Content-Type': 'application/json',
               'Authorization': f'Basic {api_token}'}
    response = requests.get(url, headers=headers)
    data = response.json()[0]
    return JIRA(site, basic_auth=(user, token)), data['accountId']


def get_issue_key(project: str = 'QBAU', top: int = 10, kw: str = '', fetch_one: bool = True, default: str = 'automation'):
    """Get issue key based on project and keyword"""
    jira_instance, _ = get_jira()
    jql = 'assignee = currentUser() AND resolution = Unresolved and status != Done and status != "Close Task" ORDER BY lastViewed DESC'
    if project is not None:
        jql = f'project = {project} and {jql}'
    my_issues = jira_instance.search_issues(jql, maxResults=top)
    issue_keys = []
    for issue in my_issues:
        summary = issue.fields.summary
        if kw and kw.lower() not in summary.lower():
            continue
        issue_keys.append(issue.key)
    if len(issue_keys) == 0 and default:
        return default
    if top == 1 or len(issue_keys) == 1:
        return issue_keys[0]
    else:
        if fetch_one:
            return issue_keys[0]
        return issue_keys


def encode_pass(val: str) -> str:
    """encode password with base64

    Args:
        val (str): password

    Returns:
        str: encoded password
    """
    base64_bytes = base64.b64encode(val.encode('ascii'))
    val = base64_bytes.decode('ascii')
    return val


def decode_pass(val: str) -> str:
    """decode the password

    Args:
        val (str): encrypted password

    Returns:
        str: decoded password
    """
    base64_bytes = val.encode('ascii')
    message_bytes = base64.b64decode(base64_bytes)
    message = message_bytes.decode('ascii')
    return message


def pexpect_to_general(login_user: str, timeout: int = 10):
    """Go to xtn box before landing on the target
    Args:
        login_user (str): login user
        timeout (int, optional): timeout for actions. Defaults to 10.

    Returns:
        child process spawned from ssh command
    """
    from pexpect import spawn
    config = get_config()
    user = config['SSH'].get('username')
    pwd = config['CREDENTIALS'].get('password')
    pwd = decode_pass(pwd)
    host = config['SSH'].get('host')
    PROMPT = [f'\r\n{user}\$ ', f'\r\n{login_user}\$ ']
    child = spawn(f'ssh -o StrictHostKeyChecking=no {user}@{host}')
    child.expect('Password:', timeout=timeout)
    child.sendline(pwd)
    child.expect(PROMPT, timeout=timeout)
    return child


def pexpect_to_machine(child, login_user: str, machine: str, ticket: str, timeout: int = 10):
    """use pexepct to land on remote machine from xtn box

    Args:
        child: spawned process from pexpect
        login_user (str): login user to remote machine
        machine (str): machine
        ticket (str): jira ticket
        timeout (int, optional): timeout for actions. Defaults to 10.

    Returns:
        spawned process from pexpect after landing on the machine
    """
    config = get_config()
    pwd = config['CREDENTIALS'].get('password')
    pwd = decode_pass(pwd)
    PROMPT = [f'\r\n{login_user}\$ ', '\]\$ ']
    child.sendline(f"xtn {login_user}@{machine}")
    child.expect(config['GENERAL'].get('reason'), timeout=timeout)
    child.sendline(ticket)
    child.expect(config['GENERAL'].get('pass'), timeout=timeout)
    child.sendline(pwd)
    child.expect(PROMPT, timeout=5)
    return child


def pexpect_run_command(child, machine: str, cmd: str, login_user: str) -> str:
    """run command on remote machine with pexpect

    Args:
        child (_type_): pexpect spawned process
        machine (str): remote machine
        cmd (str): command to run
        login_user (str): user to run command
    Returns:
        print output and return the str
    """
    PROMPT = [f'\r\n{login_user}\$ ', '\]\$ ']
    # clean up previous records
    _ = child.before
    child.sendline(cmd)
    child.expect(PROMPT, timeout=10)
    # post buffer
    output = child.before.decode('utf-8')
    # trim the last $machine:/
    if f'[{login_user}@{machine}' in output:
        # linux machine
        end_idx = output.index(f'{login_user}@{machine}')
        output = '\r\n'.join(output[:end_idx].split('\r\n')[1:-1]).strip()
    else:
        output = ''.join(output.split(f'{machine}:/')[:-1]).strip()
        # trim the datetime
        output = '\r\n'.join(output.split('\r\n')[:-1])
        # trim the beginning command
        if output.startswith(cmd):
            output = output[len(cmd):].strip()
    print(output)
    return output


def check_network() -> bool:
    """check vpn network status

    Returns:
        bool: True if connect
    """
    from platform import system
    from pathlib import Path
    cur_sys = system()
    config = get_config()
    if cur_sys == 'Linux':
        import socket
        domain_name = config['GENERAL'].get('url')
        try:
            socket.gethostbyname(domain_name)
            return True
        except socket.gaierror:
            return False
    elif cur_sys == 'Windows':
        path_kw = 'root'
        general_path = config['GENERAL'].get(path_kw)
        if not config['SSH'].get('username'):
            print("Please set up your username and credentials with command: bau config edit")
        return (Path(general_path) / '.bashrc').exists()
    else:
        raise ValueError(f'Unexpected system found as {cur_sys}.')


def check_weekends(col):
    from loguru import logger
    weekends = col.apply(lambda x: True if pd.to_datetime(x, format='%Y%m%d').weekday() in (5, 6) else False)
    df_we = cf[weekends]
    if len(df_we):
        for _, row in df_we.iterrows():
            logger.error(f'Found cashflow on weekends for {row["code"]} on {row["date"]}')
        raise ValueError('Please verify the input file to make sure all cashflow happens on weekdays')


if __name__ == '__main__':
    # check_network()
    import os
    import pandas as pd
    file = 'CF20221117143615.csv'
    names = ['code', 'date', 'amount']
    try:
        cf = pd.read_csv(os.path.join(DATA_PATH, file), header=None, names=names,
                         dtype={'code': str, 'date': 'int64', 'amount': 'float64'})
    except ValueError:
        cf = pd.read_csv(os.path.join(DATA_PATH, file), header=None, names=names,
                         dtype={'code': str, 'date': str, 'amount': 'float64'})
        cf.loc[:, 'date'] = cf['date'].apply(lambda x: pd.to_datetime(x, format='%d/%m/%Y').strftime('%Y%m%d')).astype(
            int)
    check_weekends(cf['date'])
