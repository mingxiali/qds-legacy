#!/usr/bin/env python3
from loguru import logger
import smtplib
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate
from email.mime.base import MIMEBase
from email import encoders
from pathlib import Path
from utils import PACK_PATH
from utils.funcs import get_config


def send_gbst_mail(to, subject, greeting, message, addr='', cc=(), bcc=(), files=()):
    config = get_config()
    server = config['MAIL'].get('host')
    port = int(config['MAIL'].get('port'))
    name = config['MAIL'].get('name')
    if not addr:
        addr = config['JIRA'].get('user')
    mail_name = name.strip().replace(' ', '.')
    title = config['MAIL'].get('title')
    to = [x.strip() for x in to]
    cc = [x.strip() for x in cc]
    msg = MIMEMultipart()
    msg['From'] = addr
    msg['To'] = ','.join(to)
    msg['Cc'] = ','.join(cc)
    msg['Date'] = formatdate(localtime=True)
    msg['Bcc'] = ','.join(bcc)
    msg['Subject'] = subject

    template_str = (PACK_PATH / 'resources' / 'mail_template.html').read_text().replace('\n', '')
    template_str = template_str.replace('{{full_name}}', name).replace('{{mail_name}}', mail_name)\
        .replace('{{job_title}}', title).replace('{{greeting}}', greeting).replace('{{message}}', message)\
        .replace('{{company_image}}', str((PACK_PATH / 'resources' / 'company.png').resolve()))\
        .replace('{{linked_in}}', str((PACK_PATH / 'resources' / 'linkedin.png').resolve()))\
        .replace('{{twitter}}', str((PACK_PATH / 'resources' / 'twitter.png').resolve()))

    for file in ['company', 'linkedin', 'twitter']:
        img_path = PACK_PATH / 'resources' / f'{file}.png'
        with open(img_path, 'rb') as fp:
            img = MIMEImage(fp.read())
        img.add_header('Content-ID', '<{}>'.format(img_path))
        msg.attach(img)

    msg_text = MIMEText(template_str, 'html')
    msg.attach(msg_text)

    for path in files:
        part = MIMEBase('application', "octet-stream")
        with open(path, 'rb') as file:
            part.set_payload(file.read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition',
                        'attachment; filename="{}"'.format(Path(path).name))
        msg.attach(part)

    mailserver = smtplib.SMTP(server, port)
    err = mailserver.sendmail(addr, list(to) + list(cc) + list(bcc), msg.as_string())
    # err = mailserver.send_message(msg)
    if len(err) == 0:
        logger.info('Email sent successfully')
    else:
        logger.error(err)
        logger.error('Email sent failed')
    mailserver.quit()


if __name__ == '__main__':
    to = ['example@gbst.com']
    cc = ['example@outlook.com']
    bcc = ['example@gmail.com']
    send_gbst_mail(to, 'test', 'There', 'test message', bcc=bcc, cc=cc)
