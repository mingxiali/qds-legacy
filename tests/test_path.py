#!/usr/bin/env python3

import unittest
from commands import CONFIG_PATH, DATA_PATH


class TestSimple(unittest.TestCase):

    def test_path(self):
        # Assure these path exists after installation
        self.assertEqual(CONFIG_PATH.exists(), True)
        self.assertEqual(DATA_PATH.exists(), True)
